function [values] = introduce_mutations(pred, start_seq, middle_seq, end_seq, reps)
    int_start_seq = codon2int(start_seq.seq);
    int_middle_seq = codon2int(middle_seq.seq);
    int_end_seq = codon2int(end_seq.seq);
    
    diff1 = find(int_start_seq ~= int_middle_seq);
    diff2 = find(int_middle_seq ~= int_end_seq);
    
    n_diff1 = length(diff1);
    n_diff2 = length(diff2);
    
    fprintf('Differences: %d (middle), %d (end)\n', n_diff1, n_diff2);
    
    values = zeros(n_diff1 + n_diff2, reps);
    parfor i = 1:n_diff1
        for rep = 1:reps
            perm = randperm(n_diff1, i);
            seq = start_seq;
            seq.seq = int_start_seq;
            seq.seq(diff1(perm)) = int_middle_seq(diff1(perm));
            seq.seq = int2codon(seq.seq);
            
            values(i, rep) = pred.predict_value(seq);
        end
    end
    
    parfor i = 1:n_diff2
        for rep = 1:reps
            perm = randperm(n_diff2, i);
            seq = middle_seq;
            seq.seq = int_middle_seq;
            seq.seq(diff2(perm)) = int_end_seq(diff2(perm));
            seq.seq = int2codon(seq.seq);
            
            values(n_diff1 + i, rep) = pred.predict_value(seq);
        end
    end
end