function [ds] = select_from_bins(bins, num_samples)
    ds = [];
    n_bins = size(bins, 2);
    bin_size = zeros(1, n_bins);
    for i = 1:n_bins
        bin_size(i) = size(bins{i}, 1);
    end
    
    for i = 1:n_bins
        if bin_size(i) == 0
            continue;
        end
        n_select = min(num_samples, bin_size(i));
        items = randperm(bin_size(i), n_select);
        if ~isempty(ds)
            ds = [ds; bins{i}(items, :)];
        else
            ds = bins{i}(items, :);
        end
    end
end