classdef TPI
    properties
        CodonFrequencies
        tRNAFrequencies
    end
    
    properties (Hidden)
        tTrans
        tRevTrans
        aminos
    end
    
    methods
        function [obj] = TPI(cds, tTrans)
            codons = get_codons;
            obj.aminos = amino2int(get_aminos);
            obj.CodonFrequencies = zeros(1, length(codons));
            
            if ~isnumeric(cds)
                n = length(cds);
                for i = 1:n
                    seq = cds(i).seq;
                    codon = codon2int(seq);
                    for j = 1:length(codon)
                        obj.CodonFrequencies(codon(j)) = obj.CodonFrequencies(codon(j)) + 1;
                    end
                end
                obj.CodonFrequencies = obj.CodonFrequencies / sum(obj.CodonFrequencies);
            else
                obj.CodonFrequencies = cds;
            end
            
            transLen = length(tTrans);
            obj.tTrans = cell(transLen, 1);
            for i = 1:transLen % amino
                tLen = length(tTrans{i});
                obj.tTrans{i} = cell(1, tLen);
                for j = 1:tLen % tRNA
                    cLen = length(tTrans{i}{j});
                    tArr = zeros(1, cLen);
                    for k = 1:cLen
                        tArr(k) = codon2int(tTrans{i}{j}{k});
                    end
                    obj.tTrans{i}{j} = tArr;
                end
            end
            
            obj.tRNAFrequencies = cell(transLen, 1);
            for i = 1:transLen % amino
                tLen = length(tTrans{i});
                tArr = zeros(1, tLen);
                for j = 1:tLen % tRNA
                    tArr(j) = sum(obj.CodonFrequencies(obj.tTrans{i}{j}));
                end
                obj.tRNAFrequencies{i} = tArr;
            end
            
            obj.tRevTrans = zeros(length(codons), 1);
            for i = 1:length(codons)
                done = false;
                for j = 1:length(obj.tTrans) % amino
                    for k = 1:length(obj.tTrans{j}) % tRNA
                        if ~isempty(find(obj.tTrans{j}{k} == i, 1))
                            obj.tRevTrans(i) = k;
                            done = true;
                            break;
                        end
                    end
                    if done
                        break;
                    end
                end
            end
        end
        
        function [tpi] = calculate_tpi(obj, seq)
            codon = codon2int(seq);
            tRNA  = obj.codon2tRNA(codon);
            amino = codon2amino_int(seq);
            
            distr = cell(length(obj.aminos), 1);
            distr_conv = [];
            observed_changes = 0;
            for am = obj.aminos
                subSeq = tRNA(amino == am);
                observed_changes = observed_changes + obj.count_changes(subSeq);
                if length(obj.tTrans{am}) > 1
                    distr{am} = obj.compute_distr(subSeq, obj.tRNAFrequencies{am});
                    %distr{am} = obj.make_cum(distr{am});
                    %sum(distr{am})
                    if isempty(distr_conv)
                        distr_conv = distr{am};
                    else
                        distr_conv = conv(distr_conv, distr{am});
                    end
                    %fprintf('Cur %d, Tot %d\n', length(distr{am}), length(distr_conv));
                end
            end
            
            distr_conv = obj.make_cum(distr_conv);
            
            tpi = 1 - 2 * distr_conv(observed_changes + 1);
        end
    end
    
    methods (Hidden)
        function [tRNA] = codon2tRNA(obj, seq)
            l = length(seq);
            tRNA = zeros(1, l);
            for i = 1:l
               tRNA(i) = obj.tRevTrans(seq(i));
            end
        end
        
        function [distr] = compute_distr(obj, seq, P)
            P = P / sum(P); % normalize symbol probabilities
            nSymb = length(P);
            n = length(seq);
            dp = zeros([nSymb n + 1 n + 1]); % Last symbol, number of changes, number of symbols left
            dp(:, 0 + 1, 0 + 1) = 1; % zero changes and zero symbols => probability of 1
            for i = 1:nSymb
                for j = 2:n + 1
                    dp(i, 1, j) = P(i) ^ (j - 1);
                end
            end
            
            for i = 2:n + 1 % symbols left
                for j = 2:n + 1 % changes in sequence
                    for k = 1:nSymb % last symbol
                        %s = P(k) * dp(k, j, i - 1);
                        %for l = 1:nSymb
                        %    if l == k
                        %        continue;
                        %    end
                        %    s = s + P(l) * dp(l, j - 1, i - 1);
                        %end
                        %dp(k, j, i) = s;
                        dp(k, j, i) = P * dp(:, j - 1, i - 1) - P(k) * dp(k, j - 1, i - 1) + P(k) * dp(k, j, i - 1);
                    end
                end
            end
            
            distr = zeros(1, n + 1);
            if n == 0
                distr(1) = 1;
                return;
            end
            
            for i = 1:n + 1
                distr(i) = P * dp(:, i, n);
            end
        end
    end
    
    methods (Hidden, Static)
        function [chg] = count_changes(seq)
            chg = 0;
            for i = 2:length(seq)
                if seq(i) ~= seq(i - 1)
                    chg = chg + 1;
                end
            end
        end
        
        function [cum] = make_cum(distr)
            cum = cumsum(distr) - 0.5 * distr;
        end
    end
end