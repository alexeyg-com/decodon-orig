% Compute P1 index
%   SEQ  - input sequence
%   TGCN - tRNA gene copy number
%
%   P1   - compute P1 index
function [p1] = compute_p1(seq, tGCN)
    arrayLen = length(get_codons);
    codons = codon2int(seq);
    
    usage = zeros(1, arrayLen);
    for i = 1:length(codons)
        usage(codons(i)) = usage(codons(i)) + 1;
    end
    
    usage = usage / sum(usage);
    tGCN = tGCN / sum(tGCN);
    tGCN(tGCN == 0) = 1;
    tGCN = 1 ./ tGCN;
    p1 = sum(usage .* [tGCN 0]);
end