function [set] = attach_nagalakshmi(inset, fileName)
    if nargin < 2
        fileName = 'import/Nagalakshmi.xls';
    end
    [levels, names] = xlsread(fileName);
    levels = levels(1:end, end);
    names = names(4:end, 1);
    
    non_nan = find(~isnan(levels));
    levels = 2 .^ levels(non_nan);
    names = names(non_nan);
    
    set = inset;
    map_names = strtok({inset.desc});
    total = size(levels, 1);
    missing = 0;
    for i = 1:total;
        name = names{i};
        %fprintf('%d %s\n', i, name);
        ind = find(strcmpi(name, map_names), 1);
        if ~isempty(ind)
            set(ind).mRNA_seq = levels(i);
        else
            fprintf('Unable to find CDS %s\n', name);
            missing = missing + 1;
        end
    end
    
    fprintf('Total: %d, Missing: %d\n', total, missing);
end