function [res] = construct_sequence_set(fileName)
    [header, seq] = fastaread(fileName);
    res = [];
    for i = 1:length(header)
        item.seq = seq{i};
        item.desc = header{i};
        res = [res item];
    end
end