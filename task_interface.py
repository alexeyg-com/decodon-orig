import time
from IPython.kernel import client
import marshal
import types
import os
import cluster_storage
import threading
import logging
import random
import xmlrpclib
import time
import numpy
import threading
import random
import zlib
import cPickle
import operator

NOVAL="NOVALUE"

def seqgen(start=0):
    """Generator, generating monotonically increasing indexes"""
    while True:
        start += 1
        yield start
taskids = seqgen()
genlock = threading.Lock()

####LOGGING CODE ######
def initServerLogging(loglevel=logging.WARNING):
    l = logging.getLogger("task_interface")
    l.handlers = []
    s = logging.StreamHandler()
    f = logging.Formatter('%(levelname)s (id:%(task_id)s) - %(asctime)s: %(filename)s:%(lineno)s - %(message)s')
    s.setFormatter(f)
    l.addHandler(s)
    l.setLevel(loglevel)
    l.propagate=False

#client side code
def initClientLogging(loglevel):
    l = logging.getLogger()
    l.handlers = []
    sh = RecordHandler()
    l.addHandler(sh)
    l.setLevel(loglevel)
    return sh

class RecordHandler(logging.Handler):
    def __init__(self, loglevel=logging.NOTSET):
        self.records = []
        logging.Handler.__init__(self, loglevel)

    def clear(self):
        self.records = []

    def emit(self, record):
        self.records.append(record)

    def get_records(self):
        return self.records

####  CLIENT EXEC OBJ ####
class ClientEnv(object):
    def __init__(self, engine_params, loglevel):
        self.engine_params = engine_params
        self.storage = cluster_storage.create_highlevel(os.getcwd(), engine_params)
        self.logrecords = initClientLogging(loglevel)

    def __getitem__(self, id):
        return self.pull_obj(id)

    def pull_obj(self, id):
        if not id in self.__dict__:
            if 'cluster' in self.engine_params:
                time.sleep(random.randint(0,300)) #probably a lot of objects trying simultaneously....
            self.__dict__[id] = self.storage.receive(id)
        return self.__dict__[id]

    def push_obj(self, obj, id=None,noreplace=False):
        self.storage.submit(obj,id,noreplace)

    def get_callable_obj(self, id):
        if not id in self.__dict__:
            x = self.pull_obj(id)
            if hasattr(x, 'init_client'):
                x.init_client(self)
        return self.__dict__[id]                

    def _run(self, action, inargs, inkwds):
        if(isinstance(action,tuple)):
            x = self.get_callable_obj(action[0])
            res = getattr(x,action[1])(*inargs, **inkwds)
        else:
            code = marshal.loads(action)
            func = types.FunctionType(code, globals(), "func_name")
            res = func(self, *inargs, **inkwds)
        return zlib.compress(cPickle.dumps(res, protocol=2),9)

        
    def _prepare(self, loglevel):
        l = logging.getLogger()
        l.handlers[0].clear()
        l.setLevel(loglevel)


strtask = """
outparams = None
excres = None
logmessage = None
try:
    try:
        import task_interface
        if not '%(myid)s' in globals():
            globals()['%(myid)s'] = task_interface.ClientEnv(engine_params, loglevel)
        else:
            globals()['%(myid)s']._prepare(loglevel)
        outparams = globals()['%(myid)s']._run(action, inargs, inkwds)
    except Exception, e:
        import traceback
        excres = str(e) + '\\n' + traceback.format_exc()
finally:    
    g = globals().get('%(myid)s',None)
    if hasattr(g, 'logrecords'):
        logmessage = g.logrecords.get_records()
        g.logrecords.clear()
"""
outparams = ["outparams","excres","logmessage"]

class Task(object):
    def __init__(self, task_interface, action, inargs, inkwds, retry=0, timeout=None, loglevel=logging.WARNING):
        self.action = action
        self.inparams = {'inargs':inargs, 'inkwds': inkwds, 'action':self.action,'engine_params': task_interface.engine_params,'loglevel':loglevel}
        genlock.acquire()
        self.task_local_id = taskids.next()
        genlock.release()
        self.task_interface = task_interface
        self.timeout = timeout

    def _getId(self):
        return self.task_local_id
    id = property(fget=_getId)

    def submit(self, task_interface):
        raise NotImplemented

    def receive(self, task_interface):
        raise NotImplemented
    
    def _reemitLogs(self, records):
        l = logging.getLogger("task_interface")
        for record in records:
            record.task_id = self.task_local_id
            l.handle(record)

class ClusterTask(Task):
    def __init__(self, task_interface, action, inargs, inkwds, retry=10, timeout=None, loglevel=logging.WARNING):
        Task.__init__(self, task_interface, action, inargs, inkwds, retry, timeout, loglevel)
        self.task = client.StringTask(self.task_interface.strtask, outparams, self.inparams)
        self.task_controller_id = None
        assert retry > 0, "Retry should not be smaller than 0"
        self.retry_count = retry + 1
        self.start = None
        self.event = threading.Event()

    def submit(self):
        assert self.task_controller_id  is None, "Task already submitted"
        self.task_controller_id = self.task_interface.tc.run(self.task)
        self.event.clear()
        self.start = time.time()

    def getRunTime(self):
        if hasattr(self, "running_time"):
            return self.running_time
        return None

    def _abort(self):
        try:
            self.task_interface.tc.abort(self.task_controller_id)
        except:
            pass

    def _check_timeout(self):
        if self.timeout is None:
            return False

        if self.timeout + self.start < time.time():
            if self.retry_count > 0:
                self._abort() #fixme: maybe make a list of timeout tasks, and abort them only when a result is retrieved
                logging.warning("Timeout for: " + str(self.task_controller_id) + ", retries left: " + str(self.retry_count))
                self.task_controller_id = self.task_interface.tc.run(self.task)
                self.start = time.time()
                self.retry_count -= 1
                return False
            else:
                self.exception = RuntimeError("Task " + str(self.id) + "(internal id: " + str(self.task_controller_id) + ") timed out")
                return True
        return False                

    def poll(self):
        if hasattr(self,'result'):
            return True
        try:        
            res = self._obtain_res()
            if res is NOVAL and not self._check_timeout():
                return False
            self.result = res
        except Exception, e:
            self.exception = e
        self.running_time = time.time() - self.start
        self.event.set()            
        return True            

    def receive(self):
        while True:
            self.event.wait(30.0)
            if self.event.isSet():
                break
            if not self.task_interface.isAlive():
                raise RuntimeError, "Task interface stopped"
        if hasattr(self, "exception"):
            raise self.exception
        return self.result

    def _reemitLogs(self, records):
        for record in records:
            record.internal_task_id = self.task_controller_id
        Task._reemitLogs(self, records)
       

    def _obtain_res(self):
        res = None
        curid = self.task_controller_id
        tobj = None
        try:
            tobj = self.task_interface.tc.get_task_result(curid, block=False)
            if tobj is None:
                return NOVAL

            if tobj.failure:
                raise RuntimeError, "Task failure: " + str(tobj.failure)

            excres = tobj['excres']
            if not excres is None:
                raise RuntimeError, excres
            
            res = tobj['outparams']
            res = cPickle.loads(zlib.decompress(res))
            self._reemitLogs(tobj['logmessage'])
              
        except Exception, e:
            if not tobj is None and 'logmessage' in tobj.keys:
               self._reemitLogs(tobj['logmessage'])

            res = NOVAL
            logging.warning("Exception for: " + str(self.task_controller_id) + ", retries left: " + str(self.retry_count))
            logging.warning("Exception: " + str(e))
            if self.retry_count > 0:
                self.task_controller_id = self.task_interface.tc.run(self.task)
            else:
                raise
            self.retry_count -= 1
            

        try:
            self.task_interface.tc.clear([curid])
        except Exception, e:
            logging.warning("Exception: " + str(e))

        return res

class LocalTask(Task):
    def submit(self):
        pass

    def poll(self):
        return True
    
    def receive(self):
        locals = self.inparams.copy()
        exec self.task_interface.strtask in  globals(), locals
        excres = locals["excres"]
        if not excres is None:
            raise RuntimeError, excres
        if 'logmessage' in locals:
            self._reemitLogs(locals['logmessage'])
        return locals["outparams"] 

class TaskInterface(object):
    _tasktype = Task
    def __init__(self, engine_params={'local':'*'}, tmpdir=None, retry=10, timeout=None, loglevel=logging.WARNING):
        self.tasks = {}
        if tmpdir is None:
            tmpdir = os.getcwd()
        self.storage = cluster_storage.create_highlevel(tmpdir, engine_params)
        self.engine_params = engine_params
        self.retry = retry
        self.timeout = timeout
        self.loglevel = loglevel
        self.object_ids = []
        myid = random.randint(0,100000000)
        self.strtask = strtask % {'myid':'id' + str(myid)}
        self.maptasks = {}

    def push_obj(self, obj, id=None, noreplace=False):
        id = self.storage.submit(obj, id, noreplace)
        self.object_ids.append(id)
        return id

    def pull_obj(self, id):
        return self.storage.receive(id)

    def exec_func(self, func, *inargs, **inkwds):
        action = marshal.dumps(func.func_code)
        return self._submit(action, *inargs, **inkwds)
    
    def exec_method(self, objid, methodname, *inargs, **inkwds):
        action = (objid, methodname)
        return self._submit(action, *inargs, **inkwds)

    def _submit(self, action, *inargs, **inkwds):
        retry = inkwds.pop('retry',self.retry)
        timeout= inkwds.pop("timeout",self.timeout)
        loglevel = inkwds.pop("loglevel",self.loglevel)
        t = self._tasktype(self, action, inargs, inkwds, retry=retry, timeout=timeout, loglevel=loglevel)
        t.submit()
        self.tasks[t.id] = t
        return t.id

    def get_result(self, task_id):
        if isinstance(task_id, tuple):
            blocksize = self.maptasks[task_id]
            del self.maptasks[task_id]
            if blocksize:
                res = [self.get_result(tid) for tid in task_id]
                if all([isinstance(r,(list,numpy.ndarray)) for r in res]):
                    return numpy.concatenate(res,axis=0)
                elif all([isinstance(r,dict) for r in res]):
                    assert len(set([tuple(r.keys()) for r in res])) == 1, "Dict return keys should be equal across blocks, " + str([tuple(r.keys()) for r in res])
                    xres = [(i,numpy.concatenate([r[i] for r in res],axis=0)) for i in res[0].keys()]
                    return dict(xres)
                elif all([isinstance(r,tuple) for r in res]):
                    assert len(set([len(r) for r in res])) == 1, "Tuple return lengths should be equal across blocks"
                    xres = [numpy.concatenate([r[i] for r in res],axis=0) for i in len(res[0])]
                    return tuple(xres)
            else:
                return [self.get_result(tid) for tid in task_id]
        else:
            res = self.tasks[task_id].receive()
            del self.tasks[task_id]
            return res
                
    def _map(self, action, *args, **kwds):
        blocksize = kwds.pop("blocksize",None)
        block = kwds.pop("block",True)
        if blocksize:
            elems = args[0]
            taskids = []
            while len(elems) > 0:
                selems = elems[:blocksize]
                elems = elems[blocksize:]
                taskids.append(self._submit(action, selems, *args[1:], **kwds))
        else:
            taskids = [self._submit(action, arg, *args[1:], **kwds) for arg in args[0]]
        taskids = tuple(taskids)
        self.maptasks[taskids] = blocksize
       
        if(block): 
            return self.get_result(taskids)
        else:
            return taskids

    def map_func(self, func, *args, **kwds):
        action = marshal.dumps(func.func_code)
        return self._map(action, *args, **kwds)
    
    def map_method(self, objid, methodname, *args, **kwds):
        action = (objid, methodname)
        return self._map(action, *args, **kwds)

    def close(self):
        logging.getLogger("task_interface").info("Clearing pushed objects")
        for object_id in self.object_ids:
            self.storage.destroy(object_id)
                    

class ClusterTaskInterface(TaskInterface,threading.Thread):
    _tasktype = ClusterTask
    def __init__(self, engine_params={'cluster':'*','local':'*'}, tmpdir=None, retry=10, timeout=None, loglevel=logging.WARNING, port = 30024):
        TaskInterface.__init__(self, engine_params, tmpdir, retry, timeout, loglevel)
        threading.Thread.__init__(self)
        self.tc = client.TaskClient()
        self.daemon= True
        self._stop = False
        self.start()
        self.port = port
    
    def exec_func(self, func, *inargs, **inkwds):
        assert self.isAlive(), "Cannot submit task on a dead interface. Please recreate."
        return TaskInterface.exec_func(self, func, *inargs, **inkwds)            
    
    def exec_method(self, objid, methodname, *inargs, **inkwds):
        assert self.isAlive(), "Cannot submit task on a dead interface. Please recreate."
        return TaskInterface.exec_method(self, objid, methodname, *inargs, **inkwds)            

    def run(self):
        try:
            while not self._stop:
                q = self.tc.queue_status()
                for task in list(self.tasks.values()):
                    if not task.poll() and q['succeeded'] <= (q['scheduled'] + q['pending']):
                        time.sleep(max(min(10.0 / len(self.tasks), 0.1),0.001))
                self.tc.spin()
                time.sleep(0.1)
        finally:
            self._stop = True
            for task in list(self.tasks.values()):
                try:
                    task._abort()
                except:
                    pass

    def close(self):
        try:
            self.tc.clear()
        except:
            pass
        self._stop = True
        super(ClusterTaskInterface, self).close()

    def reload_engines(self, dontwait=False):
        s = xmlrpclib.ServerProxy('http://localhost:' + str(self.port))
        s.reload()
        if not dontwait:
            time.sleep(180)

class LocalTaskInterface(TaskInterface):
    _tasktype = LocalTask


