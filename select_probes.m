% Selects microarray probes with a given name
%   EXPR        - expression array
%   PROBE_NAMES - cell array with names of probes to select
%
%   SEL         - selected probes from expression array
function [sel] = select_probes(expr, probe_names)
    ind = [];
    n = length(expr.name);
    for i = 1:n
        res = strcmpi(expr.name{i}, probe_names);
        token = strtok(expr.desc{i});
        if (max(res) == 1) && (max(strcmp(token, {'ubiquinol--cytochrome-c', 'F1F0-ATPase'})) == 0)
            ind = [ind i];
        end
    end
    sel.e_mean = expr.e_mean(ind);
    sel.e_max = expr.e_max(ind);
    sel.e_min = expr.e_min(ind);
    sel.e_std = expr.e_std(ind);
    sel.e = expr.e(:, ind);
    sel.p = expr.p(ind);
    sel.name = expr.name(ind);
    sel.desc = expr.desc(ind);
end