classdef Fop
    properties
        IsOptimal
    end
    
    properties (Hidden)
        tGCN
        Met
        Trp
    end
    
    methods
        function [obj] = Fop(tGCN)
            obj.tGCN = [tGCN 0];
            obj.IsOptimal = false(1, length(get_codons));
            obj.Met = codon2int('ATG');
            obj.Trp = codon2int('TGG');
            trans = amino2codons_int;
            for i = 1:length(trans)
                amino_codons = trans{i};
                max_copy = max(obj.tGCN(amino_codons));
                if max_copy > 0
                    max_copy_codons = amino_codons(obj.tGCN(amino_codons) == max_copy);
                    obj.IsOptimal(max_copy_codons) = true;
                end
            end
        end
        
        function [fop] = calculate_fop(obj, seq)
            if all(isnumeric(seq))
                codon = seq;
            else
                codon = codon2int(seq);
            end
            indices = codon ~= obj.Met;
            indices = logical(indices .* (codon ~= obj.Trp));
            opt = sum(obj.IsOptimal(codon(indices)));
            tot = sum(indices);
            if tot == 0
                tot = 1;
            end
            fop = opt / tot;
        end
    end
end