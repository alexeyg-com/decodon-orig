% Create a dataset with predictors and response variable mRNA
%   SEQ  - input sequences with computed features
%   MODE - 'mean' or 'max' mRNA expression levels
%
%   DS   - created dataset
function [ds] = make_dataset(seq, mode, use_utr3, use_utr5)
    if nargin < 2
        mode = 'mRNA-lim-mean';
    end
    if nargin < 3
        use_utr3 = true;
    end
    if nargin < 4
        use_utr5 = true;
    end
    
    warning('off', 'stats:dataset:genvalidnames:ModifiedVarnames');
    
    filter = {};
    zero_filter = {};
    if use_utr5
        filter = [ filter; 'utr5' ];
    end
    
    if use_utr3
        filter = [ filter; 'utr3' ];
    end
    
    if strcmpi(mode, 'mRNA-lim-mean')
        filter = [filter; 'mRNA_lim_mean'];
    end
    
    if strcmpi(mode, 'mRNA-lim-max')
        filter = [filter; 'mRNA_lim_max'];
    end
    
    if strcmpi(mode, 'mRNA-lim-min')
        filter = [filter; 'mRNA_lim_min'];
    end
    
    if strcmpi(mode, 'mRNA-Arava') || strcmpi(mode, 'GFP/mRNA-Arava') || strcmpi(mode, 'TAP/mRNA-Arava') || strcmpi(mode, 'mRNA-Arava*Density-Arava') || strcmpi(mode, 'mRNA-Arava*Relative-rate-Arava') || strcmpi(mode, 'mRNA-Arava*Density-Ingolia') || strcmpi(mode, 'Protein-Lu/mRNA-Arava') || strcmpi(mode, 'mRNA-Arava*Density-Ingolia-corrected')
        filter = [filter; 'mRNA_Arava'];
    end
    
    if strcmpi(mode, 'GFP/mRNA-Arava') || strcmpi(mode, 'TAP/mRNA-Arava') || strcmpi(mode, 'Protein-Lu/mRNA-Arava')
        zero_filter = [zero_filter; 'mRNA_Arava'];
    end
    
    if strcmpi(mode, 'mRNA-Ingolia') || strcmpi(mode, 'GFP/mRNA-Ingolia') || strcmpi(mode, 'TAP/mRNA-Ingolia') || strcmpi(mode, 'mRNA-Ingolia*Density-Arava') || strcmpi(mode, 'mRNA-Ingolia*Relative-rate-Arava') || strcmpi(mode, 'mRNA-Ingolia*Density-Ingolia') || strcmpi(mode, 'Protein-Lu/mRNA-Ingolia') || strcmpi(mode, 'mRNA-Ingolia*Density-Ingolia-corrected')
        filter = [filter; 'mRNA_Ingolia'];
    end
    
    if strcmpi(mode, 'GFP/mRNA-Ingolia') || strcmpi(mode, 'TAP/mRNA-Ingolia') || strcmpi(mode, 'Protein-Lu/mRNA-Ingolia')
        zero_filter = [zero_filter; 'mRNA_Ingolia'];
    end
    
    if strcmpi(mode, 'mRNA-count') || strcmpi(mode, 'GFP/mRNA-count') || strcmpi(mode, 'TAP/mRNA-count') || strcmpi(mode, 'mRNA-count*Density-Arava') || strcmpi(mode, 'mRNA-count*Relative-rate-Arava') || strcmpi(mode, 'mRNA-count*Density-Ingolia') || strcmpi(mode, 'Protein-Lu/mRNA-count') || strcmpi(mode, 'mRNA-count*Density-Ingolia-corrected')
        filter = [filter; 'mRNA_count'];
    end
    
    if strcmpi(mode, 'GFP/mRNA-count') || strcmpi(mode, 'TAP/mRNA-count') || strcmpi(mode, 'Protein-Lu/mRNA-count')
        zero_filter = [zero_filter; 'mRNA_count'];
    end
    
    if  strcmpi(mode, 'mRNA-seq') || strcmpi(mode, 'GFP/mRNA-seq') || strcmpi(mode, 'TAP/mRNA-seq') || strcmpi(mode, 'mRNA-seq*Density-Arava') || strcmpi(mode, 'mRNA-seq*Relative-rate-Arava') || strcmpi(mode, 'mRNA-seq*Density-Ingolia') || strcmpi(mode, 'Protein-Lu/mRNA-seq') || strcmpi(mode, 'mRNA-seq*Density-Ingolia-corrected')
        filter = [filter; 'mRNA_seq'];
    end
    
    if strcmpi(mode, 'GFP/mRNA-seq') || strcmpi(mode, 'TAP/mRNA-seq') || strcmpi(mode, 'Protein-Lu/mRNA-seq')
        zero_filter = [zero_filter; 'mRNA_seq'];
    end
    
    if strcmpi(mode, 'mRNA-Lu') || strcmpi(mode, 'GFP/mRNA-Lu') || strcmpi(mode, 'TAP/mRNA-Lu') || strcmpi(mode, 'Protein-Lu/mRNA-Lu') || strcmpi(mode, 'mRNA-Lu*Density-Arava') || strcmpi(mode, 'mRNA-Lu*Relative-rate-Arava') || strcmpi(mode, 'mRNA-Lu*Density-Ingolia') || strcmpi(mode, 'mRNA-Lu*Density-Ingolia-corrected')
        filter = [filter; 'mRNA_Lu'];
    end
    
    if strcmpi(mode, 'GFP/mRNA-Lu') || strcmpi(mode, 'TAP/mRNA-Lu') || strcmpi(mode, 'Protein-Lu/mRNA-Lu')
        zero_filter = [zero_filter; 'mRNA_Lu'];
    end
    
    if strcmpi(mode, 'GFP') || strcmpi(mode, 'GFP/mRNA-Arava') || strcmpi(mode, 'GFP/mRNA-count') || strcmpi(mode, 'GFP/mRNA-seq') || strcmpi(mode, 'GFP/mRNA-Ingolia') || strcmpi(mode, 'GFP/mRNA-Lu')
        filter = [filter; 'gfp'];
    end
    
    if strcmpi(mode, 'TAP') || strcmpi(mode, 'TAP/mRNA-Arava') || strcmpi(mode, 'TAP/mRNA-count') || strcmpi(mode, 'TAP/mRNA-seq') || strcmpi(mode, 'TAP/mRNA-Ingolia') || strcmpi(mode, 'TAP/mRNA-Lu')
        filter = [filter; 'tap'];
    end
    
    if strcmpi(mode, 'Protein-Lu') || strcmpi(mode, 'Protein-Lu/mRNA-Arava') || strcmpi(mode, 'Protein-Lu/mRNA-Ingolia') || strcmpi(mode, 'Protein-Lu/mRNA-count') || strcmpi(mode, 'Protein-Lu/mRNA-seq') || strcmpi(mode, 'Protein-Lu/mRNA-Lu')
        filter = [filter; 'protein_Lu'];
    end
    
    if strcmpi(mode, 'Density-Arava') || strcmpi(mode, 'Inv-density-Arava') || strcmpi(mode, 'mRNA-Arava*Density-Arava') || strcmpi(mode, 'mRNA-count*Density-Arava') || strcmpi(mode, 'mRNA-seq*Density-Arava') || strcmpi(mode, 'mRNA-Ingolia*Density-Arava') || strcmpi(mode, 'mRNA-Lu*Density-Arava')
        filter = [filter; 'density_Arava'];
    end
    
    if strcmpi(mode, 'Density-Ingolia') || strcmpi(mode, 'Inv-Density-Ingolia')  || strcmpi(mode, 'mRNA-Arava*Density-Ingolia') || strcmpi(mode, 'mRNA-Ingolia*Density-Ingolia') || strcmpi(mode, 'mRNA-count*Density-Ingolia') || strcmpi(mode, 'mRNA-seq*Density-Ingolia') || strcmpi(mode, 'mRNA-Lu*Density-Ingolia')
        filter = [filter; 'density_Ingolia'];
    end
    
    if strcmpi(mode, 'Density-Ingolia-corrected') || strcmpi(mode, 'Inv-Density-Ingolia-corrected')  || strcmpi(mode, 'mRNA-Arava*Density-Ingolia-corrected') || strcmpi(mode, 'mRNA-Ingolia*Density-Ingolia-corrected') || strcmpi(mode, 'mRNA-count*Density-Ingolia-corrected') || strcmpi(mode, 'mRNA-seq*Density-Ingolia-corrected') || strcmpi(mode, 'mRNA-Lu*Density-Ingolia-corrected')
        filter = [filter; 'density_Ingolia_corrected'];
    end
    
    if strcmpi(mode, 'Inv-Density-Arava')
        zero_filter = [zero_filter; 'density_Arava'];
    end
    
    if strcmpi(mode, 'Inv-Density-Ingolia')
        zero_filter = [zero_filter; 'density_Ingolia'];
    end
    
    if strcmpi(mode, 'Inv-Density-Ingolia-corrected')
        zero_filter = [zero_filter; 'density_Ingolia_corrected'];
    end
    
    if strcmpi(mode, 'Relative-Rate-Arava') || strcmpi(mode, 'Inv-Relative-Rate-arava') || strcmpi(mode, 'mRNA-Arava*Relative-rate-Arava') || strcmpi(mode, 'mRNA-count*Relative-rate-Arava') || strcmpi(mode, 'mRNA-seq*Relative-rate-Arava') || strcmpi(mode, 'mRNA-Ingolia*Relative-rate-Arava') || strcmpi(mode, 'mRNA-Lu*Relative-rate-Arava')
        filter = [filter; 'relative_rate_Arava'];
    end
    
    if strcmpi(mode, 'Inv-Relative-Rate-arava')
        zero_filter = [zero_filter; 'relative_rate_Arava'];
    end
    
    if strcmpi(mode, 'Expected-protein-Ingolia')
        filter = [filter; 'expected_protein_Ingolia'];
    end
    
    if strcmpi(mode, 'Expected-Protein-Ingolia-Corrected')
        filter = [filter; 'expected_protein_Ingolia_corrected'];
    end
    
    seq = filter_sequence_set(seq, filter);
    seq = filter_zeros_sequence_set(seq, zero_filter);
    n = length(seq);
    
    insert_dataset = dataset;
    
%    gc = dataset([seq.gc]', 'VarNames', {'GC'});
    gc15 = dataset([seq.gc15]', 'VarNames', {'GC15'});
%    gc3 = dataset([seq.gc3]', 'VarNames', {'GC3'});
    len = dataset([seq.length]', 'VarNames', {'len'});
    nc = dataset([seq.nc]', 'VarNames', {'Nc'});
    ew = dataset([seq.ew]', 'VarNames', {'Ew'});
%    scuo = dataset([seq.scuo]', 'VarNames', {'SCUO'});
    p1 = dataset([seq.p1]', 'VarNames', {'P1'});
%    p2 = dataset([seq.p2]', 'VarNames', {'P2'});
    rcbs = dataset([seq.rcbs]', 'VarNames', {'RCBSpc'});
    cai = dataset([seq.cai]', 'VarNames', {'CAI'});
    tai = dataset([seq.tai]', 'VarNames', {'tAI'});
%    rca = dataset([seq.rca]', 'VarNames', {'RCA'});
%    pindex = dataset([seq.p]', 'VarNames', {'P'});
    cpb = dataset([seq.cpb]', 'VarNames', {'CPB'});
    tpi = dataset([seq.tpi]', 'VarNames', {'TPI'});
    gravy = dataset([seq.gravy]', 'VarNames', {'GRAVY'});
    aroma = dataset([seq.aroma]', 'VarNames', {'AROMA'});
    aliphatic = dataset([seq.aliphatic]', 'VarNames', {'Aliphatic'});
    instability = dataset([seq.II]', 'VarNames', {'Instability'});
    side_charge = dataset([seq.side_charge]', 'VarNames', {'SideCharge'});
    tai14 = dataset([seq.tai14]', 'VarNames', {'tAI14'});
    tai17 = dataset([seq.tai17]', 'VarNames', {'tAI17'});
    tai19 = dataset([seq.tai19]', 'VarNames', {'tAI19'});
    side_charge4 = dataset([seq.side_charge4]', 'VarNames', {'SideCharge4'});
    side_charge11 = dataset([seq.side_charge11]', 'VarNames', {'SideCharge11'});
    side_charge15 = dataset([seq.side_charge15]', 'VarNames', {'SideCharge15'});
    side_charge40 = dataset([seq.side_charge40]', 'VarNames', {'SideCharge40'});
    cai2nd = dataset([seq.cai2nd]', 'VarNames', {'CAI2nd'});
    tai2nd = dataset([seq.tai2nd]', 'VarNames', {'tAI2nd'});
    dG_CDS = dataset([seq.cds_fold]', 'VarNames', {'dG_CDS'});
    dG_CDS17 = dataset([seq.cds17_fold]', 'VarNames', {'dG_CDS17'});
    dG_CDS34 = dataset([seq.cds34_fold]', 'VarNames', {'dG_CDS34'});
    dG_CDS53 = dataset([seq.cds53_fold]', 'VarNames', {'dG_CDS53'});
    pI = dataset([seq.pI]', 'VarNames', {'pI'});
    weight = dataset([seq.weight]', 'VarNames', {'MolWeight'});
    charge = dataset([seq.charge]', 'VarNames', {'Charge'});
    fop = dataset([seq.fop]', 'VarNames', {'Fop'});
%    cbi = dataset([seq.cbi]', 'VarNames', {'CBI'});
    dnuc = dataset([seq.dnuc]', 'VarNames', {'Dnuc'});
    
    if use_utr3
        if isfield(seq, 'utr3len')
            insert_dataset = horzcat(insert_dataset, dataset([seq.utr3len]', 'VarNames', {'len_3UTR'}));
        end
        if isfield(seq, 'utr3_fold')
            insert_dataset = horzcat(insert_dataset, dataset([seq.utr3_fold]', 'VarNames', {'dG_3UTR'}));
        end
    end
    
    if use_utr5
        if isfield(seq, 'utr5len')
            insert_dataset = horzcat(insert_dataset, dataset([seq.utr5len]', 'VarNames', {'len_5UTR'}));
        end
        if isfield(seq, 'utr5_fold')
            insert_dataset = horzcat(insert_dataset, dataset([seq.utr5_fold]', 'VarNames', {'dG_5UTR'}));
        end
        if isfield(seq, 'utr5_fold50')
            insert_dataset = horzcat(insert_dataset, dataset([seq.utr5_fold50]', 'VarNames', {'dG_5UTR50'}));
        end
        if isfield(seq, 'utr5_fold50_cds_fold17')
            insert_dataset = horzcat(insert_dataset, dataset([seq.utr5_fold50_cds_fold17]', 'VarNames', {'dG_5UTR50_CDS17'}));
        end
        if isfield(seq, 'utr5_fold50_cds_fold34')
            insert_dataset = horzcat(insert_dataset, dataset([seq.utr5_fold50_cds_fold34]', 'VarNames', {'dG_5UTR50_CDS34'}));
        end
        if isfield(seq, 'utr5_fold50_cds_fold53')
            insert_dataset = horzcat(insert_dataset, dataset([seq.utr5_fold50_cds_fold53]', 'VarNames', {'dG_5UTR50_CDS53'}));
        end
        if isfield(seq, 'utr5_fold_cds_fold17')
            insert_dataset = horzcat(insert_dataset, dataset([seq.utr5_fold_cds_fold17]', 'VarNames', {'dG_5UTR_CDS17'}));
        end
        if isfield(seq, 'utr5_fold_cds_fold34')
            insert_dataset = horzcat(insert_dataset, dataset([seq.utr5_fold_cds_fold34]', 'VarNames', {'dG_5UTR_CDS34'}));
        end
        if isfield(seq, 'utr5_fold_cds_fold53')
            insert_dataset = horzcat(insert_dataset, dataset([seq.utr5_fold_cds_fold53]', 'VarNames', {'dG_5UTR_CDS53'}));
        end
    end
    
    % No dG_full
    
    %if use_utr3 && use_utr5
    %    if isfield(seq, 'full_fold')
    %        insert_dataset = horzcat(insert_dataset, dataset([seq.full_fold]', 'VarNames', {'dG_full'}));
    %    end
    %end
    
    %log_gc = dataset(log2([seq.gc])', 'VarNames', {'LogGC'});
    %log_gc3 = dataset(log2([seq.gc3])', 'VarNames', {'LogGC3'});
    %log_len = dataset(log2([seq.length])', 'VarNames', {'Loglen'});
    %log_nc = dataset(log2([seq.nc])', 'VarNames', {'LogNc'});
    %log_ew = dataset(log2([seq.ew])', 'VarNames', {'LogEw'});
    %log_scuo = dataset(log2([seq.scuo])', 'VarNames', {'LogSCUO'});
    %log_p1 = dataset(log2([seq.p1])', 'VarNames', {'LogP1'});
    %log_p2 = dataset(log2([seq.p2])', 'VarNames', {'LogP2'});
    %log_rcbs = dataset(log2([seq.rcbs])', 'VarNames', {'LogRCBSpc'});
    %log_cai =  dataset(log2([seq.cai])', 'VarNames', {'LogCAI'});
    %log_tai =  dataset(log2([seq.tai])', 'VarNames', {'LogtAI'}); 
    
    y = zeros(1, n);
    nucleotide_usage = zeros(n, length(get_nucs));
    dinucleotide_usage = zeros(n, length(get_nucs) * length(get_nucs));
    codon_usage = zeros(n, length(get_codons));
    amino_usage = zeros(n, length(get_aminos));
    rscu_usage = zeros(n, length(get_codons));
    pair_rscu_usage = zeros(n, length(get_codons) .^ 2);
    
    if strcmpi(mode, 'mRNA-lim-mean')
        for i = 1:n
            y(i) = seq(i).mRNA_lim_mean;
        end
    elseif strcmpi(mode, 'mRNA-lim-max')
        for i = 1:n
            y(i) = seq(i).mRNA_lim_max;
        end
    elseif strcmpi(mode, 'mRNA-lim-min')
        for i = 1:n
            y(i) = seq(i).mRNA_lim_min;
        end
    elseif strcmpi(mode, 'mRNA-Arava')
        for i = 1:n
            y(i) = seq(i).mRNA_Arava;
        end
    elseif strcmpi(mode, 'mRNA-Ingolia')
        for i = 1:n
            y(i) = seq(i).mRNA_Ingolia;
        end
    elseif strcmpi(mode, 'mRNA-count')
        for i = 1:n
            y(i) = seq(i).mRNA_count;
        end
    elseif strcmpi(mode, 'mRNA-seq')
        for i = 1:n
            y(i) = seq(i).mRNA_seq;
        end
    elseif strcmpi(mode, 'mRNA-Lu')
        for i = 1:n
            y(i) = seq(i).mRNA_Lu;
        end
    elseif strcmpi(mode, 'GFP')
        for i = 1:n
            y(i) = seq(i).gfp;
        end
    elseif strcmpi(mode, 'TAP')
        for i = 1:n
            y(i) = seq(i).tap;
        end
    elseif strcmpi(mode, 'Protein-Lu')
        for i = 1:n
            y(i) = seq(i).protein_Lu;
        end
    elseif strcmpi(mode, 'Density-Arava')
        for i = 1:n
            y(i) = seq(i).density_Arava;
        end
    elseif strcmpi(mode, 'Relative-rate-Arava')
        for i = 1:n
            y(i) = seq(i).relative_rate_Arava;
        end
    elseif strcmpi(mode, 'Density-Ingolia')
        for i = 1:n
            y(i) = seq(i).density_Ingolia;
        end
    elseif strcmpi(mode, 'Density-Ingolia-corrected')
        for i = 1:n
            y(i) = seq(i).density_Ingolia_corrected;
        end
    elseif strcmpi(mode, 'Inv-density-Arava')
        for i = 1:n
            y(i) = seq(i).density_Arava ^ -1;
        end
    elseif strcmpi(mode, 'Inv-relative-rate-Arava')
        for i = 1:n
            y(i) = seq(i).relative_rate_Arava ^ -1;
        end
    elseif strcmpi(mode, 'Inv-density-Ingolia')
        for i = 1:n
            y(i) = seq(i).density_Ingolia ^ -1;
        end
    elseif strcmpi(mode, 'Inv-density-Ingolia-corrected')
        for i = 1:n
            y(i) = seq(i).density_Ingolia_corrected ^ -1;
        end
    elseif strcmpi(mode, 'GFP/mRNA-Arava')
        for i = 1:n
            y(i) = seq(i).gfp / seq(i).mRNA_Arava;
        end
    elseif strcmpi(mode, 'GFP/mRNA-Ingolia')
        for i = 1:n
            y(i) = seq(i).gfp / seq(i).mRNA_Ingolia;
        end
    elseif strcmpi(mode, 'GFP/mRNA-count')
        for i = 1:n
            y(i) = seq(i).gfp / seq(i).mRNA_count;
        end
    elseif strcmpi(mode, 'GFP/mRNA-seq')
        for i = 1:n
            y(i) = seq(i).gfp / seq(i).mRNA_seq;
        end
    elseif strcmpi(mode, 'GFP/mRNA-Lu')
        for i = 1:n
            y(i) = seq(i).gfp / seq(i).mRNA_Lu;
        end
    elseif strcmpi(mode, 'TAP/mRNA-Arava')
        for i = 1:n
            y(i) = seq(i).tap / seq(i).mRNA_Arava;
        end
    elseif strcmpi(mode, 'TAP/mRNA-Ingolia')
        for i = 1:n
            y(i) = seq(i).tap / seq(i).mRNA_Ingolia;
        end
    elseif strcmpi(mode, 'TAP/mRNA-count')
        for i = 1:n
            y(i) = seq(i).tap / seq(i).mRNA_count;
        end
    elseif strcmpi(mode, 'TAP/mRNA-seq')
        for i = 1:n
            y(i) = seq(i).tap / seq(i).mRNA_seq;
        end
    elseif strcmpi(mode, 'TAP/mRNA-Lu')
        for i = 1:n
            y(i) = seq(i).tap / seq(i).mRNA_Lu;
        end
    elseif strcmpi(mode, 'Protein-Lu/mRNA-Arava')
        for i = 1:n
            y(i) = seq(i).protein_Lu / seq(i).mRNA_Arava;
        end
   elseif strcmpi(mode, 'Protein-Lu/mRNA-Ingolia')
        for i = 1:n
            y(i) = seq(i).protein_Lu / seq(i).mRNA_Ingolia;
        end
    elseif strcmpi(mode, 'Protein-Lu/mRNA-count')
        for i = 1:n
            y(i) = seq(i).protein_Lu / seq(i).mRNA_count;
        end
    elseif strcmpi(mode, 'Protein-Lu/mRNA-seq')
        for i = 1:n
            y(i) = seq(i).protein_Lu / seq(i).mRNA_seq;
        end
    elseif strcmpi(mode, 'Protein-Lu/mRNA-Lu')
        for i = 1:n
            y(i) = seq(i).protein_Lu / seq(i).mRNA_Lu;
        end
    elseif strcmpi(mode, 'mRNA-Arava*Density-Arava')
        for i = 1:n
            y(i) = seq(i).mRNA_Arava * seq(i).density_Arava;
        end
    elseif strcmpi(mode, 'mRNA-Arava*Relative-rate-Arava')
        for i = 1:n
            y(i) = seq(i).mRNA_Arava * seq(i).relative_rate_Arava;
        end
    elseif strcmpi(mode, 'mRNA-Arava*Density-Ingolia')
        for i = 1:n
            y(i) = seq(i).mRNA_Arava * seq(i).density_Ingolia;
        end
    elseif strcmpi(mode, 'mRNA-Arava*Density-Ingolia-corrected')
        for i = 1:n
            y(i) = seq(i).mRNA_Arava * seq(i).density_Ingolia_corrected;
        end
    elseif strcmpi(mode, 'mRNA-count*Density-Arava')
        for i = 1:n
            y(i) = seq(i).mRNA_count * seq(i).density_Arava;
        end
    elseif strcmpi(mode, 'mRNA-count*Relative-rate-Arava')
        for i = 1:n
            y(i) = seq(i).mRNA_count * seq(i).relative_rate_Arava;
        end
    elseif strcmpi(mode, 'mRNA-count*Density-Ingolia')
        for i = 1:n
            y(i) = seq(i).mRNA_count * seq(i).density_Ingolia;
        end
    elseif strcmpi(mode, 'mRNA-count*Density-Ingolia-corrected')
        for i = 1:n
            y(i) = seq(i).mRNA_count * seq(i).density_Ingolia_corrected;
        end
    elseif strcmpi(mode, 'mRNA-seq*Density-Arava')
        for i = 1:n
            y(i) = seq(i).mRNA_seq * seq(i).density_Arava;
        end
    elseif strcmpi(mode, 'mRNA-seq*Relative-rate-Arava')
        for i = 1:n
            y(i) = seq(i).mRNA_seq * seq(i).relative_rate_Arava;
        end
    elseif strcmpi(mode, 'mRNA-seq*Density-Ingolia')
        for i = 1:n
            y(i) = seq(i).mRNA_seq * seq(i).density_Ingolia;
        end
    elseif strcmpi(mode, 'mRNA-seq*Density-Ingolia-corrected')
        for i = 1:n
            y(i) = seq(i).mRNA_seq * seq(i).density_Ingolia_corrected;
        end
    elseif strcmpi(mode, 'mRNA-Ingolia*Density-Arava')
        for i = 1:n
            y(i) = seq(i).mRNA_Ingolia * seq(i).density_Arava;
        end
    elseif strcmpi(mode, 'mRNA-Ingolia*Relative-rate-Arava')
        for i = 1:n
            y(i) = seq(i).mRNA_Ingolia * seq(i).relative_rate_Arava;
        end
    elseif strcmpi(mode, 'mRNA-Ingolia*Density-Ingolia')
        for i = 1:n
            y(i) = seq(i).mRNA_Ingolia * seq(i).density_Ingolia;
        end
   elseif strcmpi(mode, 'mRNA-Ingolia*Density-Ingolia-corrected')
        for i = 1:n
            y(i) = seq(i).mRNA_Ingolia * seq(i).density_Ingolia_corrected;
        end
   elseif strcmpi(mode, 'mRNA-Lu*Density-Arava')
        for i = 1:n
            y(i) = seq(i).mRNA_Lu * seq(i).density_Arava;
        end
    elseif strcmpi(mode, 'mRNA-Lu*Relative-rate-Arava')
        for i = 1:n
            y(i) = seq(i).mRNA_Lu * seq(i).relative_rate_Arava;
        end
    elseif strcmpi(mode, 'mRNA-Lu*Density-Ingolia')
        for i = 1:n
            y(i) = seq(i).mRNA_Lu * seq(i).density_Ingolia;
        end
    elseif strcmpi(mode, 'mRNA-Lu*Density-Ingolia-corrected')
        for i = 1:n
            y(i) = seq(i).mRNA_Lu * seq(i).density_Ingolia_corrected;
        end
    elseif strcmpi(mode, 'expected-protein-Ingolia')
        for i = 1:n
            y(i) = seq(i).expected_protein_Ingolia;
        end
    elseif strcmpi(mode, 'expected-protein-Ingolia-corrected')
        for i = 1:n
            y(i) = seq(i).expected_protein_Ingolia_corrected;
        end
    else
        error(sprintf('Unknown setting used for mode option: %s', mode));
    end
    
    for i = 1:n
        nucleotide_usage(i, :) = seq(i).nucleotide_usage;
        dinucleotide_usage(i, :) = seq(i).dinucleotide_usage;
        codon_usage(i, :) = seq(i).codon_usage;
        rscu_usage(i, :) = seq(i).rscu;
        amino_usage(i, :) = seq(i).amino_usage;
        pair_rscu_usage(i, :) = seq(i).pair_rscu(:);
    end
    
    codon_usage_ds = dataset();
    codons = get_codons;
    for i = 1:length(codons)
        cur_codon = codons{i};
        cur_codon_ds = dataset(codon_usage(:, i), 'VarNames', {['codon' cur_codon]});
        codon_usage_ds = horzcat(codon_usage_ds, cur_codon_ds);
    end
    
    nucleotides = get_nucs;
    nucleotide_usage_ds = dataset();
    for i = 1:length(nucleotides)
        cur_nuc = nucleotides{i};
        cur_nuc_ds = dataset(nucleotide_usage(:, i), 'VarNames', {['nuc' cur_nuc]});
        nucleotide_usage_ds = horzcat(nucleotide_usage_ds, cur_nuc_ds);
    end
    
    dinucleotide_usage_ds = dataset();
    for i = 1:length(nucleotides)
        for j = 1:length(nucleotides)
            cur_dinuc = [nucleotides{i} nucleotides{j}];
            code = (j - 1) + (i - 1) * 5 + 1;
            cur_dinuc_ds = dataset(dinucleotide_usage(:, code), 'VarNames', {['dinuc' cur_dinuc]});
            dinucleotide_usage_ds = horzcat(dinucleotide_usage_ds, cur_dinuc_ds);
        end
    end
    
    amino_usage_ds = dataset();
    aminos = get_aminos;
    for i = 1:length(aminos)
        cur_amino = aminos{i};
        if cur_amino == '-'
            cur_amino = '--';
        end
        cur_amino_ds = dataset(amino_usage(:, i), 'VarNames', {['amino' cur_amino]});
        amino_usage_ds = horzcat(amino_usage_ds, cur_amino_ds);
    end
    
    rscu_ds = dataset();
    codons = get_codons;
    for i = 1:length(codons)
        cur_codon = codons{i};
        cur_codon_ds = dataset(rscu_usage(:, i), 'VarNames', {['rscu' cur_codon]});
        rscu_ds = horzcat(rscu_ds, cur_codon_ds);
    end
    
    pair_rscu_ds = dataset();
    codons = get_codons;
    for i = 1:length(codons)
        cur_codon_i = codons{i};
        for j = 1:length(codons)
            cur_codon_j = codons{j};
            ind = (j - 1) * length(codons) + i;
            cur_pair_ds = dataset(pair_rscu_usage(:, ind), 'VarNames', {['pair_rscu' cur_codon_j '_' cur_codon_i]});
            pair_rscu_ds = horzcat(pair_rscu_ds, cur_pair_ds);
        end
    end
    
    y = dataset(y', 'VarNames', {mode});
    %ds = horzcat(gc, gc15, gc3, len, tai, cai, rca, nc, ew, scuo, p1, p2, rcbs, nucleotide_usage_ds, amino_usage_ds, rscu_ds, pindex, cpb, tpi, gravy, aroma, aliphatic, instability, side_charge, tai14, tai17, tai19, side_charge4, side_charge11, side_charge15, side_charge40, cai2nd, tai2nd, dG_CDS, dG_CDS17, dG_CDS34, dG_CDS53, pI, weight, charge, fop, cbi, insert_dataset, dnuc, dinucleotide_usage_ds, y);
    %ds = horzcat(gc15, len, tai, cai, nc, ew, p1, rcbs, nucleotide_usage_ds, amino_usage_ds, rscu_ds, cpb, tpi, gravy, aroma, aliphatic, instability, side_charge, tai14, tai17, tai19, side_charge4, side_charge11, side_charge15, side_charge40, cai2nd, tai2nd, dG_CDS, dG_CDS17, dG_CDS34, dG_CDS53, pI, weight, charge, fop, insert_dataset, dnuc, dinucleotide_usage_ds, y);
    %ds = horzcat(gc15, len, tai, cai, nc, ew, p1, rcbs, nucleotide_usage_ds, amino_usage_ds, codon_usage_ds, cpb, tpi, gravy, aroma, aliphatic, instability, side_charge, tai14, tai17, tai19, side_charge4, side_charge11, side_charge15, side_charge40, cai2nd, tai2nd, dG_CDS, dG_CDS17, dG_CDS34, dG_CDS53, pI, weight, charge, fop, insert_dataset, dnuc, dinucleotide_usage_ds, y);
    
    
    % No dG_full, dG_CDS
    ds = horzcat(gc15, len, tai, cai, nc, ew, p1, rcbs, nucleotide_usage_ds, amino_usage_ds, rscu_ds, cpb, tpi, gravy, aroma, aliphatic, instability, side_charge, tai14, tai17, tai19, side_charge4, side_charge11, side_charge15, side_charge40, cai2nd, tai2nd, dG_CDS17, dG_CDS34, dG_CDS53, pI, weight, charge, fop, insert_dataset, dnuc, dinucleotide_usage_ds, y);
    %ds = horzcat(gc15, len, tai, cai, nc, ew, p1, rcbs, nucleotide_usage_ds, amino_usage_ds, codon_usage_ds, cpb, tpi, gravy, aroma, aliphatic, instability, side_charge, tai14, tai17, tai19, side_charge4, side_charge11, side_charge15, side_charge40, cai2nd, tai2nd, dG_CDS17, dG_CDS34, dG_CDS53, pI, weight, charge, fop, insert_dataset, dnuc, dinucleotide_usage_ds, y);
end
