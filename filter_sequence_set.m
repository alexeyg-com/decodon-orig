function [inset] = filter_sequence_set(inset, filters)
    for i = 1:length(filters)
        filter = filters{i};
        sub = [];
        for j = 1:length(inset)
            if ~isempty(inset(j).(filter))
                sub = [sub j];
            end
        end
        inset = inset(sub);
    end
end