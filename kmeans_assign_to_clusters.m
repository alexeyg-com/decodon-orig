function [assignment] = kmeans_assign_to_clusters(clust, single)
    single = double(single(:, 1:end - 1));
    clust = double(clust(:, 1:end - 1));
    
    n = size(single, 1);
    m = size(clust, 1);
    
    assignment = zeros(1, n);
    
    for i = 1:n
        dist = zeros(1, m);
        for j = 1:m
            dist(j) = sum((single(i, :) - clust(j, :)) .^ 2);
        end
        [~, assignment(i)] = min(dist);
    end
    
    %dist = single * clust';
    %[~, assignment] = min(dist, [], 2);
end