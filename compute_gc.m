% Compute GC contect for a given item
%   SEQ  - input sequence
%   GC   - computed GC content
function [gc] = compute_gc(seq)
    nuc = nuc2int(seq);
    arrayLen = length(get_nucs);
    usage = zeros(1, arrayLen);
    
    len = length(nuc);
    for i = 1:len
        usage(nuc(i)) = usage(nuc(i)) + 1;
    end
    gc = (usage(nuc2int('G')) + usage(nuc2int('C'))) / sum(usage);
end