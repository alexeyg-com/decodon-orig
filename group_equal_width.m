function [bins]  = group_equal_width(ds, num_bins)
    y = double(ds(:, end));
    y_num = y(~isinf(y));
    
    mi = min(y_num);
    ma = max(y_num);
    bin_width = (ma - mi) / num_bins;
    
    bins = cell(1, num_bins);
    nonempty = [];
    for i = 1:num_bins
        start = mi + (i - 1) * bin_width;
        stop = mi + i * bin_width;
        if i == num_bins
            stop = ma;
        elseif i == 1
            start = mi - 1e-2;
        end
        
        lo = y > start;
        hi = y <= stop;
        ind = double(lo) .* double(hi) > 0;
        
        %fprintf('Between %ld and %ld : %d\n', start, stop, sum(double(lo) .* double(hi) > 0));
        
        bins{i} = ds(ind, :);
        if any(ind)
            nonempty = [nonempty i];
        end
    end
    %nonempty
    bins = bins(nonempty);
end