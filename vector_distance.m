% Compute distance between two vectors
%   ACTUAL    - vector of real values
%   PREDICTED - vector of predicted values
%   TYPE      - distance type (MSE, R2)
%
%   DIST      - computed distance
function [dist] = vector_distance(actual, predicted, type)
    if nargin < 3
        type = 'mse';
    end
    if strcmpi(type, 'mse')
        dist = mean((actual - predicted) .^ 2);
    elseif strcmpi(type, 'r2')
        avg = mean(actual);
        SStot = mean((actual - avg) .^ 2);
        %SSreg = mean((predicted - avg) .^ 2);
        SSerr = mean((actual - predicted) .^ 2);
        dist = 1 - SSerr / SStot;
    end
end