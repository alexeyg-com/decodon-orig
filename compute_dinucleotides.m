function [di] = compute_dinucleotides(seq)
    seq = nuc2int(seq) - 1;
    di = zeros(1, 25);
    n = length(seq);
    for i = 1:n - 1
        code = seq(i) + seq(i + 1) * 5;
        di(code + 1) = di(code + 1) + 1;
    end
    di = di / (n - 1);
end