function [set] = attach_ingolia(inset, fileName, fileNameLength)
    if nargin < 2
        fileName = 'import/Ingolia.xls';
    end
    if nargin < 3
        fileNameLength = 'import/Ingolia_length.xls';
    end
    [correction_factor, ~] = xlsread(fileNameLength);
    correction_factor = correction_factor(:, 2);
    max_correction_length = length(correction_factor);
    
    [levels, names] = xlsread(fileName);
    names = names(3:end, 1);
    feature_len = levels(2:end, 1);
    ribo_a = levels(2:end, 2);
    ribo_b = levels(2:end, 3);
    mRNA_a = levels(2:end, 4);
    mRNA_b = levels(2:end, 5);
    
    mRNA_ind = (mRNA_a + mRNA_b) >= 128;
    ribo_ind = (ribo_a + ribo_b) >= 128;
    translation_a_ind = (mRNA_a + ribo_a) >= 128;
    translation_b_ind = (mRNA_b + ribo_b) >= 128;
    
    %sum(translation_a_ind)
    %sum(translation_b_ind)
    %sum(translation_a_ind + translation_b_ind == 2)
    
    norm_ribo_a = levels(1, 2);
    norm_ribo_b = levels(1, 3);
    norm_mRNA_a = levels(1, 4);
    norm_mRNA_b = levels(1, 5);
    
    ribo_a = (ribo_a ./ feature_len) / norm_ribo_a; % for ALL mRNA molecules
    ribo_b = (ribo_b ./ feature_len) / norm_ribo_b;
    mRNA_a = (mRNA_a ./ feature_len) / norm_mRNA_a;
    mRNA_b = (mRNA_b ./ feature_len) / norm_mRNA_b;
    
    translation_a = ribo_a ./ mRNA_a; % per mRNA molecule (hence density)
    translation_b = ribo_b ./ mRNA_b;
    
    norm_a_indices = (double(mRNA_a ~= 0) .* double(translation_a_ind)) == 1;
    norm_b_indices = (double(mRNA_b ~= 0) .* double(translation_b_ind)) == 1;
    
    translation_a = translation_a / mean(translation_a(norm_a_indices));
    translation_b = translation_b / mean(translation_b(norm_b_indices));
    
    ribo = mean([ribo_a'; ribo_b']);
    mRNA = mean([mRNA_a'; mRNA_b']);
    translation = mean([translation_a'; translation_b']);
    
    ribo_a_corrected = ribo_a;
    ribo_b_corrected = ribo_b;
    
    set = inset;
    map_names = strtok({inset.desc});
    total = size(feature_len, 1);
    missing = 0;
    unreliable_mRNA = 0;
    unreliable_ribo = 0;
    unreliable_translation = 0;
    for i = 1:total;
        name = names{i};
        ind = find(strcmpi(name, map_names), 1);
        if ~isempty(ind)
            if mRNA_ind(i)
                set(ind).mRNA_Ingolia = mRNA(i);
            else
                unreliable_mRNA = unreliable_mRNA + 1;
                set(ind).mRNA_Ingolia = [];
            end
            if ~isnan(ribo(i)) && ~isinf(ribo(i))
                if ribo_ind(i)
                    set(ind).expected_protein_Ingolia = ribo(i);
                else
                    set(ind).expected_protein_Ingolia = [];
                    unreliable_ribo = unreliable_ribo + 1;
                end
                cds_len = min(length(set(ind).seq) / 3, max_correction_length);
                ribo_a_corrected(i) = ribo_a(i) / correction_factor(cds_len);
                ribo_b_corrected(i) = ribo_b(i) / correction_factor(cds_len);
            end
            if ~isnan(translation(i)) && ~isinf(translation(i))
                if translation_a_ind(i) && translation_b_ind(i)
                    set(ind).density_Ingolia = translation(i); % To make it comparable to Arava
                else
                    set(ind).density_Ingolia = [];
                    unreliable_translation = unreliable_translation + 1;
                end
            end
        else
            fprintf('Unable to find CDS %s\n', name);
            missing = missing + 1;
        end
    end
    
    ribo_corrected = mean([ribo_a_corrected'; ribo_b_corrected']);
    translation_a_corrected = ribo_a_corrected ./ mRNA_a; % per mRNA molecule (hence density)
    translation_b_corrected = ribo_b_corrected ./ mRNA_b;
    
    translation_a_corrected = translation_a_corrected / mean(translation_a_corrected(norm_a_indices));
    translation_b_corrected = translation_b_corrected / mean(translation_b_corrected(norm_b_indices));
    translation_corrected = mean([translation_a_corrected'; translation_b_corrected']);
    
    for i = 1:total;
        name = names{i};
        ind = find(strcmpi(name, map_names), 1);
        if ~isempty(ind)
            if ~isnan(ribo_corrected(i)) && ~isinf(ribo_corrected(i))
                if ribo_ind(i)
                    set(ind).expected_protein_Ingolia_corrected = ribo_corrected(i);
                else
                    set(ind).expected_protein_Ingolia_corrected = [];
                end
            end
            if ~isnan(translation_corrected(i)) && ~isinf(translation_corrected(i))
                if translation_a_ind(i) && translation_b_ind(i)
                    set(ind).density_Ingolia_corrected = translation_corrected(i); % To make it comparable to Arava
                else
                    set(ind).density_Ingolia_corrected = [];
                end
            end
        end
    end
    
    fprintf('Total: %d, Missing: %d\n', total, missing);
    fprintf('Unreliable mRNA: %d\n', unreliable_mRNA);
    fprintf('Unreliable EPI: %d\n', unreliable_ribo);
    fprintf('Unreliable translation: %d\n', unreliable_translation);
end