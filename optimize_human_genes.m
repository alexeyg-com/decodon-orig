function [results] = optimize_human_genes()
    save_filename = 'human_genes_addition.mat';
    filename = 'data/human_genes.fasta';
    utr5 = 'CCAGAACTTAGTTTCGACGGATTCTAGAACTAGT';
    utr3 = 'CTCGAGTCATGTAATTAGTTATGTCACGCTTACATTCACGCCCTCCCCCCACATCCGCTCTAACCGAAAAGGAAGGAG';
    pred = Predictor('epi-rscu-dG-cut');
    restriction_sites = {'CTCGAG', 'ACTAGT'};
    max_generations = 20;
    
    fasta = fastaread(filename);
    results = cell(1, length(fasta));
    %for i = 1:length(fasta)
    for i = 6:6
        seq.utr3 = utr3;
        seq.utr5 = utr5;
        seq.seq = fasta(i).Sequence;
        
        fprintf('Processing gene %d : %s (population size: %d)\n', i, fasta(i).Header, length(seq.seq) / 3);
        nsga = NSGA2(pred, length(seq.seq) / 3, max_generations);
        nsga = nsga.optimize(seq, restriction_sites);
        results{i} = nsga;
        %results{i} = pred.predict_value(seq);
        save(save_filename, 'results');
        %results{i} = pred.predict_value(seq);
        %fprintf('Prediction: %.10f\n', results{i});
    end
end
