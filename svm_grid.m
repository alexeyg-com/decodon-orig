function [grid] = svm_grid(ds, K, range, range_nu)
    if nargin < 2
        K = 10;
    end
    
    if nargin < 3
        range = [0.001 0.003 0.009 0.0270 0.2430 0.7290 1 2.1870 6.5610 19.6830 59.0490 177.1470 531.4410];
    end
    
    if nargin < 4
        range_nu = [0.001 0.003 0.009 0.0270 0.2430 0.7290];
    end

    X = double(ds(:, 1:end - 1));
    y = double(ds(:, end));
    X = zscore(X);
    y = zscore(y);
    
    n_range = length(range);
    n_range_nu = length(range_nu);
    grid = cell(1, n_range  * (n_range + n_range_nu) * (5 * 2 * n_range + n_range));
    
    item.CV = K;
    k = 0;
    for type_s = 3:4 % Type of regression
        item.s = type_s;
        for cost_c = range % Cost parameter
            item.c = cost_c;
            
            if type_s == 3
                my_range = range;
            else
                my_range = range_nu;
            end
            
            for nu_epsilon = my_range % Regression nu and epsilon parameters
                item.nu_epsilon = nu_epsilon;
                
                item.t = 1; % Polynomial kernel
                for degree_d = 1:5 % Kernel degree
                    item.d = degree_d;
                    for coef_r = 0:1 % Polynomial kernel coefficient
                        item.r = coef_r;
                        for coef_gamma = range % Kernel coefficient gamma
                            item.g = coef_gamma;
                            k = k + 1;
                            grid{k} = item;
                        end
                    end
                end
                
                item.t = 2; % RBF kernel
                item.r = 0;
                item.d = 0;
                for coef_gamma = range % Kernel coefficient gamma
                    item.g = coef_gamma;
                    k = k + 1;
                    grid{k} = item;
                end
            end
        end
    end
    
    n_items = length(grid);
    fprintf('Need to perform %d cross-validations.\n', n_items);
    
    parfor i = 1:n_items
        item = grid{i};
        
        grid{i}.exec_string = sprintf('-s %d -t %d -d %d -g %.4f -r %.4f -c %.4f -n %.4f -p %.4f -v %d -q', item.s, item.t, item.d, item.g, item.r, item.c, item.nu_epsilon, item.nu_epsilon, item.CV);
        grid{i}.r2 = svmtrain(y, X, grid{i}.exec_string);
        %fprintf('About to execute: %s\n', grid{i}.exec_string);
        fprintf('R2: %5.2f (for %s)\n', grid{i}.r2, grid{i}.exec_string);
    end
end
