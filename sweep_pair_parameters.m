function [res_all] = sweep_pair_parameters(ds, seq, go)
    res_all = [];
    i = 0;
    for lambda = [0.5:0.1:0.9]
        [go_sim, go_min] = compute_go_similarity_matrix(seq, go, lambda);
        for sim_thr = [0.8:0.05:0.95]
            for go_thr = [2:1:4] %[5:1:10]
                [i, j, ds_pair] = make_paired_dataset(ds, go_sim, go_min, sim_thr, go_thr);
                [mu, sigma, errors] = regression_cv(ds_pair, 'r2', 10, 'normal', 'identity', 'linear', [], false);
                i = i + 1;
                res.lambda = lambda;
                res.mu = mu;
                res.sigma = sigma;
                res.errors = errors;
                res.sim_thr = sim_thr;
                res.go_thr = go_thr;
                res.pairs = size(ds_pair, 1);
                res_all = [res_all res];
            end
        end
    end
end