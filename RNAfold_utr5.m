function [energy] = RNAfold_utr5(utr5, part)
    if nargin >= 2
        part = min(part, length(utr5));
        utr5 = utr5(end - part + 1:end);
    end
    
    energy = RNAfold(utr5);
end