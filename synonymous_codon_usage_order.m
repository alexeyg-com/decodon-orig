% Compute synonymous codon usage order
%   SEQ  - input sequence
%
%   SCUO - computed index
function [scuo] = synonymous_codon_usage_order(seq)
    amino_count = zeros(1, length(get_aminos));
    codon_count = zeros(1, length(get_codons));
    
    codonInt = codon2int(seq);
    aminoInt = codon2amino_int(seq);
    n = length(codonInt);
    for i = 1:n
        codon_count(codonInt(i)) = codon_count(codonInt(i)) + 1;
        amino_count(aminoInt(i)) = amino_count(aminoInt(i)) + 1;
    end
    
    trans = amino2codons_int;
    scuo = 0;
    amino_count = amino_count / sum(amino_count);
    for i = 1:length(amino_count)
        codons = trans{i};
        s = sum(codon_count(codons));
        if s > 0
            codon_count(codons) = codon_count(codons) / s;
        end
        ka = length(codons);
        if (ka > 1) && (s > 0)
            Ha = 0;
            for j = 1:ka
                % Sum only over present codons!
                if codon_count(codons(j)) > 0
                    Ha = Ha - codon_count(codons(j)) * log2(codon_count(codons(j)));
                end
            end
            %Ha = -sum(codon_count(codons) .* log2(codon_count(codons)))
            Ea = (log2(ka) - Ha) / log2(ka);
            scuo = scuo + amino_count(i) * Ea;
        end
    end
end