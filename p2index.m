% Compute the P2 index
%   SEQ - input sequence
%   P2  - computed index
function [p2] = p2index(seq)
    WWC = 0;
    SSU = 0;
    WWY = 0;
    SSY = 0;
    
    n = length(seq);
    for i = 1:3:n
        a = seq(i);
        b = seq(i + 1);
        c = seq(i + 2);
        
        if is_weak(a) && is_weak(b)
            if c == 'C'
                WWC = WWC + 1;
            end
            if is_pyromidine(c)
                WWY = WWY + 1;
            end
        elseif is_strong(a) && is_strong(b)
            if is_purine(c)
                SSU = SSU + 1;
            else
                SSY = SSY + 1;
            end
        end
    end;
    
    p2 = (WWC + SSU) / (WWY + SSY);
end

% Is nucleotide a purine?
%   CH  - input nucleotide
%   RES - answer
function [res] = is_purine(ch)
    res = ch == 'A' || ch == 'G';
end

% Is nucleotide a pyromidine?
%   CH  - input nucleotide
%   RES - answer
function [res] = is_pyromidine(ch)
    res = ch == 'C' || ch == 'T';
end

% Is nucleotide strong?
%   CH  - input nucleotide
%   RES - answer
function [res] = is_strong(ch)
    res = ch == 'C' || ch == 'G';
end

% Is nucleotide weak?
%   CH  - input nucleotide
%   RES - answer
function [res] = is_weak(ch)
    res = ch == 'A' || ch == 'T';
end