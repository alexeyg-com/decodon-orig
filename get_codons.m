% Returns a list of all codons (including the unknown codon)
%   CODONS - returned cell array
function [codons] = get_codons
    codons = {'GGG', 'GGT', 'GGC', 'GGA', 'GTG', 'GTT', 'GTC', 'GTA', 'GCG', 'GCT', 'GCC', 'GCA', 'GAG', 'GAT', 'GAC', 'GAA', 'TGG', 'TGT', 'TGC', 'TGA', 'TTG', 'TTT', 'TTC', 'TTA', 'TCG', 'TCT', 'TCC', 'TCA',                 'TAG', 'TAT', 'TAC',                 'TAA', 'CGG', 'CGT', 'CGC', 'CGA', 'CTG', 'CTT', 'CTC', 'CTA', 'CCG', 'CCT', 'CCC', 'CCA', 'CAG', 'CAT', 'CAC', 'CAA', 'AGG', 'AGT', 'AGC', 'AGA', 'ATG', 'ATT', 'ATC', 'ATA', 'ACG', 'ACT', 'ACC', 'ACA', 'AAG', 'AAT', 'AAC', 'AAA', '---'};
end