function [pI] = isoelectric_point(seq)
    seq = nt2aa(seq);
    [pI] = isoelectric(seq);
end