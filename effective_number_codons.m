% Compute the effective number of codons (EN) using
%   SEQ - input sequence
%
%   EN  - the computed number of codons
function [en] = effective_number_codons(seq)
    % multiply codon usage
    mult = 10 ^ 6;
    
    arrayLen = length(get_codons);
    codons = codon2int(seq);
    
    usage = zeros(1, arrayLen);
    for i = 1:length(codons)
        usage(codons(i)) = usage(codons(i)) + 1;
    end
    
    nc = effective_number_write(usage);
    if nc >= 0
        nc_inf = effective_number_write(usage .* mult);
        nc_f4_inf = effective_number_f4(usage .* mult);
        eps = nc_inf - nc_f4_inf;
        en = nc - eps;
    else
        en = effective_number_f4(usage);
    end
    en = min(en, 61);
end

function [f] = get_average_f(ind, Faa)
    %ids = amino2int(names);
    Faa = Faa(ind);
    Faa = Faa(Faa ~= -1);
    
    if isempty(Faa)
        f = -1;
    else
        f = mean(Faa);
    end
end

function [f4] = effective_number_f4(usage)
    aminoAcids = amino2codons;
    aminoAcids = aminoAcids(1:length(aminoAcids) - 1);
    
    p = zeros(1, length(aminoAcids));
    Faa = zeros(1, length(aminoAcids));
    DU = zeros(1, length(aminoAcids));
    NCaa = zeros(1, length(aminoAcids));
    
    for i = 1:length(aminoAcids)
        codonsForAcid = aminoAcids{i};
        nCodons = length(codonsForAcid);
        ints = zeros(1, nCodons);
        for j = 1:nCodons
            ints(j) = codon2int(codonsForAcid{j});
        end
        n = sum(usage(ints));
        p(ints) = usage(ints) ./ max(n, 1);
        if n <= 1
            Faa(i) = -1;
            NCaa(i) = -1;
            DU(i) = -1;
        else
            Faa(i) = (n * (p(ints) * p(ints)') - 1) / (n - 1);
            if Faa(i) == 0
                DU(i) = -1;
                NCaa(i) = -1;
                continue;
            end
            NCaa(i) = 1 / Faa(i);
            if nCodons == 1
                DU(i) = 0;
                NCaa(i) = 1;
            else
                DU(i) = (NCaa(i) - 1) / (nCodons - 1);
            end
        end
    end
    
    DUavg = mean(DU(DU >= 0));
    for i = 1:length(aminoAcids)
        if DU(i) < 0
            codonsForAcid = aminoAcids{i};
            nCodons = length(codonsForAcid);
            NCaa(i) = DUavg * (nCodons - 1) + 1;
        end
    end
    
    f4 = min(sum(NCaa), 61);
end

function [nc] = effective_number_write(usage)
    aminoAcids = amino2codons;
    aminoAcids = aminoAcids(1:length(aminoAcids) - 1);
    
    p = zeros(1, length(aminoAcids));
    Faa = zeros(1, length(aminoAcids));
    
    for i = 1:length(aminoAcids)
        codonsForAcid = aminoAcids{i};
        nCodons = length(codonsForAcid);
        ints = zeros(1, nCodons);
        for j = 1:nCodons
            ints(j) = codon2int(codonsForAcid{j});
        end
        n = sum(usage(ints));
        p(ints) = usage(ints) ./ max(n, 1);
        if n <= 1
            Faa(i) = -1;
        else
            Faa(i) = (n * (p(ints) * p(ints)') - 1) / (n - 1);
        end
    end
    
    f2 = amino2int('NDCQEHKFY');
    f3 = amino2int('I');
    f4 = amino2int('AGPTV');
    f6 = amino2int('RLS');
    
    f2 = get_average_f(f2, Faa);
    f3 = get_average_f(f3, Faa);
    f4 = get_average_f(f4, Faa);
    f6 = get_average_f(f6, Faa);
    
    if (f3 <= 0)
        f3 = mean([f2 f4]);
    end
    
    if (f2 <= 0) || (f4 <= 0) || (f6 <=0)
        nc = -1;
    else
        nc = min(2 + 9 / f2 + 1 / f3 + 5 / f4 + 3 / f6, 61);
    end
end
