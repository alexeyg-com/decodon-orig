function [set] = attach_belle(inset, fileName)
    if nargin < 2
        fileName = 'import/Belle.xls';
    end
    [half_lives, names] = xlsread(fileName);
    half_lives = half_lives(13:end, 3);
    names = names(13:end, 1);
    
    non_nan = find(~isnan(half_lives));
    half_lives = half_lives(non_nan);
    names = names(non_nan);
    
    set = inset;
    map_names = strtok({inset.desc});
    total = size(half_lives, 1);
    missing = 0;
    for i = 1:total;
        name = names{i};
        %fprintf('%d %s\n', i, name);
        ind = find(strcmpi(name, map_names), 1);
        if ~isempty(ind)
            set(ind).half_life = half_lives(i);
        else
            fprintf('Unable to find CDS %s\n', name);
            missing = missing + 1;
        end
    end
    
    fprintf('Total: %d, Missing: %d\n', total, missing);
end