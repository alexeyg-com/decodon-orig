function [aliphatic] = aliphatic_index(seq)
    arrayLen = length(get_aminos);
    aminos = codon2amino_int(codon2int(seq));
    
    usage = zeros(1, arrayLen);
    for i = 1:length(aminos) - 1
        usage(aminos(i)) = usage(aminos(i)) + 1;
    end
    usage = usage / sum(usage);
    
    a = 2.9;
    b = 3.9;
    aliphatic = (usage(1) + a * usage(20) + b * (usage(10) + usage(11))) * 100;
end