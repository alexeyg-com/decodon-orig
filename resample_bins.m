function [ds] = resample_bins(bins, num_samples)
    ds = [];
    n_bins = size(bins, 2);
    bin_size = zeros(1, n_bins);
    bin_avail = cell(1, n_bins);
    for i = 1:n_bins
        bin_size(i) = size(bins{i}, 1);
        bin_avail{i} = true(1, bin_size(i));
    end
    num_samples = min(num_samples, sum(bin_size));
    bin_left = bin_size;
    
    cur_sample = 0;
    while cur_sample < num_samples
        bin_id = randi(n_bins, 1);
        if bin_left(bin_id) == 0
            continue;
        end
        cur_sample = cur_sample + 1;
        bin = bins{bin_id};
        item_id = randi(bin_left(bin_id), 1);
        avail = find(bin_avail{bin_id});
        item_id = avail(item_id);
        if ~isempty(ds)
            ds = [ds; bin(item_id, :)];
        else
            ds = bin(item_id, :);
        end
    end
end