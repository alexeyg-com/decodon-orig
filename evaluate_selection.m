function [evaluations] = evaluate_selection(ds, num_features, poly, distr_types, link_types)
    if nargin < 3
        poly = 1;
    end
    if nargin < 4
        distr_types = {'gamma', 'inverse gaussian', 'normal'};
    end
    if nargin < 4
        link_types = {'identity', 'log', 'reciprocal', 'reciprocal'};
    end
    
    % First exapand the features into combinations
    ds = expand_dataset(ds, poly);
    
    % Now proceed with experiments
    evaluations = perform_evaluation(ds, num_features, distr_types, link_types);
end

function [prod] = expand_dataset(ds, poly)
    n = size(ds, 2);
    y = ds(:, end);
    ds = ds(:, 1:n - 1);
    
    if poly == 0
        prod = y;
    else
        n = n - 1;
        stopper = 1:n;
        cur_feat = n;
        prod = ds;
        for i = 2:poly
           new_feat = 0;
           new_stopper = [];
           for j = 1:cur_feat
               for k = stopper(j):n
                   feature = double(prod(:, j)) .* double(ds(:, k));
                   feature_name = [prod.Properties.VarNames{j} 'x' ds.Properties.VarNames{k}];
                   prod = horzcat(prod, dataset(feature, 'VarNames', {feature_name}));
                   new_stopper = [new_stopper k];
                   new_feat = new_feat + 1;
               end
           end
           stopper = [stopper new_stopper];
           cur_feat = cur_feat + new_feat;
        end
        prod = horzcat(prod, y);
    end
end

function [evaluations] = perform_evaluation(ds, num_features, distr_types, link_types)
    ndistr = length(distr_types);
    ntypes = length(link_types);
    evaluations = [];
    for i = 1:ndistr
        for j = 1:ntypes
            eval_dl = perform_evaluation_dl(ds, num_features, distr_types{i}, link_types{j});
            if isempty(evaluations)
                evaluations = eval_dl;
            else
                evaluations = [evaluations eval_dl];
            end
        end
    end
end

function [evaluations] = perform_evaluation_dl(ds, num_features, distr, link)
    n_feat = size(ds, 2) - 1;
    y = double(ds(:, n_feat + 1));
    allowed = true(1, n_feat);
    cur_set = [];
    evaluations = [];
    for i = 1:num_features
        bestStat = [];
        bestInd = -1;
        for j = 1:n_feat
            if allowed(j)
                eval = evaluate_dl(ds(:, [cur_set j]), y, distr, link);
                if bestInd < 0 || eval.dev < bestStat.dev
                    bestInd = j;
                    bestStat = eval;
                end
            end
        end
        cur_set = [cur_set bestInd];
        allowed(bestInd) = false;
        if isempty(evaluations)
            evaluations = bestStat;
        else
            evaluations = [evaluations bestStat];
        end
    end
end

function [evaluation] = evaluate_dl(X, y, distr, link)
    [evaluation.b, evaluation.dev, evaluation.stats] = glmfit(double(X), double(y), distr, 'link', link, 'constant', 'on');
    evaluation.features = X.Properties.VarNames;
    evaluation.distr = distr;
    evaluation.link = link;
end