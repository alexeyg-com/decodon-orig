% Computes correlation for each of the features in the dataset after
% expanding it
%   DS   - dataset
%   POLY - degree of polynomial to be used for expansion
%
%   FEAT - feature index
%   CORRELATION - computed Peason correlation
%   AGGR - structure describing the FEAT and CORRELATION
%   DS   - expanded dataset
function [feat correlation aggr ds] = sort_features(ds, poly)
    if nargin < 2
        poly = 2;
    end
    ds = expand_dataset(ds, poly);
    y = double(ds(:, end));
    n = size(ds, 2) - 1;
    arr = zeros(n, 3);
    arr(:, 1) = 1:n;
    for i = 1:n
        arr(i, 2) = corr(double(ds(:, i)), y);
        arr(i, 3) = abs(arr(i, 2));
    end
    %arr = sortrows(arr, -3);
    feat = arr(:, 1);
    correlation = arr(:, 2);
    for i = 1:n
        aggr(i).Name = ds.Properties.VarNames{feat(i)};
        aggr(i).Corr = correlation(i);
    end
end

% Expands dataset with polynomial features
%   DS   - dataset to expand
%   POLY - degree of polynomial to use
%
%   PROD - expanded dataset
function [prod] = expand_dataset(ds, poly)
    n = size(ds, 2);
    y = ds(:, n);
    ds = ds(:, 1:n - 1);
    
    if poly == 0
        prod = y;
    else
        n = n - 1;
        stopper = 1:n;
        cur_feat = n;
        prod = ds;
        for i = 2:poly
           new_feat = 0;
           new_stopper = [];
           for j = 1:cur_feat
               for k = stopper(j):n
                   feature = double(prod(:, j)) .* double(ds(:, k));
                   feature_name = [prod.Properties.VarNames{j} 'x' ds.Properties.VarNames{k}];
                   prod = horzcat(prod, dataset(feature, 'VarNames', {feature_name}));
                   new_stopper = [new_stopper k];
                   new_feat = new_feat + 1;
               end
           end
           stopper = [stopper new_stopper];
           cur_feat = cur_feat + new_feat;
        end
        prod = horzcat(prod, y);
    end
end