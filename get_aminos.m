% Returns a list of all amino acids (including the 'stop' acid and an
% unknown acid)
%   AMINO - returned cell array
function [amino] = get_aminos
    amino = {'A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V', '*', '-'};
end