% Selects a subset of highly expressed genes based on genes known to be
% highly expressed
%   SEQ - input set of genes
%   HL  - indices of genes known to be highly expressed
%
%   SEL - selected highly expressed genes.
function [sel] = select_highly(seq, hl)
    % Get indices from names
    m = length(hl);
    gene_names = strtok({seq.desc});
    hl_ind = [];
    for i = 1:m
        searchInd = find(strcmpi(hl{i}, gene_names));
        if ~isempty(searchInd)
            hl_ind = [hl_ind searchInd'];
        else
            fprintf('Not found: %s\n', hl{i});
        end
    end
    
    % Create 'highlight' subset
    sub = seq(hl_ind);
    m = length(sub);
    mu = zeros(1, m);
    sigma = zeros(1, m);
    ww = 1;
    for i = 1:m
        mu(i) = sub(i).e_mean(ww);
        sigma(i) = sub(i).e_std(ww);
    end
    
    badInd = find(isnan(mu));
    for i = 1:length(badInd)
        name = strtok(sub(badInd(i)).probe_desc);
        fprintf('Gene %s has NaN expression - ignoring\n', name);
    end
    
    fixInd = find(~isnan(mu));
    mu = mu(fixInd);
    sigma = sigma(fixInd);
    
    mean_sigma = mean(sigma);
    min_mu = min(mu);
    
    sel = [];
    n = length(seq);
    for i = 1:n
        if (seq(i).e_mean(ww) - min_mu >= - mean_sigma) && (seq(i).e_std(ww) <= 2 * mean_sigma)
            sel = [sel seq(i)];
        end
    end
    
    fprintf('Minimum: %f, Mean std: %f\n', min_mu, mean_sigma);
    
    m = length(mu);
    for i = 1:m
        if ~ ((mu(i) - min_mu >= - mean_sigma) && (sigma(i) <= 3 * mean_sigma))
            fprintf('Not selecting reference %f +- %f\n', mu(i), sigma(i));
        end
    end
end