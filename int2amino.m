% Converts integer representation of amino acids to a string.
%   INTS       - input integer array
%   AMINOACIDS - output amino acid representation
function [aminoAcid] = int2amino(ints)
    aminoAcids = get_aminos;
    aminoCount = length(aminoAcids);
    len = length(ints);
    aminoAcid = '';
    for i = 1:len
        aminoInt = ints(i);
        if aminoInt <= 0 || aminoInt > aminoCount
            continue;
        end
        aminoAcid = [aminoAcid aminoAcids{aminoInt}];
    end
end