function [set] = attach_newman(inset, fileName)
    if nargin < 2
        fileName = 'import/Newman.xls';
    end
    [pre_levels, names] = xlsread(fileName);
    pre_levels = [NaN(2, 2); pre_levels(1:end, [3 5])];
    names = names(5:end, 1);
    levels = pre_levels(:, 1);
    levels(isnan(levels)) = pre_levels(isnan(levels), 2);
    
    non_nan = find(~isnan(levels));
    levels = levels(non_nan);
    names = names(non_nan);
    
    set = inset;
    map_names = strtok({inset.desc});
    total = size(levels, 1);
    missing = 0;
    for i = 1:total;
        name = names{i};
        ind = find(strcmpi(name, map_names), 1);
        if ~isempty(ind)
            set(ind).gfp = levels(i);
        else
            fprintf('Unable to find CDS %s\n', name);
            missing = missing + 1;
        end
    end
    
    fprintf('Total: %d, Missing: %d\n', total, missing);
end
