% Return array of codon ids encoding corresponding amino acids
%   ARRAY - cell array of codon ids
function [array] = amino2codons_int()
    init = amino2codons;
    for i = 1:length(init)
        array{i} = [];
        for j = 1:length(init{i})
            array{i} = [array{i} codon2int(init{i}{j})];
        end
    end
end