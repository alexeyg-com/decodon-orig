% Compute features for a given set of seuqences
%   INSET - input set of sequences
%   SET   - output set of sequences with computed features
function [set] = compute_features(inset, varargin)
    computeGO = false;
    computeNucleotideUsage = false;
    computeCodonUsage = false;
    computeAminoUsage = false;
    computeRSCU = false;
    computePairRSCU = false;
    computeGC = false;
    compute15GC = false;
    computeGC3 = false;
    computeLength = false;
    computeNc = false;
    computeEw = false;
    computeSCUO = false;
    computeP1 = false;
    computeP2 = false;
    computeRCBS = false;
    computeCAI = false;
    computetAI = false;
    computetAI14 = false;
    computetAI17 = false;
    computetAI19 = false;
    computetAI51 = false;
    computeRCA = false;
    computeP = false;
    computeCPB = false;
    computeTPI = false;
    computeUTR3len = false;
    computeUTR5len = false;
    computeGRAVY = false;
    computeAROMA = false;
    computeAliphatic = false;
    computeII = false;
    computeSideCharge = false;
    computeSideCharge4 = false;
    computeSideCharge11 = false;
    computeSideCharge15 = false;
    computeSideCharge40 = false;
    computeCAI2nd = false;
    computetAI2nd = false;
    computeCDSfold = false;
    computeCDSfold17 = false;
    computeCDSfold34 = false;
    computeCDSfold53 = false;
    computeUTR3fold = false;
    computeUTR5fold = false;
    computeUTR5fold50 = false;
    computeUTR5fold50CDSfold17 = false;
    computeUTR5fold50CDSfold34 = false;
    computeUTR5fold50CDSfold53 = false;
    computeUTR5foldCDSfold17 = false;
    computeUTR5foldCDSfold34 = false;
    computeUTR5foldCDSfold53 = false;
    computeUTR5foldCDSfold = false;
    computepI = false;
    computeMolWeight = false;
    computeCharge = false;
    computeFullfold = false;
    computeFop = false;
    computeCBI = false;
    computeDnuc = false;
    computeDinucleotide = false;

    for i = 2:nargin
        arg = varargin{i - 1};
        if ischar(arg)
            if strcmpi(arg, 'GO') || strcmpi(arg, 'all')
                computeGO = true;
            end
            if strcmpi(arg, 'nucleotide') || strcmpi(arg, 'all')
                computeNucleotideUsage = true;
            end
            if strcmpi(arg, 'dinucleotide') || strcmpi(arg, 'all')
                computeDinucleotide = true;
            end
            if strcmpi(arg, 'codon') || strcmpi(arg, 'all')
                computeCodonUsage = true;
            end
            if strcmpi(arg, 'amino') || strcmpi(arg, 'all')
                computeAminoUsage = true;
            end
            if strcmpi(arg, 'rscu') || strcmpi(arg, 'all')
                computeRSCU = true;
            end
            if strcmpi(arg, 'pairrscu') || strcmpi(arg, 'all')
                computePairRSCU = true;
            end
            if strcmpi(arg, 'gc') || strcmpi(arg, 'all')
                computeGC = true;
            end
            if strcmpi(arg, '15gc') || strcmpi(arg, 'all')
                compute15GC = true;
            end
            if strcmpi(arg, 'gc3') || strcmpi(arg, 'all')
                computeGC3 = true;
            end
            if strcmpi(arg, 'length') || strcmpi(arg, 'all')
                computeLength = true;
            end
            if strcmpi(arg, 'nc') || strcmpi(arg, 'all')
                computeNc = true;
            end
            if strcmpi(arg, 'ew') || strcmpi(arg, 'all')
                computeEw = true;
            end
            if strcmpi(arg, 'scuo') || strcmpi(arg, 'all')
                computeSCUO = true;
            end
            if strcmpi(arg, 'p1') || strcmpi(arg, 'all')
                computeP1 = true;
            end
            if strcmpi(arg, 'p2') || strcmpi(arg, 'all')
                computeP2 = true;
            end
            if strcmpi(arg, 'rcbs') || strcmpi(arg, 'all')
                computeRCBS = true;
            end
            if strcmpi(arg, 'cai') || strcmpi(arg, 'all')
                computeCAI = true;
            end
            if strcmpi(arg, 'tai') || strcmpi(arg, 'all')
                computetAI = true;
            end
            if strcmpi(arg, 'rca') || strcmpi(arg, 'all')
                computeRCA = true;
            end
            if strcmpi(arg, 'P') || strcmpi(arg, 'all')
                computeP = true;
            end
            if strcmpi(arg, 'CPB') || strcmpi(arg, 'all')
                computeCPB = true;
            end
            if strcmpi(arg, 'TPI') || strcmpi(arg, 'all')
                computeTPI = true;
            end
            if strcmpi(arg, 'UTR3len') || strcmpi(arg, 'utr3') || strcmpi(arg, 'utr') || strcmpi(arg, 'all')
                computeUTR3len = true;
            end
            if strcmpi(arg, 'UTR5len') || strcmpi(arg, 'utr5') || strcmpi(arg, 'utr') || strcmpi(arg, 'all')
                computeUTR5len = true;
            end
            if strcmpi(arg, 'GRAVY') || strcmpi(arg, 'all')
                computeGRAVY = true;
            end
            if strcmpi(arg, 'AROMA') || strcmpi(arg, 'all')
                computeAROMA = true;
            end
            if strcmpi(arg, 'Aliphatic') || strcmpi(arg, 'all')
                computeAliphatic = true;
            end
            if strcmpi(arg, 'II') || strcmpi(arg, 'all')
                computeII = true;
            end
            if strcmpi(arg, 'side-charge') || strcmpi(arg, 'all')
                computeSideCharge = true;
            end
            if strcmpi(arg, 'tAI14') || strcmpi(arg, 'all')
                computetAI14 = true;
            end
            if strcmpi(arg, 'tAI17') || strcmpi(arg, 'all')
                computetAI17 = true;
            end
            if strcmpi(arg, 'tAI19') || strcmpi(arg, 'all')
                computetAI19 = true;
            end
            if strcmpi(arg, 'tAI51') || strcmpi(arg, 'all')
                computetAI19 = true;
            end
            if strcmpi(arg, 'side-charge4') || strcmpi(arg, 'all')
                computeSideCharge4 = true;
            end
            if strcmpi(arg, 'side-charge11') || strcmpi(arg, 'all')
                computeSideCharge11 = true;
            end
            if strcmpi(arg, 'side-charge15') || strcmpi(arg, 'all')
                computeSideCharge15 = true;
            end
            if strcmpi(arg, 'side-charge40') || strcmpi(arg, 'all')
                computeSideCharge40 = true;
            end
            if strcmpi(arg, 'CAI2nd') || strcmpi(arg, 'all')
                computeCAI2nd = true;
            end
            if strcmpi(arg, 'tAI2nd') || strcmpi(arg, 'all')
                computetAI2nd = true;
            end
            if strcmpi(arg, 'CDS-fold') || strcmpi(arg, 'all')
                computeCDSfold = true;
            end
            if strcmpi(arg, 'CDS-fold17') || strcmpi(arg, 'all')
                computeCDSfold17 = true;
            end
            if strcmpi(arg, 'CDS-fold34') || strcmpi(arg, 'all')
                computeCDSfold34 = true;
            end
            if strcmpi(arg, 'CDS-fold53') || strcmpi(arg, 'all')
                computeCDSfold53 = true;
            end
            if strcmpi(arg, 'UTR3-fold') || strcmpi(arg, 'utr3') || strcmpi(arg, 'utr') || strcmpi(arg, 'all')
                computeUTR3fold = true;
            end
            if strcmpi(arg, 'UTR5-fold') || strcmpi(arg, 'utr5') || strcmpi(arg, 'utr') || strcmpi(arg, 'all')
                computeUTR5fold = true;
            end
            if strcmpi(arg, 'UTR5-fold50') || strcmpi(arg, 'utr5') || strcmpi(arg, 'utr') || strcmpi(arg, 'all')
                computeUTR5fold50 = true;
            end
            if strcmpi(arg, 'UTR5-fold50-CDS-fold17') || strcmpi(arg, 'utr5') || strcmpi(arg, 'utr') || strcmpi(arg, 'all')
                computeUTR5fold50CDSfold17 = true;
            end
            if strcmpi(arg, 'UTR5-fold50-CDS-fold34') || strcmpi(arg, 'utr5') || strcmpi(arg, 'utr') || strcmpi(arg, 'all')
                computeUTR5fold50CDSfold34 = true;
            end
            if strcmpi(arg, 'UTR5-fold50-CDS-fold53') || strcmpi(arg, 'utr5') || strcmpi(arg, 'utr') || strcmpi(arg, 'all')
                computeUTR5fold50CDSfold53 = true;
            end
            if strcmpi(arg, 'UTR5-fold-CDS-fold17') || strcmpi(arg, 'utr5') || strcmpi(arg, 'utr') || strcmpi(arg, 'all')
                computeUTR5foldCDSfold17 = true;
            end
            if strcmpi(arg, 'UTR5-fold-CDS-fold34') || strcmpi(arg, 'utr5') || strcmpi(arg, 'utr') || strcmpi(arg, 'all')
                computeUTR5foldCDSfold34 = true;
            end
            if strcmpi(arg, 'UTR5-fold-CDS-fold53') || strcmpi(arg, 'utr5') || strcmpi(arg, 'utr') || strcmpi(arg, 'all')
                computeUTR5foldCDSfold53 = true;
            end
            if strcmpi(arg, 'full-fold') || strcmpi(arg, 'utr') || strcmpi(arg, 'all')
                computeFullfold = true;
            end
            if strcmpi(arg, 'pI') || strcmpi(arg, 'all')
                computepI = true;
            end
            if strcmpi(arg, 'charge') || strcmpi(arg, 'all')
                computeCharge = true;
            end
            if strcmpi(arg, 'mol-weight') || strcmpi(arg, 'all')
                computeMolWeight = true;
            end
            if strcmpi(arg, 'Fop') || strcmpi(arg, 'all')
                computeFop = true;
            end
            if strcmpi(arg, 'CBI') || strcmpi(arg, 'all')
                computeCBI = true;
            end
            if strcmpi(arg, 'Dnuc') || strcmpi(arg, 'all')
                computeDnuc = true;
            end
        end
    end
    
    set = inset;
    n = length(inset);
    opt = get_S288c_options;
    
    if computeRCA
        fprintf('[i] Selecting RCA reference genes...\n');
        CAIitetative = CAIselecter(inset);
    end
    
    if computeGO
        fprintf('[i] Computing GO terms...\n');
        parfor i = 1:n
            set(i).go = extract_go(inset(i).desc);
        end
    end
    
    if computeNucleotideUsage
        fprintf('[i] Computing nucleotide frequencies...\n');
        parfor i = 1:n
            set(i).nucleotide_usage = compute_nucleotide_usage(inset(i));
        end
    end
    
    if computeCodonUsage
        fprintf('[i] Computing codon frequencies...\n');
        parfor i = 1:n
            set(i).codon_usage = compute_codon_usage(inset(i));
        end
    end
    
    if computeAminoUsage
        fprintf('[i] Computing amino acid frequencies...\n');
        parfor i = 1:n
            set(i).amino_usage = compute_amino_usage(inset(i).seq);
        end
    end
    
    if computeRSCU
        fprintf('[i] Computing RSCU...\n');
        parfor i = 1:n
            set(i).rscu = compute_rscu(inset(i).seq);
        end
    end
    
    if computePairRSCU
        fprintf('[i] Computing Pair RSCU...\n');
        parfor i = 1:n
            set(i).pair_rscu = compute_pair_rscu(inset(i));
        end
    end
    
    if computeGC
        fprintf('[i] Computing GC content...\n');
        parfor i = 1:n
            set(i).gc = compute_gc(inset(i).seq);
        end
    end
    
    if compute15GC
        fprintf('[i] Computing 15-GC content...\n');
        parfor i = 1:n
            set(i).gc15 = compute_15gc(inset(i).seq);
        end
    end
    
    if computeGC3
        fprintf('[i] Computing GC3 content...\n');
        parfor i = 1:n
            set(i).gc3 = compute_gc3(inset(i).seq);
        end
    end
    
    if computeLength
        fprintf('[i] Computing length...\n');
        parfor i = 1:n
            set(i).length = compute_length(inset(i));
        end
    end
    
    if computeNc
        fprintf('[i] Computing effective number of codons (Nc)...\n');
        parfor i = 1:n
            set(i).nc = compute_nc(inset(i));
        end
    end
    
    if computeEw
        fprintf('[i] Computing Ew...\n');
        parfor i = 1:n
            set(i).ew = compute_ew(inset(i));
        end
    end
    
    if computeSCUO
        fprintf('[i] Computing SCUO...\n');
        parfor i = 1:n
            set(i).scuo = compute_scuo(inset(i));
        end
    end
    
    if computeP1
        fprintf('[i] Computing P1...\n');
        parfor i = 1:n
            set(i).p1 = compute_p1(inset(i).seq, opt.tGCN);
        end
    end
    
    if computeP2
        fprintf('[i] Computing P2...\n');
        parfor i = 1:n
            set(i).p2 = compute_p2(inset(i));
        end
    end
    
    if computeRCBS
        fprintf('[i] Computing RCBS...\n');
        RCBSpseudo = RCBS_pseudo(inset);
        parfor i = 1:n
            set(i).rcbs = RCBSpseudo.calculate_rcbs(inset(i).seq);
        end
    end
    
    if computeCAI
        fprintf('[i] Computing CAI...\n');
        %CAIitetative = CAIselecter(inset);
        %CAIcalc = CAI(CAIitetative.Sequences(CAIitetative.SelectedIndices));
        CAIcalc = CAI([]);
        CAIcalc = CAIcalc.set_W(opt.W);
        parfor i = 1:n
            set(i).cai = CAIcalc.calculate_cai(inset(i).seq);
        end
    end
    
    if computetAI
        fprintf('[i] Computing tAI...\n');
        tAIcalc = tAI(opt.tGCN, opt.S);
        parfor i = 1:n
            set(i).tai = tAIcalc.calculate_tai(inset(i).seq);
        end
    end
    
    if computeRCA
        fprintf('[i] Computing RCA...\n');
        RCAcalc = RCA(CAIitetative.Sequences(CAIitetative.SelectedIndices));
        parfor i = 1:n
            set(i).rca = RCAcalc.calculate_rca(inset(i).seq);
        end
    end
    
    if computeP
        fprintf('[i] Computing P...\n');
        Pcalc = PIndex(inset);
        parfor i = 1:n
            %fprintf('Passed %d\n', i);
            set(i).p = Pcalc.calculate_p(inset(i).seq);
        end
    end
    
    if computeCPB
        fprintf('[i] Computing CPB...\n');
        CPBcalc = CPB(inset);
        parfor i = 1:n
            %fprintf('Passed %d\n', i);
            set(i).cpb = CPBcalc.calculate_cpb(inset(i).seq);
        end
    end
    
    if computeTPI
        fprintf('[i] Computing TPI...\n');
        TPIcalc = TPI(inset, opt.tTrans);
        parfor i = 1:n
            %fprintf('Passed %d\n', i);
            set(i).tpi = TPIcalc.calculate_tpi(inset(i).seq);
        end
    end
    
    if computeUTR3len
        fprintf('[i] Computing UTR3 length...\n');
        parfor i = 1:n
            if ~isempty(inset(i).utr3)
                set(i).utr3len = length(inset(i).utr3);
            else
                set(i).utr3len = [];
            end
        end
    end
    
    if computeUTR5len
        fprintf('[i] Computing UTR5 length...\n');
        parfor i = 1:n
            if ~isempty(inset(i).utr5)
                set(i).utr5len = length(inset(i).utr5);
            else
                set(i).utr5len = [];
            end
        end
    end
    
    if computeGRAVY
        fprintf('[i] Computing GRAVY...\n');
        parfor i = 1:n
            set(i).gravy = compute_gravy(inset(i));
        end
    end
    
    if computeAROMA
        fprintf('[i] Computing AROMA...\n');
        parfor i = 1:n
            set(i).aroma = compute_aroma(inset(i));
        end
    end
    
    if computeAliphatic
        fprintf('[i] Computing Aliphatic index...\n');
        parfor i = 1:n
            set(i).aliphatic = compute_aliphatic(inset(i));
        end
    end
    
    if computeII
        fprintf('[i] Computing Instability index...\n');
        parfor i = 1:n
            set(i).II = compute_II(inset(i));
        end
    end
    
    if computeSideCharge
        fprintf('[i] Computing average side chain charge...\n');
        parfor i = 1:n
            set(i).side_charge = compute_side_charge(inset(i));
        end
    end
    
    if computetAI14
        fprintf('[i] Computing tAI (14 codons)...\n');
        tAIcalc = tAI(opt.tGCN, opt.S);
        parfor i = 1:n
            len = length(inset(i).seq);
            wanted = 14 * 3;
            set(i).tai14 = tAIcalc.calculate_tai(inset(i).seq(1:min(len, wanted)));
        end
    end
    
    if computetAI17
        fprintf('[i] Computing tAI (17 codons)...\n');
        tAIcalc = tAI(opt.tGCN, opt.S);
        parfor i = 1:n
            len = length(inset(i).seq);
            wanted = 17 * 3;
            set(i).tai17 = tAIcalc.calculate_tai(inset(i).seq(1:min(len, wanted)));
        end
    end
    
    if computetAI19
        fprintf('[i] Computing tAI (19 codons)...\n');
        tAIcalc = tAI(opt.tGCN, opt.S);
        parfor i = 1:n
            len = length(inset(i).seq);
            wanted = 19 * 3;
            set(i).tai19 = tAIcalc.calculate_tai(inset(i).seq(1:min(len, wanted)));
        end
    end
    
    if computetAI51
        fprintf('[i] Computing tAI (51 codons)...\n');
        tAIcalc = tAI(opt.tGCN, opt.S);
        parfor i = 1:n
            len = length(inset(i).seq);
            wanted = 51 * 3;
            set(i).tai51 = tAIcalc.calculate_tai(inset(i).seq(1:min(len, wanted)));
        end
    end
    
    if computeSideCharge4
        fprintf('[i] Computing side chain charge (4 codons)...\n');
        parfor i = 1:n
            len = length(inset(i).seq);
            wanted = 4 * 3;
            set(i).side_charge4 = 4 * compute_side_charge(inset(i), min(len, wanted));
        end
    end
    
    if computeSideCharge11
        fprintf('[i] Computing side chain charge (11 codons)...\n');
        parfor i = 1:n
            len = length(inset(i).seq);
            wanted = 11 * 3;
            set(i).side_charge11 = 11 * compute_side_charge(inset(i), min(len, wanted));
        end
    end
    
    if computeSideCharge15
        fprintf('[i] Computing side chain charge (15 codons)...\n');
        parfor i = 1:n
            len = length(inset(i).seq);
            wanted = 15 * 3;
            set(i).side_charge15 = 15 * compute_side_charge(inset(i), min(len, wanted));
        end
    end
    
    if computeSideCharge40
        fprintf('[i] Computing side chain charge (40 codons)...\n');
        parfor i = 1:n
            len = length(inset(i).seq);
            wanted = 40 * 3;
            set(i).side_charge40 = 40 * compute_side_charge(inset(i), min(len, wanted));
        end
    end
    
    if computeCAI2nd
        fprintf('[i] Computing CAI of 2nd codons...\n');
        parfor i = 1:n
            set(i).cai2nd = opt.W(codon2int(inset(i).seq(4:6)));
        end
    end
    
    if computetAI2nd
        fprintf('[i] Computing tAI of 2nd codons...\n');
        tAIcalc = tAI(opt.tGCN, opt.S);
        parfor i = 1:n
            set(i).tai2nd = tAIcalc.W(codon2int(inset(i).seq(4:6)));
            %tAIcalc.calculate_tai([inset(i).seq(1:6) inset(i).seq(end - 2:end)]);
        end
    end
    
    if computeCDSfold
        fprintf('[i] Computing mRNA folding energy of CDS...\n');
        parfor i = 1:n
            set(i).cds_fold = RNAfold(inset(i).seq);
        end
    end
    
    if computeCDSfold17
        fprintf('[i] Computing mRNA folding energy of CDS (17 codons)...\n');
        parfor i = 1:n
            len = length(inset(i).seq);
            wanted = 17 * 3;
            set(i).cds17_fold = RNAfold(inset(i).seq(1:min(len, wanted)));
        end
    end
    
    if computeCDSfold34
        fprintf('[i] Computing mRNA folding energy of CDS (34 codons)...\n');
        parfor i = 1:n
            len = length(inset(i).seq);
            wanted = 34 * 3;
            set(i).cds34_fold = RNAfold(inset(i).seq(1:min(len, wanted)));
        end
    end
    
    if computeCDSfold53
        fprintf('[i] Computing mRNA folding energy of CDS (53 codons)...\n');
        parfor i = 1:n
            len = length(inset(i).seq);
            wanted = 53 * 3;
            set(i).cds53_fold = RNAfold(inset(i).seq(1:min(len, wanted)));
        end
    end
    
    if computeUTR3fold
        fprintf('[i] Computing mRNA folding energy of UTR3...\n');
        parfor i = 1:n
            if ~isempty(inset(i).utr3)
                set(i).utr3_fold = RNAfold(inset(i).utr3);
            else
                set(i).utr3_fold = [];
            end
        end
    end
    
    if computeUTR5fold
        fprintf('[i] Computing mRNA folding energy of UTR5...\n');
        parfor i = 1:n
            if ~isempty(inset(i).utr5)
                set(i).utr5_fold = RNAfold(inset(i).utr5);
            else
                set(i).utr5_fold = [];
            end
        end
    end
    
    if computeUTR5fold50
        fprintf('[i] Computing mRNA folding energy of UTR5 (50 last codons)...\n');
        parfor i = 1:n
            if ~isempty(inset(i).utr5)
                set(i).utr5_fold50 = compute_partial_utr5_fold(inset(i), 50);
            else
                set(i).utr5_fold50 = [];
            end
        end
    end
    
    if computeFullfold
        fprintf('[i] Computing mRNA folding energy for full molecules...\n');
        parfor i = 1:n
            if ~isempty(inset(i).utr3) && ~isempty(inset(i).utr5)
                set(i).full_fold = compute_full_fold(inset(i));
            else
                set(i).full_fold = [];
            end
        end
    end
    
    if computeUTR5fold50CDSfold17
        fprintf('[i] Computing mRNA folding energy of UTR5 (50 last codons) + CDS (17 codons)...\n');
        parfor i = 1:n
            if  ~isempty(inset(i).utr5)
                set(i).utr5_fold50_cds_fold17 = compute_partial_utr5_cds_fold(inset(i), 50, 17 * 3);
            else
                set(i).utr5_fold50_cds_fold17 = [];
            end
        end
    end
    
    if computeUTR5fold50CDSfold34
        fprintf('[i] Computing mRNA folding energy of UTR5 (50 last codons) + CDS (34 codons)...\n');
        parfor i = 1:n
            if  ~isempty(inset(i).utr5)
                set(i).utr5_fold50_cds_fold34 = compute_partial_utr5_cds_fold(inset(i), 50, 34 * 3);
            else
                set(i).utr5_fold50_cds_fold34 = [];
            end
        end
    end
    
    if computeUTR5fold50CDSfold53
        fprintf('[i] Computing mRNA folding energy of UTR5 (50 last codons) + CDS (53 codons)...\n');
        parfor i = 1:n
            if  ~isempty(inset(i).utr5)
                set(i).utr5_fold50_cds_fold53 = compute_partial_utr5_cds_fold(inset(i), 50, 53 * 3);
            else
                set(i).utr5_fold50_cds_fold53 = [];
            end
        end
    end
    
    if computeUTR5foldCDSfold17
        fprintf('[i] Computing mRNA folding energy of UTR5 + CDS (17 codons)...\n');
        parfor i = 1:n
            if  ~isempty(inset(i).utr5)
                set(i).utr5_fold_cds_fold17 = compute_partial_utr5_cds_fold(inset(i), -1, 17 * 3);
            else
                set(i).utr5_fold_cds_fold17 = [];
            end
        end
    end
    
    if computeUTR5foldCDSfold34
        fprintf('[i] Computing mRNA folding energy of UTR5 + CDS (34 codons)...\n');
        parfor i = 1:n
            if  ~isempty(inset(i).utr5)
                set(i).utr5_fold_cds_fold34 = compute_partial_utr5_cds_fold(inset(i), -1, 34 * 3);
            else
                set(i).utr5_fold_cds_fold34 = [];
            end
        end
    end
    
    if computeUTR5foldCDSfold53
        fprintf('[i] Computing mRNA folding energy of UTR5 + CDS (53 codons)...\n');
        parfor i = 1:n
            if  ~isempty(inset(i).utr5)
                set(i).utr5_fold_cds_fold53 = compute_partial_utr5_cds_fold(inset(i), -1, 53 * 3);
            else
                set(i).utr5_fold_cds_fold53 = [];
            end
        end
    end
    
    if computeUTR5foldCDSfold
        fprintf('[i] Computing mRNA folding energy of UTR5 + CDS...\n');
        parfor i = 1:n
            if  ~isempty(inset(i).utr5)
                set(i).utr5_fold_cds_fold = compute_partial_utr5_cds_fold(inset(i), -1, -1);
            else
                set(i).utr5_fold_cds_fold = [];
            end
        end
    end
    
    if computepI
        fprintf('[i] Computing pI...\n');
        parfor i = 1:n
            set(i).pI = isoelectric_point(inset(i).seq);
        end
    end
    
    if computeMolWeight
        fprintf('[i] Computing protein molecular weight...\n');
        parfor i = 1:n
            set(i).weight = protein_weight(inset(i).seq);
        end
    end
    
    if computeCharge
        fprintf('[i] Computing protein charge...\n');
        parfor i = 1:n
            set(i).charge = protein_charge(inset(i).seq);
        end
    end
    
    if computeFop
        fprintf('[i] Computing Fop...\n');
        Fop_calc = Fop(opt.tGCN);
        parfor i = 1:n
            set(i).fop = Fop_calc.calculate_fop(inset(i).seq);
        end
    end
    
    if computeCBI
        fprintf('[i] Computing CBI...\n');
        CBI_calc = CBI;
        parfor i = 1:n
            set(i).cbi = CBI_calc.calculate_cbi(inset(i).seq);
        end
    end
    
    if computeDnuc
        fprintf('[i] Computing Dnuc...\n');
        Dnuc_calc = Dnuc(inset);
        parfor i = 1:n
            set(i).dnuc = Dnuc_calc.calculate_dnuc(inset(i).seq);
        end
    end
    
    if computeDinucleotide
        fprintf('[i] Computing dinucleotide usage...\n');
        parfor i = 1:n
            set(i).dinucleotide_usage = compute_dinucleotides(inset(i).seq);
        end
    end
    
    %parfor i = 1:n
    %    set(i).codon_pair_usage = compute_codon_pair_usage(inset(i));
    %end
end

% Compute codon usage for a given item
%   ITEM  - input item
%   USAGE - computed codon usage
function [usage] = compute_codon_usage(item)
    arrayLen = length(get_codons);
    codons = codon2int(item.seq);
    
    usage = zeros(1, arrayLen);
    for i = 1:length(codons)
        usage(codons(i)) = usage(codons(i)) + 1;
    end
    
    usage = usage / sum(usage);
    
    %aminoAcids = amino2codons;
    %for i = 1:length(aminoAcids)
    %    codonsForAcid = aminoAcids{i};
    %    nCodons = length(codonsForAcid);
    %    ints = zeros(1, nCodons);
    %    for j = 1:nCodons
    %        ints(j) = codon2int(codonsForAcid{j});
    %    end
    %    usage(ints) = usage(ints) ./ max(sum(usage(ints)), 1);
    %    %usage(ints) = usage(ints) ./ sum(usage(ints));
    %end
end

% Compute paired relative synonymous codon usage (RSCU) for a given item
%   ITEM  - input item
%   USAGE - computed codon usage
function [usage] = compute_pair_rscu(item)
    arrayLen = length(get_codons);
    codons = codon2int(item.seq);
    
    usage = zeros(arrayLen);
    for i = 1:length(codons) - 1
        usage(codons(i), codons(i + 1)) = usage(codons(i), codons(i + 1)) + 1;
    end
    
    aminoAcids = amino2codons;
    for i = 1:length(aminoAcids)
        for j = 1:length(aminoAcids)
            a = aminoAcids{i};
            b = aminoAcids{j};
            s = 0;
            for k = 1:length(a)
                kind = codon2int(a{k});
                for l = 1:length(b)
                    lind = codon2int(b{l});
                    s = s + usage(kind, lind);
                end
            end
            s = s / (length(a) * length(b));
            s = max(s, 1);
            for k = 1:length(a)
                kind = codon2int(a{k});
                for l = 1:length(b)
                    lind = codon2int(b{l});
                    usage(kind, lind) = usage(kind, lind) / s;
                end
            end
        end
    end
end

% Compute codon pair usage for a given item
%   ITEM  - input item
%   USAGE - computed codon pair usage
function [usage] = compute_codon_pair_usage(item)
    arrayLen = length(get_codons);
    codons = codon2int(item.seq);
    
    usage = zeros(arrayLen);
    for i = 1:length(codons) - 1
        usage(codons(i), codons(i + 1)) = usage(codons(i), codons(i + 1)) + 1;
    end
    
    
    aminoAcids = amino2codons;
    for i = 1:length(aminoAcids)
        for j = 1:length(aminoAcids)
            a = aminoAcids{i};
            b = aminoAcids{j};
            s = 0;
            for k = 1:length(a)
                kind = codon2int(a{k});
                for l = 1:length(b)
                    lind = codon2int(b{l});
                    s = s + usage(kind, lind);
                end
            end
            s = max(s, 1);
            for k = 1:length(a)
                kind = codon2int(a{k});
                for l = 1:length(b)
                    lind = codon2int(b{l});
                    usage(kind, lind) = usage(kind, lind) / s;
                end
            end
        end
    end
end

% Compute nucleotide usage histogram for a given item
%   ITEM  - input item
%   USAGE - computed histogram
function [usage] = compute_nucleotide_usage(item)
    nuc = nuc2int(item.seq);
    arrayLen = length(get_nucs);
    usage = zeros(1, arrayLen);
    
    len = length(nuc);
    for i = 1:len
        usage(nuc(i)) = usage(nuc(i)) + 1;
    end
    usage = usage / sum(usage);
end

% Compute sequence length for a given item
%   ITEM - input item
%   len  - computed sequence length
function [len] = compute_length(item)
    len = length(item.seq);
end

% Compute effective number of codons Nc for a given item
%   ITEM - input item
%   Nc   - computed effective number of codons
function [Nc] = compute_nc(item)
    Nc = effective_number_codons(item.seq);
end

% Compute weighted sum of the relative entropy Ew for a given item
%   ITEM - input item
%   Ew   - computed weighted sum of relative entropy
function [Ew] = compute_ew(item)
    Ew = weighted_sum_of_relative_entropy(item.seq);
end

% Compute synonymous codon usage order
%   ITEM - input item
%   SCUO - computed synonymous codon usage order
function [scuo] = compute_scuo(item)
    scuo = synonymous_codon_usage_order(item.seq);
end

% Compute P2 index
%   ITEM - input item
%
%   P2   - computed P2 index
function [p2] = compute_p2(item)
    p2 = p2index(item.seq);
end

% Compute GRAVY index
%   ITEM    - input item
%
%   gravy   - computed GRAVY index
function [gravy] = compute_gravy(item)
    gravy = gravy_index(item.seq);
end

% Compute AROMA index
%   ITEM    - input item
%
%   aroma   - computed AROMA index
function [aroma] = compute_aroma(item)
    aroma = aroma_index(item.seq);
end

% Compute Aliphatic index
%   ITEM      - input item
%
%   aliphatic - computed Aliphatic index
function [aliphatic] = compute_aliphatic(item)
    aliphatic = aliphatic_index(item.seq);
end

% Compute Instability index
%   ITEM  - input item
%
%   index - computed Instability index
function [index] = compute_II(item)
    index = instability_index(item.seq);
end

% Compute average side chain charge
%   ITEM   - input item
%
%   charge - computed charge
function [charge] = compute_side_charge(item, len)
    if nargin < 2
        charge = side_charge(item.seq);
    else
        charge = side_charge(item.seq(1:len));
    end
end

% Compute MFE of a complete mRNA molecule
%   ITEM   - input item
%
%   fold   - computed fold
function [fold] = compute_full_fold(item)
    fold = RNAfold([item.utr5 item.seq item.utr3]);
end

% Compute MFE of <= n UTR5 bases
%   ITEM   - input item
%
%   fold   - computed fold
function [fold] = compute_partial_utr5_fold(item, part)
    if nargin < 2
        part = 50;
    end
    
    len = length(item.utr5);
    fold = RNAfold(item.utr5(max(len - part + 1, 1):len));
end

% Compute MFE of <= n UTR5 and m CDS bases
%   ITEM   - input item
%
%   fold   - computed fold
function [fold] = compute_partial_utr5_cds_fold(item, partUTR, partCDS)
    if nargin < 2
        partUTR = 50;
    end
    if nargin < 2
        partCDS = 53 * 3;
    end
    
    lenUTR = length(item.utr5);
    lenCDS = length(item.seq);
    
    if partUTR < 0
        partUTR = lenUTR;
    end
    
    if partCDS < 0
        partCDS = lenCDS;
    end
    
    seq = [item.utr5(max(1, lenUTR - partUTR + 1):lenUTR) item.seq(1:min(partCDS, lenCDS))];
    fold = RNAfold(seq);
end
