function [snr_mu snr_sigma] = evaluate_groupping_snr(ds, sizes, variance)
    if nargin < 2
        sizes = [50:10:100 200:100:1000 1500:500:6000];
    end
    
    if nargin < 3
        variance = 0.9;
    end
    
    snr1_mu = zeros(1, length(sizes));
    snr2_mu = zeros(1, length(sizes));
    snr3_mu = zeros(1, length(sizes));    
    snr4_mu = zeros(1, length(sizes));
    snr5_mu = zeros(1, length(sizes));
    snr6_mu = zeros(1, length(sizes));
    snr7_mu = zeros(1, length(sizes));
    snr8_mu = zeros(1, length(sizes));
    
    snr1_sigma = zeros(1, length(sizes));
    snr2_sigma = zeros(1, length(sizes));
    snr3_sigma = zeros(1, length(sizes));    
    snr4_sigma = zeros(1, length(sizes));
    snr5_sigma = zeros(1, length(sizes));
    snr6_sigma = zeros(1, length(sizes));
    snr7_sigma = zeros(1, length(sizes));
    snr8_sigma = zeros(1, length(sizes));
    
    parfor i = 1:length(sizes)
        fprintf('Processing size %d\n', sizes(i));
        
        bins = group_by_expression(ds, sizes(i));
        [snr1_mu(i) snr1_sigma(i)] = compute_bins_snr(bins);
        
        bins = group_by_log_expression(ds, sizes(i));
        [snr2_mu(i) snr2_sigma(i)] = compute_bins_snr(bins);
        
        bins = group_equal_width(ds, sizes(i));
        [snr3_mu(i) snr3_sigma(i)] = compute_bins_snr(bins);
        
        bins = group_equal_width_log(ds, sizes(i));
        [snr4_mu(i) snr4_sigma(i)] = compute_bins_snr(bins);
        
        bins = group_by_features(ds, sizes(i), 'kmeans');
        [snr5_mu(i) snr5_sigma(i)] = compute_bins_snr(bins);
        
        bins = group_by_features(ds, sizes(i), 'hierarchial');
        [snr6_mu(i) snr6_sigma(i)] = compute_bins_snr(bins);
        
        bins = group_by_features_pca(ds, sizes(i), variance, 'kmeans');
        [snr7_mu(i) snr7_sigma(i)] = compute_bins_snr(bins);
        
        bins = group_by_features_pca(ds, sizes(i), variance, 'hierarchial');
        [snr8_mu(i) snr8_sigma(i)] = compute_bins_snr(bins);
    end
    
    snr_mu = [snr1_mu; snr2_mu; snr3_mu; snr4_mu; snr5_mu; snr6_mu; snr7_mu; snr8_mu];
    snr_sigma = [snr1_sigma; snr2_sigma; snr3_sigma; snr4_sigma; snr5_sigma; snr6_sigma; snr7_sigma; snr8_sigma];
end