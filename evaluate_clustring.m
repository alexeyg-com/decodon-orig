function [mu sigma removed] = evaluate_clustring(ds, no_singleton, sizes)
    if nargin < 2
        no_singleton = false;
    end
    if nargin < 3
        sizes = [50:10:100 200:100:1000 1500:500:6000];
        if no_singleton
            sizes = [50:10:100 200:100:1000 1500];
        end
    end
    
    folds = 10;
    
    mu1 = zeros(1, length(sizes));
    sigma1 = zeros(1, length(sizes));
    removed1 = zeros(1, length(sizes));
    
    mu2 = zeros(1, length(sizes));
    sigma2 = zeros(1, length(sizes));
    removed2 = zeros(1, length(sizes));
    
    mu3 = zeros(1, length(sizes));
    sigma3 = zeros(1, length(sizes));
    removed3 = zeros(1, length(sizes));
    
    mu4 = zeros(1, length(sizes));
    sigma4 = zeros(1, length(sizes));
    removed4 = zeros(1, length(sizes));
    
    mu5 = zeros(1, length(sizes));
    sigma5 = zeros(1, length(sizes));
    removed5 = zeros(1, length(sizes));
    
    mu6 = zeros(1, length(sizes));
    sigma6 = zeros(1, length(sizes));
    removed6 = zeros(1, length(sizes));
    
    parfor i = 1:length(sizes)
    %for i = 1:length(sizes)
        fprintf('Processing size %d\n', sizes(i));
        
        fprintf(' - Kmeans centroid\n');
        bins = group_by_features(ds, sizes(i), 'kmeans');
        if no_singleton
            [bins removed1(i)] = remove_singleton_bins(bins);
        end
        ds_bins = summarize_bins(bins);
        [mu1(i) sigma1(i)] = regression_cv(ds_bins, 'r2', min(folds, size(ds_bins, 1)), 'normal', 'identity', 'linear', [], true);
        
        fprintf(' - Hierarchial centroid\n');
        bins = group_by_features(ds, sizes(i), 'hierarchial', 'centroid');
        if no_singleton
            [bins removed2(i)] = remove_singleton_bins(bins);
        end
        ds_bins = summarize_bins(bins);
        [mu2(i) sigma2(i)] = regression_cv(ds_bins, 'r2', min(folds, size(ds_bins, 1)), 'normal', 'identity', 'linear', [], true);
        
        fprintf(' - Kmeans PCA\n');
        bins = group_by_features_pca(ds, sizes(i), 0.9, 'kmeans');
        if no_singleton
            [bins removed3(i)] = remove_singleton_bins(bins);
        end
        ds_bins = summarize_bins(bins);
        [mu3(i) sigma3(i)] = regression_cv(ds_bins, 'r2', min(folds, size(ds_bins, 1)), 'normal', 'identity', 'linear', [], true);
        
        fprintf(' - Hierarchial centroid PCA\n');
        bins = group_by_features_pca(ds, sizes(i), 0.9, 'hierarchial', 'centroid');
        if no_singleton
            [bins removed4(i)] = remove_singleton_bins(bins);
        end
        ds_bins = summarize_bins(bins);
        [mu4(i) sigma4(i)] = regression_cv(ds_bins, 'r2', min(folds, size(ds_bins, 1)), 'normal', 'identity', 'linear', [], true);
        
        fprintf(' - Hierarchial complete\n');
        bins = group_by_features(ds, sizes(i), 'hierarchial', 'complete');
        if no_singleton
            [bins removed5(i)] = remove_singleton_bins(bins);
        end
        ds_bins = summarize_bins(bins);
        [mu5(i) sigma5(i)] = regression_cv(ds_bins, 'r2', min(folds, size(ds_bins, 1)), 'normal', 'identity', 'linear', [], true);
        
        fprintf(' - Hierarchial complete PCA\n');
        bins = group_by_features_pca(ds, sizes(i), 0.9, 'hierarchial', 'complete');
        if no_singleton
            [bins removed6(i)] = remove_singleton_bins(bins);
        end
        ds_bins = summarize_bins(bins);
        [mu6(i) sigma6(i)] = regression_cv(ds_bins, 'r2', min(folds, size(ds_bins, 1)), 'normal', 'identity', 'linear', [], true);
    end
    
    mu = [mu1; mu2; mu3; mu4; mu5; mu6];
    sigma = [sigma1; sigma2; sigma3; sigma4; sigma5; sigma6];
    removed = [removed1; removed2; removed3; removed4; removed5; removed6];
end