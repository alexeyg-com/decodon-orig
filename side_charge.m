function [charge] = side_charge(seq, part)
    arrayLen = length(get_aminos);
    aminos = codon2amino_int(codon2int(seq));
    
    if nargin >= 2
        aminos = aminos(min(part, length(aminos)));
    end
    
    usage = zeros(1, arrayLen);
    for i = 1:length(aminos)
        usage(aminos(i)) = usage(aminos(i)) + 1;
    end
    usage = usage / (sum(usage) - usage(22));
    
    charge = usage(2) + usage(12) + 0.1 * usage(9) - usage(4) - usage(7);
end