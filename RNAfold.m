function [energy] = RNAfold(seq, native, normalize)
    if nargin < 2
        native = false;
    end
    if nargin < 3
        normalize = false;
    end
    
    if native
        [~, energy] = rnafold(seq);
        return;
    end

    path = '~/apps/bin/';

    temp_in = tempname;
    
    fin = fopen(temp_in, 'w');
    if iscell(seq)
        n = length(seq);
    else
        seq = { seq };
        n = 1;
    end
    len = zeros(1, n);
    for i = 1:n
        outSeq = seq{i};
        len(i) = length(outSeq);
        for j = 1:length(outSeq)
            if outSeq(j) == 'T'
                outSeq(j) = 'U';
            elseif outSeq(j) == 't'
                outSeq(j) = 'u';
            end
        end
        fprintf(fin, '%s\n', outSeq);
    end
    fprintf(fin, '@\n');
    fclose(fin);    
    
    [status, result] = unix(sprintf(['%sRNAfold < %s'], path, temp_in));
    delete(temp_in);
    
    if status > 0
        warning('RNAfold:notNULL','RNAfold execution did not return NULL.');
    end
    
    M = regexpi(result, '((\-)?[0-9]+\.[0-9]+)', 'match');
    energy = zeros(1, n);
    for i = 1:n
        energy(i) = sscanf(M{i}, '%f');
    end
    if normalize
        energy = energy ./ len;
    end
end
