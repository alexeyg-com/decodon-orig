function [set] = attach_lu(inset, fileName)
    if nargin < 2
        fileName = 'import/Lu.xls';
    end
    [levels, names] = xlsread(fileName);
    
    names = names(17:end, 1);
    mRNA = levels(:, 25);
    protein = levels(:, 21);
    
    set = inset;
    map_names = strtok({inset.desc});
    total = size(levels, 1);
    missing = 0;
    for i = 1:total;
        name = names{i};
        ind = find(strcmpi(name, map_names), 1);
        if ~isempty(ind)
            if ~isnan(mRNA(i))
                set(ind).mRNA_Lu = mRNA(i);
            end
            if ~isnan(protein(i))
                set(ind).protein_Lu = protein(i);
            end
        else
            fprintf('Unable to find CDS %s\n', name);
            missing = missing + 1;
        end
    end
    
    fprintf('Total: %d, Missing: %d\n', total, missing);
end
