% Visualize a set microarray expression values.
%   SEQ - set of sequences to visualize
%   HL  - names of sequences to be highligted
function [] = visualize_set(seq, hl)
    n = length(seq);
    mu1 = zeros(1, n);
    mu2 = zeros(1, n);
    sigma1 = zeros(1, n);
    sigma2 = zeros(1, n);
    for i = 1:n
        mu1(i) = seq(i).e_mean(1);
        mu2(i) = seq(i).e_mean(2);
        sigma1(i) = seq(i).e_std(1);
        sigma2(i) = seq(i).e_std(2);
    end
    
    mu1 = log2(mu1);
    mu2 = log2(mu2);
    
    [dummy, ind] = sort(mu1);
    n = length(mu1);
    revInd = zeros(1, n);
    x = 1:n;
    for i = 1:n
        revInd(ind(i)) = i;
    end
    
    m = length(hl);
    gene_names = strtok({seq.desc});
    mv = max([mu1 mu2]);
    hl_ind = [];
    for i = 1:m
        searchInd = find(strcmpi(hl{i}, gene_names));
        if length(searchInd) ~= 0
            hl_ind = [hl_ind searchInd'];
        else
            fprintf('Not found: %s\n', hl{i});
            %fprintf('%s\n', hl{i});
        end
    end
    
    yvalues = repmat([0 mv], [length(hl_ind) 1]);
    xvalues = [revInd(hl_ind); revInd(hl_ind)]';
    
    plot(x, mu2(ind), 'g', x, mu1(ind), 'b', xvalues', yvalues', 'r--');
    axis([1 n 0 mv]);
    xlabel('Gene #');
    ylabel('Log-scale average expression');
end