function [cur, cur_r2, cur_width, ds] = nw_select_bandwith(ds)
    width = [0.0001 0.0003 0.0009 0.001 0.0027 0.003 0.0081 0.009 0.027 0.081 0.243 0.729];
    n = size(ds, 2) - 1;
    
    for i = 1:n + 1
        vals = double(ds(:, i));
        ma = max(vals);
        mi = min(vals);
        if ma ~= mi
            vals = (vals - mi) / (ma - mi);
        else
            vals(:) = 0;
        end
        ds(:, i) = dataset(vals, 'VarNames', ds.Properties.VarNames{i});
    end
    
    cur = [];
    cur_r2 = [];
    cur_width = [];
    for j = 1:n
        r2 = -ones(1, length(width));
        for i = 1:length(width)
            warning off stats:cvpartition:KFoldMissingGrp
            warning off stats:cvpartition:TestZero
            r2(i) = get_error(ds(:, [j n + 1]), width(i));
            fprintf('  Feature %s, width %f : %f\n', ds.Properties.VarNames{j}, width(i), r2(i));
        end
        [r2m, curB] = max(r2);
        if r2m == -1
            break
        end
        cur = [cur j];
        cur_r2 = [cur_r2 r2m];
        cur_width = [cur_width width(curB)];
        fprintf('%d : Selected %s, width %f, R2 = %f\n', j, ds.Properties.VarNames{j}, width(curB), r2m);
    end
end

function [mu sigma error] = get_error(ds, w)
    [mu sigma error] = nw_regression_cv(ds, 'r2', 10, w);
end