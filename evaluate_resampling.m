function [mu sigma] = evalutuate_resampling(ds, sizes, samples)
    if nargin < 2
        sizes = [10:10:100 200:100:1000];
    end
    
    if nargin < 3
        samples = 2000;
    end
    
    folds = 10;
    
    mu1 = zeros(1, length(sizes));
    sigma1 = zeros(1, length(sizes));
    
    mu2 = zeros(1, length(sizes));
    sigma2 = zeros(1, length(sizes));
    
    mu3 = zeros(1, length(sizes));
    sigma3 = zeros(1, length(sizes));
    
    mu4 = zeros(1, length(sizes));
    sigma4 = zeros(1, length(sizes));
    
    mu5 = zeros(1, length(sizes));
    sigma5 = zeros(1, length(sizes));
    
    parfor i = 1:length(sizes)
        fprintf('Processing size %d\n', sizes(i));
        
        bins = group_by_expression(ds, sizes(i));
        ds_bins = resample_bins(bins, samples);
        [mu1(i) sigma1(i)] = regression_cv(ds_bins, 'r2', folds, 'normal', 'identity', 'linear', [], true);
        
        bins = group_by_log_expression(ds, sizes(i));
        ds_bins = resample_bins(bins, samples);
        [mu2(i) sigma2(i)] = regression_cv(ds_bins, 'r2', folds, 'normal', 'identity', 'linear', [], true);
        
        bins = group_equal_width(ds, sizes(i));
        ds_bins = resample_bins(bins, samples);
        [mu3(i) sigma3(i)] = regression_cv(ds_bins, 'r2', folds, 'normal', 'identity', 'linear', [], true);
        
        bins = group_equal_width_log(ds, sizes(i));
        ds_bins = resample_bins(bins, samples);
        [mu4(i) sigma4(i)] = regression_cv(ds_bins, 'r2', folds, 'normal', 'identity', 'linear', [], true);
        
        bins = group_by_features(ds, sizes(i));
        ds_bins = resample_bins(bins, samples);
        [mu5(i) sigma5(i)] = regression_cv(ds_bins, 'r2', folds, 'normal', 'identity', 'linear', [], true);
    end
    
    mu = [mu1; mu2; mu3; mu4; mu5];
    sigma = [sigma1; sigma2; sigma3; sigma4; sigma5];

end