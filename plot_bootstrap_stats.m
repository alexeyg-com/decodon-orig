function [vars_intersect, counts_matrix] = plot_bootstrap_stats(names, vars, counts, reps)
    if nargin < 4
        reps = 1000;
    end

    %n_models = length(models);
    n_models = length(vars);
    %vars = cell(1, n_models);
    %counts = cell(1, n_models);
    
    vars_intersect = {};
    for i = 1:n_models
        %[vars{i} counts{i}] = bootstrap_models2stats(models{i});
        vars_intersect = union(vars_intersect, vars{i});
    end
    
    n_vars = length(vars_intersect);
    counts_matrix = zeros(n_models, n_vars);
    for i = 1:n_models
        new_counts = zeros(1, n_vars);
        for j = 1:n_vars
            ind = find(strcmp(vars_intersect{j}, vars{i}));
            if isempty(ind)
                new_counts(j) = 0;
            else
                new_counts(j) = counts{i}(ind);
            end
        end
        counts{i} = new_counts;
        counts_matrix(i, :) = new_counts;
    end
    
    counts_matrix = counts_matrix ./ reps;
    
    bar(counts_matrix');
    %set(gca, 'XTickLabel', vars_intersect);
    ylabel('Fraction of bootstraps');
    xticklabel_rotate(1:n_vars, 90, vars_intersect);
end