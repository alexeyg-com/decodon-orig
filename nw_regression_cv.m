function [mu, sigma, errors] = nw_regression_cv(ds, dist, K, weights)
    if nargin < 2
        dist = 'mse';
    end
    if nargin < 3
        K = 10;
    end
    if nargin < 4
        weights = [];
    end
    
    y = double(ds(:, end));
    partition = cvpartition(y, 'Kfold', K);
    errors = zeros(1, K);
    for i = 1:K
        dsTrain = ds(partition.training(i), :);
        dsTest = ds(partition.test(i), :);
        model = NWModel.fit(dsTrain, [], weights);
        errors(i) = evaluate_model(model, dsTest, dist);
    end
    mu = mean(errors);
    sigma = std(errors);
end