classdef GOoperations
    properties
        go
        go_ids
        go_syn
        graph
        graph_dist
        graph_size
    end
    
    methods
        function [obj] = GOoperations(go)
            obj.go = go;
            
            fprintf('[i] Constructing adjacency matrix...\n');
            obj = obj.construct_matrix();
            
            fprintf('[i] Constructing synonyms matrix...\n');
            obj = obj.find_synonyms();
            
            fprintf('[i] Computing all pairs shortest paths distance matrix...\n');
            obj = obj.compute_distances();
        end
        
        % Compute vector-max distance between sets of go terms
        %   A      - GO terms set one
        %   B      - GO terms set two
        %   LAMBDA - distance damping paramter, 0 <= LAMBDA <= 1.
        %
        %   DIST   - computed distance
        function [dist] = compute_distance(obj, a, b, lambda)
            if nargin < 4
                lambda = 0.9;
            end
            
            a = obj.str2id(a);
            b = obj.str2id(b);
            
            a = obj.obtain_vector(a, lambda);
            b = obj.obtain_vector(b, lambda);
            
            dist = obj.cosine(a, b);
        end
        
        % Convert GO term ids into indices used in the GO object
        %   GO      - the GO annotations object
        %   IDS     - input GO term ids
        %
        %   INDICES - output GO object indices
        function [indices] = num2id(obj, ids)
            m = length(ids);
            indices = [];
            for i = 1:m
                found = find(ids(i) == obj.go_ids);
                if isempty(found)
                    found = find(ids(i) == obj.go_syn(2, :));
                    if isempty(found)
                        fprintf('Unable to find GO ID for %d\n', ids(i));
                        continue;
                    else
                        found = obj.go_syn(1, found);
                    end
                end
                indices = [indices found];
            end
        end
        
        % Construct max-vector for given go annotations
        function [vec] = obtain_vector(obj, a, lambda)
            vec = Inf(1, obj.graph_size);
            for i = 1:length(a)
                [val, ind] = find(obj.graph_dist(a(i), :));
                vec(ind) = min(vec(ind), val);
                vec(a(i)) = 0;
            end
            %vec(~isinf(vec))
            %mat = obj.graph_dist(a, :);
            %mat(mat == 0) = Inf;
            %mat(1:length(a), a) = 0;
            %vec = min(mat);
            vec = lambda .^ vec;
        end
        
        % CConvert GO:0000XYZ strings into indices used in the GO object
        %   OBJ     - the GO annotations object
        %   IDS     - input GO term ids
        %
        %   INDICES - output GO object indices
        function [INDICES] = str2id(obj, ids)
            INDICES = obj.num2id(obj.str2num(ids));
        end
    end
    
    methods (Access = private)
        % Find term synonyms
        function [obj] = find_synonyms(obj)
            obj.go_syn = [];
            syn = get(obj.go.Terms, 'synonym');
            n = length(syn);
            for i = 1:n
                m = size(syn{i}, 1);
                for j = 1:m
                    if strcmpi(syn{i}{j, 1}, 'alt_id')
                        id = obj.str2num(syn{i}{j, 2});
                        obj.go_syn = [obj.go_syn, [i; id]];
                    end
                end
            end
        end
        
        % Compute all pair shortest path distance matrix
        function [obj] = compute_distances(obj)
            current_g = sparse(eye(obj.graph_size));
            double_g = sparse(double(obj.graph));
            
            obj.graph_dist = Inf(obj.graph_size);
            for i = 1:obj.graph_size
                obj.graph_dist(i, i) = 0;
            end
            for d = 1:obj.graph_size
                tic;
                fprintf('   [i] Step %d...\n', d);
                current_g = sparse((current_g * double_g) > 0);
                obj.graph_dist(current_g) = min(obj.graph_dist(current_g), d);
                toc;
                if all(current_g(:) == 0)
                    break;
                end
            end
            obj.graph_dist(isinf(obj.graph_dist)) = 0;
            obj.graph_dist = sparse(obj.graph_dist);
        end
        
        % Construct adjacency matrix for GO terms
        function [obj] = construct_matrix(obj)
           [mat, obj.go_ids, rel] = getmatrix(obj.go);
           is_a = find(strcmpi('is_a', rel));
           part_of = find(strcmpi('part_of', rel));
           obj.graph = ((mat == is_a) + (mat == part_of)) > 0;
           obj.graph_size = size(obj.graph, 1);
        end
    end
    
    methods (Static = true)
        % Convert GO:0000XYZ strings to integers XYZ
        %   NAMES - cell array of string term ids
        %
        %   IDS   - output integer vector
        function [ids] = str2num(names)
            if isinteger(names)
                ids = names;
            else
                if ~iscell(names)
                    names = {names};
                end
                
                n = length(names);
                ids = zeros(1, n);
                for i = 1:n
                    len = length(names{i});
                    ids(i) = str2double(names{i}(4:len));
                end
            end
        end
        
        % Compute cosine measure for two vectors
        function [cos] = cosine(a, b)
            cos = a * b' / (norm(a, 2) * norm(b, 2));
        end
    end
end