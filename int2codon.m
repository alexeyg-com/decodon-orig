function [codon] = int2codon(ints)
    codons = get_codons;
    codonsCount = length(codons);
    len = length(ints);
    codon = '';
    for i = 1:len
        codonInt = ints(i);
        if codonInt <= 0 || codonInt > codonsCount
            continue;
        end
        codon = [codon codons{codonInt}];
    end
end