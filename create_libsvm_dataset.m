function [] = create_libsvm_dataset(fileName, ds)
    %ds = make_dataset(feat, ds);
    
    X = double(ds(:, 1:end - 1));
    y = double(ds(:, end));
    X = zscore(X);
    y = zscore(y);
    
    n = size(X, 1);
    m = size(X, 2);
    
    fout = fopen(fileName, 'w');
    for i = 1:n
        fprintf(fout, '%f', y(i));
        for j = 1:m
            fprintf(fout, ' %d:%f', j, X(i, j));
        end;
        fprintf(fout, '\n');
    end;
    fclose(fout);
end