classdef NWModel
    properties
        kern_x
        kern_y
        N
        Width
    end
    
    methods
        function [obj] = NWModel(x, y, width, n)
            obj.kern_x = x;
            obj.kern_y = y;
            obj.N = n;
            obj.Width = width;
        end
        
        function [y] = predict(obj, x)
            if isa(x, 'dataset')
                x = double(x(:, 1:size(x, 2) - 1));
            end
            t = ksrmv(obj.kern_x, obj.kern_y, obj.Width, x);
            y = t.f;
        end
    end
    
    methods (Static)
        function [obj] = fit(x, y, width)
            % We have a dataset
            if nargin < 2 || isempty(y)
                n = size(x, 2);
                y = double(x(:, n));
                x = double(x(:, 1:n - 1));
            end
            if nargin < 3
                width = [];
            end
            if ~isempty(width)
                t = ksrmv(x, y, width);
            else
                t = ksrmv(x, y);
            end
            obj = NWModel(x, y, t.h, t.n);
        end
    end
end