function [bins] = group_by_features_pca(ds, num_bins, variance, mode, type_linkage)
    if nargin < 3
        variance = 0.9;
    end
    if nargin < 4
        mode = 'kmeans';
    end
    if nargin < 5
        type_linkage = 'centroid';
    end
    
    n = size(ds, 1);
    m = size(ds, 2);
    % Should we normalize?
    ds_pca = double(ds(:, [1 : m - 1]));
    for i = 1:size(ds_pca, 2)
        t = double(ds_pca(:, i));
        ma = max(t);
        mi = min(t);
        scal = ma - mi;
        if scal == 0
            scal = 1;
        end
        ds_pca(:, i) = (t - mi) / scal;
    end
    [pca, score, latent] = princomp(ds_pca);
    explained = cumsum(latent) ./ sum(latent);
    
    for n_comp = 1:m-1
        if explained(n_comp) >= variance
            break;
        end
    end
    fprintf('Retaining %d dimensions.\n', n_comp);
    
    ds_clust = pca(1:n_comp, :) * ds_pca';
    ds_clust = ds_clust';
    
    if strcmpi(mode, 'kmeans')
        ids = kmeans(ds_clust, num_bins, 'EmptyAction', 'drop');
    elseif strcmpi(mode, 'hierarchial')
        ds_dist = pdist(ds_clust);
        link = linkage(ds_dist, type_linkage);
        ids = cluster(link, 'maxclust', num_bins);
    else
        error('Unknown groupping mode.');
    end
    
    
    bins = cell(1, num_bins);
    
    for i = 1:num_bins
        id = find(ids == i);
        bins{i} = ds(id, :);
    end
end