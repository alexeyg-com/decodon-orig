function [mu sigma x removed ds] = evaluate_clustring_datasets(feat, no_singleton, steps, goals)
    if nargin < 2
        no_singleton = true;
    end
    if nargin < 3
        steps = 100;
    end
    if nargin < 4
        goals = {'mRNA-lim-mean', 'mRNA-lim-max', 'mRNA-lim-min', 'mRNA-Arava', 'mRNA-count', 'mRNA-seq', 'Density-Arava', 'Relative-rate-Arava', 'GFP', 'TAP', 'mRNA-Arava*Density-Arava', 'mRNA-Arava*Relative-rate-Arava', 'mRNA-count*Density-Arava', 'mRNA-count*Relative-rate-Arava', 'mRNA-seq*Density-Arava', 'mRNA-seq*Relative-rate-Arava'};
    end
    
    [goals ds] = prepare_datasets(feat, goals);
    folds = 20;
    
    x = cell(1, length(goals));
    mu = cell(1, length(goals));
    sigma = cell(1, length(goals));
    removed = cell(1, length(goals));
    
    for i = 1:length(goals)
        num = size(ds{i}, 1);
        fprintf('[i] Processing dataset %s (%d genes)\n', goals{i}, num);
        
        mu_kmeans = zeros(1, steps);
        sigma_kmeans = zeros(1, steps);
        removed_kmeans = zeros(1, steps);
        
        mu_hierarchial = zeros(1, steps);
        sigma_hierarchial = zeros(1, steps);
        removed_hierarchial = zeros(1, steps);
        
        step_size = (num - 500 - 100) / steps;
        sizes = round(100 + [100:step_size:num - 500]);
        sizes(1) = 100;
        sizes(steps) = num - 500;
        
        parfor j = 1:steps
            cur_size = sizes(j);
            fprintf('    [i] Processing size %d\n', cur_size);
            
            fprintf('        [i] K-means\n');
            bins = group_by_features(ds{i}, cur_size, 'kmeans');
            if no_singleton
                [bins removed_kmeans(j)] = remove_singleton_bins(bins);
            end
            ds_bins = summarize_bins(bins);
            [mu_kmeans(j) sigma_kmeans(j)] = regression_cv(ds_bins, 'r2', min(folds, size(ds_bins, 1)), 'normal', 'identity', 'linear', [], true);
            
            
            fprintf('        [i] Hierarchial complete\n');
            bins = group_by_features(ds{i}, cur_size, 'hierarchial', 'complete');
            if no_singleton
                [bins removed_hierarchial(j)] = remove_singleton_bins(bins);
            end
            ds_bins = summarize_bins(bins);
            [mu_hierarchial(j) sigma_hierarchial(j)] = regression_cv(ds_bins, 'r2', min(folds, size(ds_bins, 1)), 'normal', 'identity', 'linear', [], true);
        end
        
        x{i} = sizes;
        mu{i} = [mu_kmeans; mu_hierarchial];
        sigma{i} = [sigma_kmeans; sigma_hierarchial];
        removed{i} = [removed_kmeans; removed_hierarchial];
    end
end