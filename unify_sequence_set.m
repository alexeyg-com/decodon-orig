function [set] = unify_sequence_set(set)
    n = length(set);
    parfor i = 1:n
        set(i).mRNA_lim_mean = set(i).mRNA_lim_mean(1);
        set(i).mRNA_lim_max = set(i).mRNA_lim_max(1);
        set(i).mRNA_lim_min = set(i).mRNA_lim_min(1);
        set(i).mRNA_lim_std = set(i).mRNA_lim_std(1);
        if ~isempty(set(i).utr3)
            len = length(set(i).utr3);
            set(i).utr3 = set(i).utr3{len};
        end
        if ~isempty(set(i).utr5)
            len = length(set(i).utr5);
            set(i).utr5 = set(i).utr5{len};
        end
    end
end