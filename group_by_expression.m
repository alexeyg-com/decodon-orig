% Group entries in a dataset by expression levels
%   DS       - dataset
%   NUM_BINS - number of bins to group expression into
%
%   BINS     - created bins
function [bins] = group_by_expression(ds, num_bins)
    y = double(ds(:, end));
    [y, ind] = sort(y);
    
    n = size(ds, 1);
    per_bin = n / num_bins;
    
    bins = cell(1, num_bins);
    for i = 1:num_bins
        start = floor((i - 1) * per_bin + 1);
        stop = floor(i * per_bin);
        if i == num_bins
            stop = n;
        end
        bins{i} = ds(ind(start:stop), :);
    end
end