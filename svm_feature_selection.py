#/usr/bin/env python

from svmutil import svm_read_problem
import svmutil
import svm_tools
from IPython.parallel import Client

def evaluate_feature_set(features) :
	subX = svm_tools.reduce_X(X, features)
	return svmutil.svm_train(y, subX, param_string)

class SvmFeatureSelector :
	def __init__(self, data_file, param_string) :
		self.data_file = data_file
		self.param_string = param_string
		self.y, self.X = svm_read_problem(self.data_file)
		self.n_features = len(self.X[1])

		self.client = Client()
		self.loader = self.client.load_balanced_view()
		self.direct = self.client[:]

		with self.direct.sync_imports() :
			import svmutil
			import svm_tools

		self.feature_dict2list()

		self.direct.push(dict(X=self.X, y=self.y, param_string=param_string))

	def feature_dict2list(self) :
		n_items = len(self.X)
		for i in range(n_items) :
			item = self.X[i]
			item_list = []
			for key, feat in item.iteritems() :
				item_list.append(feat)
			self.X[i] = item_list
	
	def select_features(self) :
		n_features = self.n_features

		r2 = [0] * n_features
		sigma = [0] * n_features
		features = [[]] * n_features

		features[0] = range(n_features)
		res = self.direct.map(evaluate_feature_set, [range(n_features)])
		r2[0], sigma[0] = zip(*res)
		r2[0], sigma[0] = r2[0][0], sigma[0][0]
		#r2[0], sigma[0] = evaluate_feature_set(range(n_features))
			    
		for i in range(n_features - 1, 0, -1) :
			print '[i] Starting feature selection for %d features' % i
			current_features = features[n_features - 1 - i]
			new_features = [[]] * (i + 1) 
			for j in range(i + 1) :
				new_features[j] = current_features[:]
				del new_features[j][j]
				#new_features[j] = (new_features[j], self.y, self.X, self.param_string)
			
			zipped_results = self.direct.map_sync(evaluate_feature_set, new_features)
			current_r2, current_sigma = zip(*zipped_results)
			current_r2 = list(current_r2)
			current_sigma = list(current_sigma)
			
			max_r2 = max(current_r2)
			ind = current_r2.index(max_r2)

			r2[n_features - i] = max_r2
			sigma[n_features - i] = current_sigma[ind]
			features[n_features - i] = new_features[ind]
			print '[+] Best result: %g +/- %g' % (r2[n_features - i], sigma[n_features - i])
		
		return (r2, sigma, features)

