classdef CAI
    properties
        W
        MissingWeight
    end
    
    properties (Hidden)
        codon2amino
        codon_len
        trans
    end
    
    methods
        function [obj] = CAI(reference_set, missingWeight)
            if nargin < 2
                missingWeight = 0.01;
            end
            obj.codon_len = length(get_codons);
            obj.MissingWeight = missingWeight;
            n = length(reference_set);
            obj.W = zeros(1, obj.codon_len);
            obj.codon2amino = zeros(1, obj.codon_len);
            obj.trans = amino2codons_int;
            x = zeros(1, obj.codon_len);
            for i = 1:obj.codon_len
                obj.codon2amino(i) = codon2amino_int(i);
            end
            for i = 1:n
                seq = reference_set(i).seq;
                if isfield(reference_set(i), 'seq_int')
                    codon = reference_set(i).seq_int;
                else
                    codon = codon2int(seq);
                end
                for j = 1:length(codon)
                    obj.W(codon(j)) = obj.W(codon(j)) + 1;
                end
                x(codon) = x(codon) + 1;
            end
            x = x / n;
            trans = amino2codons_int;
            for i = 1:length(trans)
                y = max(obj.W(trans{i}));
                if y > 0
                    obj.W(trans{i}) = obj.W(trans{i}) / y;
                end
            end
            obj.W = obj.W .* x;
            obj.W(find(obj.W == 0)) = obj.MissingWeight;
        end
        
        function [obj] = set_W(obj, W)
            obj.W = W;
            obj.W(find(W == 0)) = obj.MissingWeight;
        end
        
        function [cai] = calculate_cai(obj, seq)
            if all(isnumeric(seq))
                codon = seq;
            else
                codon = codon2int(seq);
            end
            n = 0;
            cai = 0;
            for i = 2:length(codon) - 1
                amino = obj.codon2amino(codon(i));
                if length(obj.trans{amino}) <= 1
                    continue;
                end
                n = n + 1;
                cai = cai + log(obj.W(codon(i)));
            end
            cai = exp(cai / n);
        end
    end
end