function [energy] = RNAfold_cds(seq, part)
    if nargin >= 2
        part = min(part, round(length(seq) / 3));
        seq = seq(1:min(length(seq), part* 3));
    end
    energy = RNAfold(seq);
end