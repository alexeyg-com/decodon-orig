% Converts integer representation of nucleotides to a string.
%   INTS   - input integer array
%
%   NUCINT - output nucleotide representation
function [nucInt] = nuc2int(nuc)
    nucs = get_nucs;
    len = length(nuc);
    nucInt = zeros(1, len);
    for i = 1:len
        found = find(strcmpi(nuc(i), nucs));
        if ~isempty(found) && length(found) == 1
            nucInt(i) = found;
        else
            nucInt(i) = length(get_nucs);
        end
    end
end