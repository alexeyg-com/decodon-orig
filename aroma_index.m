function [aroma] = aroma_index(seq)
    arrayLen = length(get_aminos);
    aminos = codon2amino_int(codon2int(seq));
    
    usage = zeros(1, arrayLen);
    for i = 1:length(aminos) - 1
        usage(aminos(i)) = usage(aminos(i)) + 1;
    end
    usage = usage / sum(usage);
    aroma = sum(usage([14 19 18]));
end