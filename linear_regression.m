% Create a stepwise generalized linear regression model
%   DS       - training dataset
%   DISTR    - underlying distribution
%   LINK     - link function
%   UPPER    - upper model complexity
%   WEIGHTS  - regression weights
%   STEPWISE - use stepwise regression?
%
%   MODEL    - created model
function [model] = linear_regression(ds, distr, link, upper, weights, stepwise)
    if nargin < 2
        distr = 'normal';
    end
    if nargin < 3
        link = 'identity';
    end
    if nargin < 4
        upper = 'quadratic';
    end
    if nargin < 5
        weights = [];
    end
    if nargin < 6
        stepwise = true;
    end
    if stepwise
        if isempty(weights)
            model = GeneralizedLinearModel.stepwise(ds, 'constant', 'Upper', upper, 'Penter', 1e-4, 'PRemove', 1e-2, 'ResponseVar', ds.Properties.VarNames{end}, 'Distribution', distr, 'Link', link);
            %model = GeneralizedLinearModel.stepwise(ds, 'constant', 'Upper', upper, 'Penter', 1e-2, 'PRemove', 1e-1, 'ResponseVar', 'mRNA', 'Distribution', distr, 'Link', link);
        else
            model = GeneralizedLinearModel.stepwise(ds, 'constant', 'Upper', upper, 'Penter', 1e-4, 'PRemove', 1e-2, 'ResponseVar', ds.Properties.VarNames{end}, 'Distribution', distr, 'Link', link, 'weights', weights);
        end
    else
        if isempty(weights)
            model = GeneralizedLinearModel.fit(ds, 'linear', 'ResponseVar', ds.Properties.VarNames{end}, 'Distribution', distr, 'Link', link);
        else
            model = GeneralizedLinearModel.fit(ds, 'linear', 'ResponseVar', ds.Properties.VarNames{end}, 'Distribution', distr, 'Link', link, 'weights', weights);
        end
    end
    
end