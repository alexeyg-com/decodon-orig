function [mu sigma x ds] = evaluate_clustring_datasets_proper_cv(feat, folds, step_size, goals)
    if nargin < 2
        folds = 10;
    end
    if nargin < 3
        step_size = 50;
    end
    if nargin < 4
        goals = {'mRNA-lim-mean', 'mRNA-Arava', 'mRNA-count', 'mRNA-seq', 'mRNA-Ingolia', 'Density-Arava', 'Relative-rate-Arava', 'Density-Arava', 'Inv-Density-Arava', 'Inv-Relative-rate-Arava', 'Inv-Density-Arava', 'GFP', 'TAP', 'mRNA-Arava*Density-Arava', 'mRNA-Arava*Relative-rate-Arava', 'mRNA-Arava*Density-Ingolia', 'mRNA-count*Density-Arava', 'mRNA-count*Relative-rate-Arava', 'mRNA-count*Density-Ingolia', 'mRNA-seq*Density-Arava', 'mRNA-seq*Relative-rate-Arava', 'mRNA-seq*Density-Ingolia', 'mRNA-Ingolia*Density-Arava', 'mRNA-Ingolia*Relative-rate-Arava', 'mRNA-Ingolia*Density-Ingolia', 'expected-protein-Ingolia'};
    end
    
    [goals ds] = prepare_datasets(feat, true, true, goals);
    
    x = cell(1, length(goals));
    mu = cell(1, length(goals));
    sigma = cell(1, length(goals));
    
    for i = 1:length(goals)
        num = size(ds{i}, 1);
        
        max_num = floor(num * (folds - 1) / folds);
        
        sizes = 100:step_size:max_num - 100;
        if sizes(end) ~= max_num - 100
            sizes = [sizes max_num - 100];
        end
        
        steps = length(sizes);
        
        fprintf('[i] Processing dataset %s (%d genes, %d steps)\n', goals{i}, num, steps);
        
        mu_kmeans = zeros(1, steps);
        sigma_kmeans = zeros(1, steps);
        
        mu_kmeans_equal = zeros(1, steps);
        sigma_kmeans_equal = zeros(1, steps);
        
        mu_hierarchial = zeros(1, steps);
        sigma_hierarchial = zeros(1, steps);
        
        mu_hierarchial_equal = zeros(1, steps);
        sigma_hierarchial_equal = zeros(1, steps);
        
        parfor j = 1:steps
            cur_size = sizes(j);
            
            [mu_kmeans(j) sigma_kmeans(j)] = kmeans_regression_cv(ds{i}, cur_size, false, 'r2', folds, 'normal', 'identity', 'linear', [], true);
            fprintf('        [i] K-means (%s) %d : %.3f +/- %.3f\n', goals{i}, cur_size, mu_kmeans(j), sigma_kmeans(j));
            
            [mu_kmeans_equal(j) sigma_kmeans_equal(j)] = kmeans_regression_cv(ds{i}, cur_size, true, 'r2', folds, 'normal', 'identity', 'linear', [], true);
            fprintf('        [i] K-means (log-width) (%s) %d : %.3f +/- %.3f\n', goals{i}, cur_size, mu_kmeans(j), sigma_kmeans(j));

            [mu_hierarchial(j) sigma_hierarchial(j)] = hierarchial_regression_cv(ds{i}, cur_size, false, 'r2', folds, 'normal', 'identity', 'linear', [], true);
            fprintf('        [i] Hierarchial complete (%s) %d : %.3f +/- %.3f\n\n', goals{i}, cur_size, mu_hierarchial(j), sigma_hierarchial(j));
            
            [mu_hierarchial_equal(j) sigma_hierarchial_equal(j)] = hierarchial_regression_cv(ds{i}, cur_size, true, 'r2', folds, 'normal', 'identity', 'linear', [], true);
            fprintf('        [i] Hierarchial complete (log-width) (%s) %d : %.3f +/- %.3f\n\n', goals{i}, cur_size, mu_hierarchial(j), sigma_hierarchial(j));
        end
        
        x{i} = sizes;
        mu{i} = [mu_kmeans; mu_kmeans_equal; mu_hierarchial; mu_hierarchial_equal];
        sigma{i} = [sigma_kmeans; sigma_kmeans_equal; sigma_hierarchial; sigma_hierarchial_equal];
    end
end