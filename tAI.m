classdef tAI
    properties
        W
        tGCN
        S
    end
    
    properties (Hidden)
        codon2amino
        trans
    end
    
    methods
        function [obj] = tAI(tGCN, S)
            obj.tGCN = tGCN;
            obj.S = S;
            obj = obj.compute_weights();
        end
        
        function [tai] = calculate_tai(obj, seq, part)
            if all(isnumeric(seq))
                codon = seq;
            else
                codon = codon2int(seq);
            end
            
            if nargin >= 3
                codon = codon(1:min(length(codon), part));
            end
            
            n = 0;
            tai = 0;
            for i = 2:length(codon) - 1
                amino = obj.codon2amino(codon(i));
                if length(obj.trans{amino}) <= 1
                    continue;
                end
                n = n + 1;
                tai = tai + log(obj.W(codon(i)));
            end
            tai = exp(tai / n);
        end
    end
    
    methods (Access = private)
        function [obj] = compute_weights(obj)
            obj.W = zeros(1, length(get_codons));
            compute_order = { 'TTT', 'TTC', 'TTA', 'TTG', 'TCT', 'TCC', 'TCA', 'TCG', 'TAT', 'TAC', 'TAA', 'TAG', 'TGT', 'TGC', 'TGA', 'TGG', 'CTT', 'CTC', 'CTA', 'CTG', 'CCT', 'CCC', 'CCA', 'CCG', 'CAT', 'CAC', 'CAA', 'CAG', 'CGT', 'CGC', 'CGA', 'CGG', 'ATT', 'ATC', 'ATA', 'ATG', 'ACT', 'ACC', 'ACA', 'ACG', 'AAT', 'AAC', 'AAA', 'AAG', 'AGT', 'AGC', 'AGA', 'AGG', 'GTT', 'GTC', 'GTA', 'GTG', 'GCT', 'GCC', 'GCA', 'GCG', 'GAT', 'GAC', 'GAA', 'GAG', 'GGT', 'GGC', 'GGA', 'GGG' };
            
            codon_len = length(get_codons);
            obj.trans = amino2codons_int;
            obj.codon2amino = zeros(1, codon_len);
            for i = 1:codon_len
                obj.codon2amino(i) = codon2amino_int(i);
            end
            
            
            for i = 1:4:length(compute_order)
                a = compute_order{i};
                b = compute_order{i + 1};
                c = compute_order{i + 2};
                d = compute_order{i + 3};
                a = codon2int(a);
                b = codon2int(b);
                c = codon2int(c);
                d = codon2int(d);
                obj.W(a) = (1 - obj.S.IU) * obj.tGCN(a) + (1 - obj.S.GU) * obj.tGCN(b);
                obj.W(b) = (1 - obj.S.GC) * obj.tGCN(b) + (1 - obj.S.IC) * obj.tGCN(a);
                obj.W(c) = (1 - obj.S.UA) * obj.tGCN(c) + (1 - obj.S.IA) * obj.tGCN(a);
                obj.W(d) = (1 - obj.S.CG) * obj.tGCN(d) + (1 - obj.S.UG) * obj.tGCN(c);
            end
            Wmax = max(obj.W(obj.W ~= 0));
            obj.W = obj.W / Wmax;
            Wmean = geomean(obj.W(obj.W ~= 0));
            obj.W(obj.W == 0) = Wmean;
        end
    end
end
