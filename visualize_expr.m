% Plot probe expression levels for two array sets.
%   EXPR1 - array set one
%   EXPR2 - array set two
function [] = visualize_expr(expr1, expr2, hl)
    mu1 = log2(expr1.e_mean);
    mu2 = log2(expr2.e_mean);
    sigma1 = expr1.e_std;
    sigma2 = expr2.e_std;
    [dummy, ind] = sort(mu1);
    n = length(mu1);
    revInd = zeros(1, n);
    x = 1:n;
    for i = 1:n
        revInd(ind(i)) = i;
    end
    
    m = length(hl);
    gene_names = strtok(expr1.desc);
    mv = max([mu1 mu2]);
    hl_ind = [];
    for i = 1:m
        searchInd = find(strcmpi(hl{i}, gene_names));
        if length(searchInd) ~= 0
            hl_ind = [hl_ind searchInd'];
            %fprintf('%s %d\n', hl{i}, revInd(searchInd(1)));
        else
            fprintf('Not found: %s\n', hl{i});
        end
    end
    
    yvalues = repmat([0 mv], [length(hl_ind) 1]);
    xvalues = [revInd(hl_ind); revInd(hl_ind)]';
    
    
    %subplot(2, 1, 1);
    %plot(x, mu1(ind), 'b', xvalues', yvalues', 'r--');
    %axis([1 n 0 mv]);
    %xlabel('Gene #');
    %ylabel('Log-scale average expression');
    %title('Protocol A');
    %subplot(2, 1, 2);
    %plot(x, mu2(ind), 'g', xvalues', yvalues', 'r--');
    %axis([1 n 0 mv]);
    %xlabel('Gene #');
    %ylabel('Log-scale average expression');
    %title('Protocol B');
    
    plot(x, mu2(ind), 'g', x, mu1(ind), 'b', xvalues', yvalues', 'r--');
    axis([1 n 0 mv]);
    xlabel('Gene #');
    ylabel('Log-scale average expression');
    
    %desc = expr1.desc;
    %for i = [n:-1:n - 500]
    %    fprintf('%s\n', desc{ind(i)});
    %end
    
    %errorbar(x, mu(ind), sigma(ind));
    %errorbar
end