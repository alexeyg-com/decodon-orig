classdef FeatureComputer
    properties (Constant = true, Hidden)
        NumberOfFeatures = 169;
    end
    
    properties
        DefaultFeatures
    end
    
    properties (Hidden)
        options 
        CAIcalc
        tAIcalc
        RCAcalc
        CPBcalc
        TPIcalc
        Fop_calc
        CBIcalc
        Pcalc
        RCBScalc
        Dnuc_calc
    end
    
    methods
        function [obj] = FeatureComputer(default_features)
            if nargin < 1
                default_features = 1:obj.NumberOfFeatures;
            end
            
            obj.DefaultFeatures = default_features;
            
            obj.options = get_S288c_options;
            obj.CAIcalc = CAI([]);
            obj.CAIcalc = obj.CAIcalc.set_W(obj.options.W);
            obj.tAIcalc = tAI(obj.options.tGCN, obj.options.S);
            obj.RCAcalc = RCA(obj.options.RCA.W);
            obj.CPBcalc = CPB(obj.options.CPB.CPS);
            obj.TPIcalc = TPI(obj.options.TPI.CodonFrequencies, obj.options.tTrans);
            obj.Fop_calc = Fop(obj.options.tGCN);
            obj.CBIcalc = CBI;
            obj.Pcalc = PIndex(obj.options.PIndex.Ffamily);
            obj.RCBScalc = RCBS_pseudo(obj.options.RCBS);
            obj.Dnuc_calc = Dnuc(obj.options.Dnuc.NativeCodonUsage);
        end
        
        function [feat, combo_seq] = compute_features(obj, combo_seq, which)
            if nargin < 3
                which = obj.DefaultFeatures;
            end
            
            [feat, combo_seq.cache] = obj.do_compute_features(combo_seq, which);
        end
    end
    
    methods (Access = private)
        function [feat, cache] = do_compute_features(obj, combo_seq, which)
            compute = false(1, obj.NumberOfFeatures);
            compute(which) = true;
            feat = zeros(1, sum(compute));
            ind = 0;
            
            if isfield(combo_seq, 'aminos')
                aminos = combo_seq.aminos;
            else
                aminos = [];
            end
            
            rscu = [];
            dinucleotides = [];
            
%            %GC
%            if compute(1)
%                ind = ind + 1;
%                feat(ind) = compute_gc(combo_seq.seq);
%            end

            %GC15
            if compute(1)
                ind = ind + 1;
                feat(ind) = compute_15gc(combo_seq.seq);
            end

%            %GC3
%            if compute(3)
%                ind = ind + 1;
%                feat(ind) = compute_gc3(combo_seq.seq);
%            end

            %len
            if compute(2)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(4);
                else
                    feat(ind) = length(combo_seq.seq);
                end
            end

            %tAI
            if compute(3)
                ind = ind + 1;
                feat(ind) = obj.tAIcalc.calculate_tai(combo_seq.seq);
            end

            %CAI
            if compute(4)
                ind = ind + 1;
                feat(ind) = obj.CAIcalc.calculate_cai(combo_seq.seq);
            end

%            %RCA
%            if compute(7)
%                ind = ind + 1;
%                feat(ind) = obj.RCAcalc.calculate_rca(combo_seq.seq);
%            end

            %Nc
            if compute(5)
                ind = ind + 1;
                feat(ind) = effective_number_codons(combo_seq.seq);
            end

            %Ew
            if compute(6)
                ind = ind + 1;
                feat(ind) = weighted_sum_of_relative_entropy(combo_seq.seq);
            end

%            %SCUO
%            if compute(10)
%                ind = ind + 1;
%                feat(ind) = synonymous_codon_usage_order(combo_seq.seq);
%            end

            %P1
            if compute(7)
                ind = ind + 1;
                feat(ind) = compute_p1(combo_seq.seq, obj.options.tGCN);
            end

%            %P2
%            if compute(12)
%                ind = ind + 1;
%                feat(ind) = p2index(combo_seq.seq);
%            end

            %RCBSpc
            if compute(8)
                ind = ind + 1;
                feat(ind) = obj.RCBScalc.calculate_rcbs(combo_seq.seq);
            end

            %nucA
            if compute(9)
                ind = ind + 1;
                feat(ind) = (sum(combo_seq.seq == 'A') + sum(combo_seq.seq == 'a')) / length(combo_seq.seq);
            end

            %nucC
            if compute(10)
                ind = ind + 1;
                feat(ind) = (sum(combo_seq.seq == 'C') + sum(combo_seq.seq == 'c')) / length(combo_seq.seq);
            end

            %nucT
            if compute(11)
                ind = ind + 1;
                feat(ind) = (sum(combo_seq.seq == 'T') + sum(combo_seq.seq == 't')) / length(combo_seq.seq);
            end

            %nucG
            if compute(12)
                ind = ind + 1;
                feat(ind) = (sum(combo_seq.seq == 'G') + sum(combo_seq.seq == 'g')) / length(combo_seq.seq);
            end

            %nuc_
            if compute(13)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(18);
                else
                    feat(ind) = (sum((combo_seq.seq == 'A') + (combo_seq.seq == 'a') + (combo_seq.seq == 'C') + (combo_seq.seq == 'c') + (combo_seq.seq == 'G') + (combo_seq.seq == 'g') + (combo_seq.seq == 'T') + (combo_seq.seq == 't') == 0)) / length(combo_seq.seq);
                end
            end

            %aminoA
            if compute(14)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(19);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(1);
                end
            end

            %aminoR
            if compute(15)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(20);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(2);
                end
            end

            %aminoN
            if compute(16)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(21);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(3);
                end
            end

            %aminoD
            if compute(17)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(22);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(4);
                end
            end

            %aminoC
            if compute(18)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(23);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(5);
                end
            end

            %aminoQ
            if compute(19)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(24);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(6);
                end
            end

            %aminoE
            if compute(20)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(25);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(7);
                end
            end

            %aminoG
            if compute(21)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(26);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(8);
                end
            end

            %aminoH
            if compute(22)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(27);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(9);
                end
            end

            %aminoI
            if compute(23)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(28);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(10);
                end
            end

            %aminoL
            if compute(24)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(29);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(11);
                end
            end

            %aminoK
            if compute(25)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(30);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(12);
                end
            end

            %aminoM
            if compute(26)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(31);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(13);
                end
            end

            %aminoF
            if compute(27)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(32);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(14);
                end
            end

            %aminoP
            if compute(28)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(33);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(15);
                end
            end

            %aminoS
            if compute(29)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(34);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(16);
                end
            end

            %aminoT
            if compute(30)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(35);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(17);
                end
            end

            %aminoW
            if compute(31)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(36);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(18);
                end
            end

            %aminoY
            if compute(32)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(39);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(19);
                end
            end

            %aminoV
            if compute(33)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(38);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(20);
                end
            end

            %amino_
            if compute(34)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(39);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(21);
                end
            end

            %amino__
            if compute(35)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(40);
                else
                    if isempty(aminos)
                        aminos = compute_amino_usage(combo_seq.seq);
                    end
                    feat(ind) = aminos(22);
                end
            end

            %rscuGGG
            if compute(36)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(1);
            end

            %rscuGGT
            if compute(37)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(2);
            end

            %rscuGGC
            if compute(38)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(3);
            end

            %rscuGGA
            if compute(39)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(4);
            end

            %rscuGTG
            if compute(40)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(5);
            end

            %rscuGTT
            if compute(41)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(6);
            end

            %rscuGTC
            if compute(42)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(7);
            end

            %rscuGTA
            if compute(43)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(8);
            end

            %rscuGCG
            if compute(44)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(9);
            end

            %rscuGCT
            if compute(45)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(10);
            end

            %rscuGCC
            if compute(46)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(11);
            end

            %rscuGCA
            if compute(47)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(12);
            end

            %rscuGAG
            if compute(48)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(13);
            end

            %rscuGAT
            if compute(49)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(14);
            end

            %rscuGAC
            if compute(50)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(15);
            end

            %rscuGAA
            if compute(51)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(16);
            end

            %rscuTGG
            if compute(52)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(17);
            end

            %rscuTGT
            if compute(53)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(18);
            end

            %rscuTGC
            if compute(54)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(19);
            end

            %rscuTGA
            if compute(55)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(20);
            end

            %rscuTTG
            if compute(56)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(21);
            end

            %rscuTTT
            if compute(57)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(22);
            end

            %rscuTTC
            if compute(58)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(23);
            end

            %rscuTTA
            if compute(59)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(24);
            end

            %rscuTCG
            if compute(60)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(25);
            end

            %rscuTCT
            if compute(61)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(26);
            end

            %rscuTCC
            if compute(62)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(27);
            end

            %rscuTCA
            if compute(63)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(28);
            end

            %rscuTAG
            if compute(64)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(29);
            end

            %rscuTAT
            if compute(65)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(30);
            end

            %rscuTAC
            if compute(66)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(31);
            end

            %rscuTAA
            if compute(67)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(32);
            end

            %rscuCGG
            if compute(68)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(33);
            end

            %rscuCGT
            if compute(69)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(34);
            end

            %rscuCGC
            if compute(70)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(35);
            end

            %rscuCGA
            if compute(71)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(36);
            end

            %rscuCTG
            if compute(72)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(37);
            end

            %rscuCTT
            if compute(73)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(38);
            end

            %rscuCTC
            if compute(74)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(39);
            end

            %rscuCTA
            if compute(75)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(40);
            end

            %rscuCCG
            if compute(76)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(41);
            end

            %rscuCCT
            if compute(77)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(42);
            end

            %rscuCCC
            if compute(78)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(43);
            end

            %rscuCCA
            if compute(79)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(44);
            end

            %rscuCAG
            if compute(80)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(45);
            end

            %rscuCAT
            if compute(81)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(46);
            end

            %rscuCAC
            if compute(82)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(47);
            end

            %rscuCAA
            if compute(83)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(48);
            end

            %rscuAGG
            if compute(84)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(49);
            end

            %rscuAGT
            if compute(85)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(50);
            end

            %rscuAGC
            if compute(86)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(51);
            end

            %rscuAGA
            if compute(87)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(52);
            end

            %rscuATG
            if compute(88)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(53);
            end

            %rscuATT
            if compute(89)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(54);
            end

            %rscuATC
            if compute(90)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(55);
            end

            %rscuATA
            if compute(91)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(56);
            end

            %rscuACG
            if compute(92)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(57);
            end

            %rscuACT
            if compute(93)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(58);
            end

            %rscuACC
            if compute(94)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(59);
            end

            %rscuACA
            if compute(95)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(60);
            end

            %rscuAAG
            if compute(96)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(61);
            end

            %rscuAAT
            if compute(97)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(62);
            end

            %rscuAAC
            if compute(98)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(63);
            end

            %rscuAAA
            if compute(99)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(64);
            end

            %rscu___
            if compute(100)
                ind = ind + 1;
                if isempty(rscu)
                    rscu = compute_rscu(combo_seq.seq);
                end
                feat(ind) = rscu(65);
            end

%            %P
%            if compute(106)
%                ind = ind + 1;
%                feat(ind) = obj.Pcalc.calculate_p(combo_seq.seq);
%            end

            %CPB
            if compute(101)
                ind = ind + 1;
                feat(ind) = obj.CPBcalc.calculate_cpb(combo_seq.seq);
            end

            %TPI
            if compute(102)
                ind = ind + 1;
                feat(ind) = obj.TPIcalc.calculate_tpi(combo_seq.seq);
            end

            %GRAVY
            if compute(103)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(109);
                else
                    feat(ind) = gravy_index(combo_seq.seq);
                end
            end

            %AROMA
            if compute(104)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(110);
                else
                    feat(ind) = aroma_index(combo_seq.seq);
                end
            end

            %Aliphatic
            if compute(105)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(111);
                else
                    feat(ind) = aliphatic_index(combo_seq.seq);
                end
            end

            %Instability
            if compute(106)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(112);
                else
                    feat(ind) = instability_index(combo_seq.seq);
                end
            end

            %SideCharge
            if compute(107)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(113);
                else
                    feat(ind) = side_charge(combo_seq.seq);
                end
            end

            %tAI14
            if compute(108)
                ind = ind + 1;
                feat(ind) = obj.tAIcalc.calculate_tai(combo_seq.seq, 14);
            end

            %tAI17
            if compute(109)
                ind = ind + 1;
                feat(ind) = obj.tAIcalc.calculate_tai(combo_seq.seq, 17);
            end

            %tAI19
            if compute(110)
                ind = ind + 1;
                feat(ind) = obj.tAIcalc.calculate_tai(combo_seq.seq, 19);
            end

            %SideCharge4
            if compute(111)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(117);
                else
                    feat(ind) = side_charge(combo_seq.seq, 4) * 4;
                end
            end

            %SideCharge11
            if compute(112)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(118);
                else
                    feat(ind) = side_charge(combo_seq.seq, 11) * 11;
                end
            end

            %SideCharge15
            if compute(113)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(119);
                else
                    feat(ind) = side_charge(combo_seq.seq, 15) * 15;
                end
            end

            %SideCharge40
            if compute(114)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(120);
                else
                    feat(ind) = side_charge(combo_seq.seq, 40) * 40;
                end
            end

            %CAI2nd
            if compute(115)
                ind = ind + 1;
                codon2nd = codon2int(combo_seq.seq(4:6));
                feat(ind) = obj.options.W(codon2nd);
            end

            %tAI2nd
            if compute(116)
                ind = ind + 1;
                codon2nd = codon2int(combo_seq.seq(4:6));
                feat(ind) = obj.tAIcalc.W(codon2nd);
            end

%            %dG_CDS
%            if compute(123)
%                ind = ind + 1;
%                feat(ind) = RNAfold_cds(combo_seq.seq);
%            end

            %dG_CDS17
            if compute(117)
                ind = ind + 1;
                feat(ind) = RNAfold_cds(combo_seq.seq, 17);
            end

            %dG_CDS34
            if compute(118)
                ind = ind + 1;
                feat(ind) = RNAfold_cds(combo_seq.seq, 34);
            end

            %dG_CDS53
            if compute(119)
                ind = ind + 1;
                feat(ind) = RNAfold_cds(combo_seq.seq, 53);
            end

            %pI
            if compute(120)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(127);
                else
                    feat(ind) = isoelectric_point(combo_seq.seq);
                end
            end

            %MolWeight
            if compute(121)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(128);
                else
                    feat(ind) = protein_weight(combo_seq.seq);
                end
            end

            %Charge
            if compute(122)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(129);
                else
                    feat(ind) = protein_charge(combo_seq.seq);
                end
            end

            %Fop
            if compute(123)
                ind = ind + 1;
                feat(ind) = obj.Fop_calc.calculate_fop(combo_seq.seq);
            end

%            %CBI
%            if compute(131)
%                ind = ind + 1;
%                feat(ind) = obj.CBIcalc.calculate_cbi(combo_seq.seq);
%            end

            %len_3UTR
            if compute(124)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(132);
                else
                    feat(ind) = length(combo_seq.utr3);
                end
            end

            %dG_3UTR
            if compute(125)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(133);
                else
                    feat(ind) = RNAfold(combo_seq.utr3);
                end
            end

            %len_5UTR
            if compute(126)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(134);
                else
                    feat(ind) = length(combo_seq.utr5);
                end
            end

            %dG_5UTR
            if compute(127)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(135);
                else
                    feat(ind) = RNAfold_utr5(combo_seq.utr5);
                end
            end

            %dG_5UTR50
            if compute(128)
                ind = ind + 1;
                if isfield(combo_seq, 'cache')
                    feat(ind) = combo_seq.cache(136);
                else
                    feat(ind) = RNAfold_utr5(combo_seq.utr5, 50);
                end
            end

            %dG_5UTR50_CDS17
            if compute(129)
                ind = ind + 1;
                feat(ind) = RNAfold_utr5cds(combo_seq.utr5, combo_seq.seq, 50, 17);
            end

            %dG_5UTR50_CDS34
            if compute(130)
                ind = ind + 1;
                feat(ind) = RNAfold_utr5cds(combo_seq.utr5, combo_seq.seq, 50, 34);
            end

            %dG_5UTR50_CDS53
            if compute(131)
                ind = ind + 1;
                feat(ind) = RNAfold_utr5cds(combo_seq.utr5, combo_seq.seq, 50, 53);
            end

            %dG_5UTR_CDS17
            if compute(132)
                ind = ind + 1;
                feat(ind) = RNAfold_utr5cds(combo_seq.utr5, combo_seq.seq, -1, 17);
            end

            %dG_5UTR_CDS34
            if compute(133)
                ind = ind + 1;
                feat(ind) = RNAfold_utr5cds(combo_seq.utr5, combo_seq.seq, -1, 34);
            end

            %dG_5UTR_CDS53
            if compute(134)
                ind = ind + 1;
                feat(ind) = RNAfold_utr5cds(combo_seq.utr5, combo_seq.seq, -1, 53);
            end

%            %dG_full
%            if compute(143)
%                ind = ind + 1;
%                feat(ind) = RNAfold([combo_seq.utr5 combo_seq.seq combo_seq.utr3]);
%            end
            
            %Dnuc
            if compute(135)
                ind = ind + 1;
                feat(ind) = obj.Dnuc_calc.calculate_dnuc(combo_seq.seq);
            end
            
            %dinucAA
            if compute(136)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(1);
            end
            
            %dinucAC
            if compute(137)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(2);
            end

            %dinucAT
            if compute(138)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(3);
            end
            
            %dinucAG
            if compute(139)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(4);
            end
            
            %dinucA_
            if compute(140)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(5);
            end
            
            %dinucCA
            if compute(141)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(6);
            end
            
            %dinucCC
            if compute(142)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(7);
            end
            
            %dinucCT
            if compute(143)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(8);
            end
            
            %dinucCG
            if compute(144)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(9);
            end
            
            %dinucC_
            if compute(145)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(10);
            end
            
            %dinucTA
            if compute(146)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(11);
            end
            
            %dinucTC
            if compute(147)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(12);
            end
            
            %dinucTT
            if compute(148)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(13);
            end
            
            %dinucTG
            if compute(149)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(14);
            end
            
            %dinucT_
            if compute(150)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(15);
            end
            
            %dinucGA
            if compute(151)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(16);
            end
            
            %dinucGC
            if compute(152)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(17);
            end
            
            %dinucGT
            if compute(153)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(18);
            end
            
            %dinucGG
            if compute(154)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(19);
            end
            
            %dinucG_
            if compute(155)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(20);
            end
            
            %dinuc_A
            if compute(156)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(21);
            end
            
            %dinuc_C
            if compute(157)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(22);
            end
            
            %dinuc_T
            if compute(158)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(23);
            end
            
            %dinuc_G
            if compute(159)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(24);
            end
            
            %dinuc__
            if compute(160)
                ind = ind + 1;
                if isempty(dinucleotides)
                    dinucleotides = compute_dinucleotides(combo_seq.seq);
                end
                feat(ind) = dinucleotides(25);
            end
            
            cache = zeros(1, obj.NumberOfFeatures);
            cache(compute) = feat;
        end
    end
end
