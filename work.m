% Batches for proper CV
% {'mRNA-lim-mean', 'mRNA-Arava', 'mRNA-count', 'mRNA-seq', 'mRNA-Ingolia', 'Density-Arava', 'Relative-rate-Arava'}
% {'Density-Arava', 'Inv-Density-Arava', 'Inv-Relative-rate-Arava', 'Inv-Density-Arava', 'GFP', 'TAP', 'mRNA-Arava*Density-Arava'}
% {'mRNA-Arava*Relative-rate-Arava', 'mRNA-Arava*Density-Ingolia', 'mRNA-count*Density-Arava', 'mRNA-count*Relative-rate-Arava', 'mRNA-count*Density-Ingolia', 'mRNA-seq*Density-Arava', 'mRNA-seq*Relative-rate-Arava'}
% {'mRNA-seq*Density-Ingolia', 'mRNA-Ingolia*Density-Arava', 'mRNA-Ingolia*Relative-rate-Arava', 'mRNA-Ingolia*Density-Ingolia', 'expected-protein-Ingolia'}

% {'mRNA-Lu', 'Density-Ingolia-corrected', 'Inv-density-Ingolia-corrected', 'Protein-Lu', 'GFP/mRNA-Lu', 'TAP/mRNA-Lu', 'Protein-Lu/mRNA-Arava'}
% {'Protein-Lu/mRNA-count', 'Protein-Lu/mRNA-seq', 'Protein-Lu/mRNA-Ingolia', 'Protein-Lu/mRNA-Lu', 'mRNA-Arava*Density-Ingolia-corrected', 'mRNA-Ingolia*Density-Ingolia-corrected', 'mRNA-count*Density-Ingolia-corrected'}
% {'mRNA-seq*Density-Ingolia-corrected', 'mRNA-Lu*Density-Arava', 'mRNA-Lu*Relative-rate-Arava', 'mRNA-Lu*Density-Ingolia', 'mRNA-Lu*Density-Ingolia-corrected', 'expected-protein-Ingolia-corrected'}


% +Kanta  %[mu sigma x ds] = evaluate_clustring_datasets_proper_cv(feat, 10, 50, {'mRNA-lim-mean', 'mRNA-Arava', 'mRNA-count', 'mRNA-seq', 'mRNA-Ingolia', 'Density-Arava', 'Relative-rate-Arava'}); save proper-cv-batch1;
% +Kanta  %[mu sigma x ds] = evaluate_clustring_datasets_proper_cv(feat, 10, 50, {'Density-Arava', 'Inv-Density-Arava', 'Inv-Relative-rate-Arava', 'Inv-Density-Arava', 'GFP', 'TAP', 'mRNA-Arava*Density-Arava'}); save proper-cv-batch2;
% +Kanta  %[mu sigma x ds] = evaluate_clustring_datasets_proper_cv(feat, 10, 50, {'mRNA-Arava*Relative-rate-Arava', 'mRNA-Arava*Density-Ingolia', 'mRNA-count*Density-Arava', 'mRNA-count*Relative-rate-Arava', 'mRNA-count*Density-Ingolia', 'mRNA-seq*Density-Arava', 'mRNA-seq*Relative-rate-Arava'}); save proper-cv-batch3;
% +Kanta  %[mu sigma x ds] = evaluate_clustring_datasets_proper_cv(feat, 10, 50, {'mRNA-seq*Density-Ingolia', 'mRNA-Ingolia*Density-Arava', 'mRNA-Ingolia*Relative-rate-Arava', 'mRNA-Ingolia*Density-Ingolia', 'expected-protein-Ingolia'}); save proper-cv-batch4;
% +Kanta  %[mu sigma x ds] = evaluate_clustring_datasets_proper_cv(feat, 10, 50, {'mRNA-Lu', 'Density-Ingolia-corrected', 'Inv-density-Ingolia-corrected', 'Protein-Lu', 'GFP/mRNA-Lu', 'TAP/mRNA-Lu', 'Protein-Lu/mRNA-Arava'}); save proper-cv-batch5;
% +Kanta  %[mu sigma x ds] = evaluate_clustring_datasets_proper_cv(feat, 10, 50, {'Protein-Lu/mRNA-count', 'Protein-Lu/mRNA-seq', 'Protein-Lu/mRNA-Ingolia', 'Protein-Lu/mRNA-Lu', 'mRNA-Arava*Density-Ingolia-corrected', 'mRNA-Ingolia*Density-Ingolia-corrected', 'mRNA-count*Density-Ingolia-corrected'}); save proper-cv-batch6;
% +Kanta  %[mu sigma x ds] = evaluate_clustring_datasets_proper_cv(feat, 10, 50, {'mRNA-seq*Density-Ingolia-corrected', 'mRNA-Lu*Density-Arava', 'mRNA-Lu*Relative-rate-Arava', 'mRNA-Lu*Density-Ingolia', 'mRNA-Lu*Density-Ingolia-corrected', 'expected-protein-Ingolia-corrected'}); save proper-cv-batch7;


% +Gauss % [mu sigma x ds] = evaluate_clustring_datasets_proper_cv(feat, 10, 50, {'expected-protein-ingolia', 'expected-protein-ingolia-corrected', 'mRNA-count'}); save proper-cv-s2-1;
% +Gauss % [mu sigma x ds] = evaluate_clustring_datasets_proper_cv(feat, 10, 50, {'density-ingolia', 'density-ingolia-corrected', 'protein-lu', 'gfp'}); save proper-cv-s2-2;

% function [] = work(ids, feat)
%     names = { '\text{GC}', '\text{GC}_{15}', '\text{GC3}', '\text{len}', '\text{tAI}', '\text{CAI}', '\text{RCA}', 'N_c', 'E_w', '\text{SCUO}', 'P_1', 'P_2', '\text{RCBS}^{pc}', '\text{A}_\text{nuc}', '\text{C}_\text{nuc}', '\text{T}_\text{nuc}', '\text{G}_\text{nuc}', '\text{X}_\text{nuc}', '\text{A}', '\text{R}', '\text{N}', '\text{D}', '\text{C}', '\text{Q}', '\text{E}', '\text{G}', '\text{H}', '\text{I}', '\text{L}', '\text{K}', '\text{M}', '\text{F}', '\text{P}', '\text{S}', '\text{T}', '\text{W}', '\text{Y}', '\text{V}', '\text{*}', '\text{X}', '\text{RSCU}_\text{GGG}', '\text{RSCU}_\text{GGT}', '\text{RSCU}_\text{GGC}', '\text{RSCU}_\text{GGA}', '\text{RSCU}_\text{GTG}', '\text{RSCU}_\text{GTT}', '\text{RSCU}_\text{GTC}', '\text{RSCU}_\text{GTA}', '\text{RSCU}_\text{GCG}', '\text{RSCU}_\text{GCT}', '\text{RSCU}_\text{GCC}', '\text{RSCU}_\text{GCA}', '\text{RSCU}_\text{GAG}', '\text{RSCU}_\text{GAT}', '\text{RSCU}_\text{GAC}', '\text{RSCU}_\text{GAA}', '\text{RSCU}_\text{TGG}', '\text{RSCU}_\text{TGT}', '\text{RSCU}_\text{TGC}', '\text{RSCU}_\text{TGA}', '\text{RSCU}_\text{TTG}', '\text{RSCU}_\text{TTT}', '\text{RSCU}_\text{TTC}', '\text{RSCU}_\text{TTA}', '\text{RSCU}_\text{TCG}', '\text{RSCU}_\text{TCT}', '\text{RSCU}_\text{TCC}', '\text{RSCU}_\text{TCA}', '\text{RSCU}_\text{TAG}', '\text{RSCU}_\text{TAT}', '\text{RSCU}_\text{TAC}', '\text{RSCU}_\text{TAA}', '\text{RSCU}_\text{CGG}', '\text{RSCU}_\text{CGT}', '\text{RSCU}_\text{CGC}', '\text{RSCU}_\text{CGA}', '\text{RSCU}_\text{CTG}', '\text{RSCU}_\text{CTT}', '\text{RSCU}_\text{CTC}', '\text{RSCU}_\text{CTA}', '\text{RSCU}_\text{CCG}', '\text{RSCU}_\text{CCT}', '\text{RSCU}_\text{CCC}', '\text{RSCU}_\text{CCA}', '\text{RSCU}_\text{CAG}', '\text{RSCU}_\text{CAT}', '\text{RSCU}_\text{CAC}', '\text{RSCU}_\text{CAA}', '\text{RSCU}_\text{AGG}', '\text{RSCU}_\text{AGT}', '\text{RSCU}_\text{AGC}', '\text{RSCU}_\text{AGA}', '\text{RSCU}_\text{ATG}', '\text{RSCU}_\text{ATT}', '\text{RSCU}_\text{ATC}', '\text{RSCU}_\text{ATA}', '\text{RSCU}_\text{ACG}', '\text{RSCU}_\text{ACT}', '\text{RSCU}_\text{ACC}', '\text{RSCU}_\text{ACA}', '\text{RSCU}_\text{AAG}', '\text{RSCU}_\text{AAT}', '\text{RSCU}_\text{AAC}', '\text{RSCU}_\text{AAA}', '\text{RSCU}_\text{(*)}', 'P', '\text{CPB}', '\text{TPI}', '\text{GRAVY}', '\text{AROMA}', '\text{Aliphatic}', '\text{Instability}', '\text{SideCharge}', '\text{tAI}_{14}', '\text{tAI}_{17}', '\text{tAI}_{19}', '\text{SideCharge}_{4}', '\text{SideCharge}_{11}', '\text{SideCharge}_{15}', '\text{SideCharge}_{40}', '\text{CAI}_{2^\text{nd}}', '\text{tAI}_{2^\text{nd}}', '\Delta G_\text{CDS}', '\Delta G_{\text{CDS}_{17}}', '\Delta G_{\text{CDS}_{34}}', '\Delta G_{\text{CDS}_{53}}', '\text{pI}', '\text{Weight}', '\text{Charge}', 'F_{op}', '\text{CBI}', '\text{len}_\text{3UTR}', '\Delta G_{3UTR}', '\text{len}_{\text{5UTR}}', '\Delta G_{\text{5UTR}}', '\Delta G_{\text{5UTR}_{50}}', '\Delta G_{\text{5UTR}_{50},\text{CDS}_{17}}', '\Delta G_{\text{5UTR}_{50},\text{CDS}_{34}}', '\Delta G_{\text{5UTR}_{50},\text{CDS}_{53}}', '\Delta G_{\text{5UTR},\text{CDS}_{17}}', '\Delta G_{\text{5UTR},\text{CDS}_{34}}', '\Delta G_{\text{5UTR},\text{CDS}_{53}}', '\Delta G_{full}' };
%     for i = 1:length(ids)
%         fprintf('$%s$ ', names{ids(i)});
%         for j = 1:size(feat, 1)
%             fprintf('& %.2f ', feat(j, i));
%         end
%         fprintf('\\tabularnewline\n');
%     end
% end

function [model] = work(ds)
%    % Expected protein Ingolia (RSCU, cut, dG)
%    model.features = [83 134 12 116 17 68 25 132 94 93 55 18 47 27 119 147 86 146 78 120 67 142 71 79 103 137 126 1 43 121 72 92 73 74 53 52 44 105 141 102 48 42 99 122 2 104 40 36 39 98 77 84 125 80 96 81 85 115 76 20 75 19 97 109 57 60 65 66 110 46 45 3 136 135 124 6 7 123];
%    model.exec_string = '-s 3 -t 1 -r 1 -d 4 -g 0.027 -c 0.001 -p 0.027 -q';

    % Expected protein Ingolia (RSCU, cut, dG, cut dataset)
    model.features = [112  116  24  77  78  105  28  79  134  153  69  13  76  83  135  139  118  11  154  119  142  72  30  31  155  149  75  110  147  88  109  70  71  130  66  67  111  127  25  106  26  143  90  128  117  148  23  42  94  65  144  138  3  74  44  46  57  40  97  41  10  45  115  132  123  64  9  73  129  131  80  136  137  81  54  55  47  48  122  82  87  92  93  39  38  37  35  4  125  124  5  8];
    model.exec_string = '-s 3 -t 1 -r 1 -d 3 -g 0.027 -c 0.003 -p 0.027 -q';
    
    % Lu (RSCU, dG)
    %model.features = [32 114 15 46 86 27 111 61 24 141 26 45 2 142 33 105 53 58 92 125 106 34 28 1 36 54 87 93 89 137 50 85 55 14 31 126 6 98 56 127 128 64 104 29 134 68 133 69 107 20 30 52 129 41 132 5 21 130 77 109 10 113 60 19 25 18 119 12 131 124 117 63 118 110 70 78 71 17 72 112 39 73 139 154 74 16 146 148 149 103 108 116 122 44 123 37 115 38 144 40 43 47 102 79 80 48 49 51 84 75 76 121 138 57 59 143 120 3 95 81 152 11 82 83 151 96 67 153 65 136 99 66 97 4 7 135 8 9];
    %model.exec_string = '-s 3 -t 1 -r 1 -d 5 -g 0.009 -c 0.009 -p 0.001 -q';
    
    model.features = sort(model.features);
    ds = ds(:, [model.features end]);
    X = double(ds(:, 1:end - 1));
    y = double(ds(:, end));
    
    model.feature_mean = mean(X);
    model.feature_std = std(X);
    model.predictor_mean = mean(y);
    model.predictor_std = std(y);
    
    X = zscore(X);
    y = zscore(y);
    
    model.model = svmtrain(y, X, model.exec_string);
end

% function [model] = work(feat)
%     % Expected protein Ingolia
%     % model.features = [2 4 5 6 7 8 9 10 11 13 14 21 23 24 25 27 35 39 41 43 44 45 47 48 49 50 53 55 57 58 59 65 66 70 71 73 75 76 77 79 80 81 82 84 85 86 87 88 89 91 95 96 97 98 99 101 102 103 104 106 108 109 110 112 113 114 115 116 117 118 124 126 127 128 129 130 131 136 137];
%     % model.exec_string = '-s 4 -t 1 -d 5 -g 0.0090 -r 1.0000 -c 0.0030 -n 0.7290 -p 0.7290 -q';
%     % ds = make_dataset(feat, 'expected-protein-ingolia');
%     
%     % GFP
%     %model.features = [4 5 6 7 8 13 20 21 30 31 32 34 39 42 46 47 48 54 55 56 57 61 67 69 70 71 88 102 107 109 116 120 123 124 127 128 129 130 131 135 143];
%     %model.exec_string = '-s 3 -t 2 -d 0 -g 0.0090 -r 0.0000 -c 2.1870 -n 0.0270 -p 0.0270 -q';
%     %ds = make_dataset(feat, 'gfp');
%     
%     % Protein Lu
%     %model.features = [1 3 4 5 6 7 8 9 10 11 13 14 15 19 20 21 22 24 28 30 35 37 39 42 45 52 53 56 58 68 70 73 74 75 77 78 80 81 82 83 85 86 87 88 98 100 106 107 109 118 121 123 125 127 128 129 131 134 135 138 140 141 142 143];
%     %model.exec_string = '-s 4 -t 2 -d 0 -g 0.0090 -r 0.0000 -c 2.1870 -n 0.7290 -p 0.7290 -q';
%     %ds = make_dataset(feat, 'protein-lu');
%     
%     ds = ds(:, [model.features end]);
%     X = double(ds(:, 1:end - 1));
%     y = double(ds(:, end));
%     
%     model.feature_mean = mean(X);
%     model.feature_std = std(X);
%     model.predictor_mean = mean(y);
%     model.predictor_std = std(y);
%     
%     X = zscore(X);
%     y = zscore(y);
%     
%     model.model = svmtrain(y, X, model.exec_string);
% end

% function [] = work(mu, sigma, x, num)
%     %names = {'Expected-Protein-Ingolia', 'Expected-Protein-Ingolia-corrected', 'mRNA-count', 'Density-Ingolia', 'Density-Ingolia-corrected', 'Protein-Lu', 'GFP'};
%     names = {'Density-Ingolia', 'Density-Ingolia-corrected', 'Protein-Lu', 'GFP'};
%     
%     mu_plot = mu{num};
%     sigma_plot = sigma{num};
%     %x_plot = repmat(x{num}, 2, 1);
%     x_plot = x{num};
%     
%     figure;
%     hold on;
%     plot(x_plot, mu_plot(1, :), 'b-');
%     plot(x_plot, mu_plot(2, :), 'b--');
%     plot(x_plot, mu_plot(3, :), 'g-');
%     plot(x_plot, mu_plot(4, :), 'g--');
%     hold off;
%     axis([90 max(x_plot(1, :)) + 10 0 1]);
%     legend('K-means', 'K-means (log width)', 'Hierarchical (complete)', 'Hierarchical (complete, log width)');
%     xlabel('Number of clusters', 'FontSize', 19);
%     ylabel('S^2', 'FontSize', 19);
%     
%     save_name = names{num};
%     save_name = strrep(save_name, '*', 'X');
%     save_name = strrep(save_name, '/', '-');
%     
%     saveas(gcf, sprintf('clustering-s2-%s.eps', save_name), 'epsc');
%     
%     fprintf('\\begin{figure}[t]\n');
%     fprintf('\\center\n');
%     fprintf('\\includegraphics[width=0.45\\textwidth]{clustering-s2-%s}\n', save_name);
%     fprintf('\\caption{Linear regression for %s.}\n', save_name);
%     fprintf('\\label{fig:clustering-s2-%s}\n', save_name);
%     fprintf('\\end{figure}\n\n');
% end

% function [] = work(batch, num)
%     names = {};
%     if batch == 1
%         names = {'mRNA-lim-mean', 'mRNA-Arava', 'mRNA-count', 'mRNA-seq', 'mRNA-Ingolia', 'Density-Arava', 'Relative-rate-Arava'};
%     elseif batch == 2
%         names = {'Density-Arava', 'Inv-Density-Arava', 'Inv-Relative-rate-Arava', 'Inv-Density-Arava', 'GFP', 'TAP', 'mRNA-Arava*Density-Arava'};
%     elseif batch == 3
%         names = {'mRNA-Arava*Relative-rate-Arava', 'mRNA-Arava*Density-Ingolia', 'mRNA-count*Density-Arava', 'mRNA-count*Relative-rate-Arava', 'mRNA-count*Density-Ingolia', 'mRNA-seq*Density-Arava', 'mRNA-seq*Relative-rate-Arava'};
%     elseif batch == 4
%         names = {'mRNA-seq*Density-Ingolia', 'mRNA-Ingolia*Density-Arava', 'mRNA-Ingolia*Relative-rate-Arava', 'mRNA-Ingolia*Density-Ingolia', 'expected-protein-Ingolia'};
%     elseif batch == 5
%         names = {'mRNA-Lu', 'Density-Ingolia-corrected', 'Inv-density-Ingolia-corrected', 'Protein-Lu', 'GFP/mRNA-Lu', 'TAP/mRNA-Lu', 'Protein-Lu/mRNA-Arava'};
%     elseif batch == 6
%         names = {'Protein-Lu/mRNA-count', 'Protein-Lu/mRNA-seq', 'Protein-Lu/mRNA-Ingolia', 'Protein-Lu/mRNA-Lu', 'mRNA-Arava*Density-Ingolia-corrected', 'mRNA-Ingolia*Density-Ingolia-corrected', 'mRNA-count*Density-Ingolia-corrected'};
%     elseif batch == 7
%         names = {'mRNA-seq*Density-Ingolia-corrected', 'mRNA-Lu*Density-Arava', 'mRNA-Lu*Relative-rate-Arava', 'mRNA-Lu*Density-Ingolia', 'mRNA-Lu*Density-Ingolia-corrected', 'expected-protein-Ingolia-corrected'};
%     end
%     
%     sizes = [7 7 7 5 7 7 6];
%     
%     load_str = sprintf('%s%d', 'proper-cv-batch', batch);
%     %load load_str mu sigma x removed
%     load(load_str, 'mu', 'sigma', 'x', 'removed');
%     mu_plot = mu{num};
%     sigma_plot = sigma{num};
%     x_plot = repmat(x{num}, 2, 1);
%     
%     figure;
%     hold on;
%     plot(x_plot(1, :), mu_plot(1, :), 'b-');
%     plot(x_plot(2, :), mu_plot(2, :), 'g-');
%     hold off;
%     axis([90 max(x_plot(1, :)) + 10 0 1]);
%     legend('K-means', 'Hierarchical (complete)');
%     xlabel('Number of clusters', 'FontSize', 19);
%     ylabel('Q^2', 'FontSize', 19);
%     
%     save_name = names{num};
%     save_name = strrep(save_name, '*', 'X');
%     save_name = strrep(save_name, '/', '-');
%     
%     saveas(gcf, sprintf('clustering-%s.eps', save_name), 'epsc');
%     
%     fprintf('\\begin{figure}[t]\n');
%     fprintf('\\center\n');
%     fprintf('\\includegraphics[width=0.45\\textwidth]{clustering-%s}\n', save_name);
%     fprintf('\\caption{Linear regression for %s.}\n', save_name);
%     fprintf('\\label{fig:clustering-%s}\n', save_name);
%     fprintf('\\end{figure}\n\n');
% end

% function [] = work(correlations)
%     n = size(correlations, 1);
%     names = { '$\text{mean}(\text{mRNA}_\text{lim})$', '$\max(\text{mRNA}_\text{lim})$', '$\min(\text{mRNA}_\text{lim})$', '$\text{mRNA}_\text{Arava}$', '$\text{mRNA}_\text{Ingolia}$', '$\text{mRNA}_\text{count}$', '$\text{mRNA}_\text{seq}$', '$\text{mRNA}_\text{Lu}$', '$D_\text{Arava}$', '$\text{Rate}_\text{Arava}$', '$D_\text{Ingolia}$', '$D^\text{corr}_\text{Ingolia}$', '$D^{-1}_\text{Arava}$', '$\text{Rate}^{-1}_\text{Arava}$', '$D^{-1}_\text{Ingolia}$', '${D^\text{corr}_\text{Ingolia}}^{-1}$', '$\text{GFP}$', '$\text{TAP}$', '$\text{Protein}_\text{Lu}$', '$\frac{\text{GFP}}{\text{mRNA}_\text{Arava}}$', '$\frac{\text{GFP}}{\text{mRNA}_\text{count}}$', '$\frac{\text{GFP}}{\text{mRNA}_\text{seq}}$', '$\frac{\text{GFP}}{\text{mRNA}_\text{Ingolia}}$', '$\frac{\text{GFP}}{\text{mRNA}_\text{Lu}}$', '$\frac{\text{TAP}}{\text{mRNA}_\text{Arava}}$', '$\frac{\text{TAP}}{\text{mRNA}_\text{count}}$', '$\frac{\text{TAP}}{\text{mRNA}_\text{seq}}$', '$\frac{\text{TAP}}{\text{mRNA}_\text{Ingolia}}$', '$\frac{\text{TAP}}{\text{mRNA}_\text{Lu}}$', '$\frac{\text{Protein}_\text{Lu}}{\text{mRNA}_\text{Arava}}$', '$\frac{\text{Protein}_\text{Lu}}{\text{mRNA}_\text{count}}$', '$\frac{\text{Protein}_\text{Lu}}{\text{mRNA}_\text{seq}}$', '$\frac{\text{Protein}_\text{Lu}}{\text{mRNA}_\text{Ingolia}}$', '$\frac{\text{Protein}_\text{Lu}}{\text{mRNA}_\text{Lu}}$', '$\text{mRNA}_\text{Arava}\cdot{}D_\text{Arava}$', '$\text{mRNA}_\text{Arava}\cdot{}\text{Rate}_\text{Arava}$', '$\text{mRNA}_\text{Arava}\cdot{}D_\text{Ingolia}$', '$\text{mRNA}_\text{Arava}\cdot{}D^\text{corr}_\text{Ingolia}$', '$\text{mRNA}_\text{Ingolia}\cdot{}D_\text{Arava}$', '$\text{mRNA}_\text{Ing}\cdot{}\text{Rate}_\text{Arava}$', '$\text{mRNA}_\text{Ingolia}\cdot{}D_\text{Ingolia}$', '$\text{mRNA}_\text{Ingolia}\cdot{}D^\text{corr}_\text{Ingolia}$', '$\text{mRNA}_\text{count}\cdot{}D_\text{Arava}$', '$\text{mRNA}_\text{count}\cdot{}\text{Rate}_\text{Arava}$', '$\text{mRNA}_\text{count}\cdot{}D_\text{Ingolia}$', '$\text{mRNA}_\text{count}\cdot{}D^\text{corr}_\text{Ingolia}$', '$\text{mRNA}_\text{seq}\cdot{}D_\text{Arava}$', '$\text{mRNA}_\text{seq}\cdot\text{Rate}_\text{Arava}$', '$\text{mRNA}_\text{seq}\cdot{}D_\text{Ingolia}$', '$\text{mRNA}_\text{seq}\cdot{}D^\text{corr}_\text{Ingolia}$', '$\text{mRNA}_\text{Lu}\cdot{}D_\text{Arava}$', '$\text{mRNA}_\text{Lu}\cdot\text{Rate}_\text{Arava}$', '$\text{mRNA}_\text{Lu}\cdot{}D_\text{Ingolia}$', '$\text{mRNA}_\text{Lu}\cdot{}D^\text{corr}_\text{Ingolia}$', '$E_\text{Ingolia}$', '$E^\text{corr}_\text{Ingolia}$' };
%     
%     fprintf('\\begin{tabular}{l ');
%     for i = 1:length(names)
%         fprintf('r');
%     end
%     fprintf('}\n');
% 
%     fprintf('\\toprule\n');
%     fprintf('Name & ');
%     for i = 1:n
%         fprintf('\\begin{sideways}%s\\end{sideways} ', names{i});
%         if i == n
%             fprintf('\\tabularnewline');
%         else
%             fprintf('& ');
%         end
%     end
%     fprintf('\n');
%     
%     for i = 1:n
%         fprintf('%s', names{i});
%         for j = 1:i
%             cur_corr = correlations(i, j);
%             if abs(cur_corr) >= 0.6
%                 fprintf(' & \\textcolor{NavyBlue}{$%.2f$}', cur_corr);
%             elseif abs(cur_corr) <= 0.2
%                 fprintf(' & \\textcolor{red}{$%.2f$}', cur_corr);
%             else
%                 fprintf(' & $%.2f$', cur_corr);
%             end
%             if i <= 8 && j <= 8
%                 fprintf('\\cellcolor[gray]{0.95}');
%             elseif i <= 12 && j >= 9
%                 fprintf('\\cellcolor[gray]{0.90}');
%             elseif i <= 16 && j >= 13
%                 fprintf('\\cellcolor[gray]{0.85}');
%             elseif i <= 19 && j >= 17
%                 fprintf('\\cellcolor[gray]{0.80}');
%             elseif i <= 34 && j >= 20
%                 fprintf('\\cellcolor[gray]{0.75}');
%             elseif i <= 54 && j >= 35
%                 fprintf('\\cellcolor[gray]{0.75}');
%             elseif i <= 56 && j >= 55
%                 fprintf('\\cellcolor[gray]{0.70}');
%             end
%         end
%         for j = i + 1:n - 1
%             fprintf(' &');
%         end
%         fprintf(' \\tabularnewline\n');
%     end
%     fprintf('\\bottomrule\n\\end{tabular}\n');
% end


% function [correlations] = work(set)
%     n = length(set);
%     for i = 1:n
%         if isempty(set(i).mRNA_lim_mean)
%             set(i).mRNA_lim_mean = NaN;
%         end
%         if isempty(set(i).mRNA_lim_min)
%             set(i).mRNA_lim_min = NaN;
%         end
%         if isempty(set(i).mRNA_lim_max)
%             set(i).mRNA_lim_max = NaN;
%         end
%         if isempty(set(i).mRNA_Arava)
%             set(i).mRNA_Arava = NaN;
%         end
%         if isempty(set(i).mRNA_Lu)
%             set(i).mRNA_Lu = NaN;
%         end
%         if isempty(set(i).density_Arava)
%             set(i).density_Arava = NaN;
%         end
%         if isempty(set(i).relative_rate_Arava)
%             set(i).relative_rate_Arava = NaN;
%         end
%         if isempty(set(i).gfp)
%             set(i).gfp = NaN;
%         end
%         if isempty(set(i).tap)
%             set(i).tap = NaN;
%         end
%         if isempty(set(i).protein_Lu)
%             set(i).protein_Lu = NaN;
%         end
%         if isempty(set(i).mRNA_count)
%             set(i).mRNA_count = NaN;
%         end
%         if isempty(set(i).mRNA_seq)
%             set(i).mRNA_seq = NaN;
%         end
%         if isempty(set(i).mRNA_Ingolia)
%             set(i).mRNA_Ingolia = NaN;
%         end
%         if isempty(set(i).expected_protein_Ingolia)
%             set(i).expected_protein_Ingolia = NaN;
%         end
%         if isempty(set(i).expected_protein_Ingolia_corrected)
%             set(i).expected_protein_Ingolia_corrected = NaN;
%         end
%         if isempty(set(i).density_Ingolia)
%             set(i).density_Ingolia = NaN;
%         end
%         if isempty(set(i).density_Ingolia_corrected)
%             set(i).density_Ingolia_corrected = NaN;
%         end
%     end
%     
%     mRNA_lim_mean = [set.mRNA_lim_mean]';
%     mRNA_lim_min = [set.mRNA_lim_min]';
%     mRNA_lim_max = [set.mRNA_lim_max]';
%     
%     mRNA_Arava = [set.mRNA_Arava]';
%     mRNA_count = [set.mRNA_count]';
%     mRNA_seq = [set.mRNA_seq]';
%     mRNA_Ingolia = [set.mRNA_Ingolia]';
%     mRNA_Lu = [set.mRNA_Lu]';
%     
%     d_Arava = [set.density_Arava]';
%     dinv_Arava = d_Arava .^ -1; dinv_Arava(isinf(dinv_Arava)) = NaN;
%     
%     r_Arava = [set.relative_rate_Arava]';
%     rinv_Arava = r_Arava .^ -1; rinv_Arava(isinf(rinv_Arava)) = NaN;
%     
%     d_Ingolia = [set.density_Ingolia]';
%     dinv_Ingolia = d_Ingolia .^ -1; dinv_Ingolia(isinf(dinv_Ingolia)) = NaN;
%     
%     d_Ingolia_corrected = [set.density_Ingolia_corrected]';
%     dinv_Ingolia_corrected = d_Ingolia_corrected .^ -1; dinv_Ingolia_corrected(isinf(dinv_Ingolia_corrected)) = NaN;
%     
%     gfp = [set.gfp]';
%     tap = [set.tap]';
%     protein_Lu = [set.protein_Lu]';
%     
%     gfp_Arava = gfp ./ mRNA_Arava; gfp_Arava(isinf(gfp_Arava)) = NaN;
%     gfp_count = gfp ./ mRNA_count; gfp_count(isinf(gfp_count)) = NaN;
%     gfp_seq = gfp ./ mRNA_seq; gfp_seq(isinf(gfp_seq)) = NaN;
%     gfp_Ingolia = gfp ./ mRNA_Ingolia; gfp_Ingolia(isinf(gfp_Ingolia)) = NaN;
%     gfp_Lu = gfp ./ mRNA_Lu; gfp_Lu(isinf(gfp_Lu)) = NaN;
%     
%     tap_Arava = tap ./ mRNA_Arava; tap_Arava(isinf(tap_Arava)) = NaN;
%     tap_count = tap ./ mRNA_count; tap_count(isinf(tap_count)) = NaN;
%     tap_seq = tap ./ mRNA_seq; tap_seq(isinf(tap_seq)) = NaN;
%     tap_Ingolia = tap ./ mRNA_Ingolia; tap_Ingolia(isinf(tap_Ingolia)) = NaN;
%     tap_Lu = tap ./ mRNA_Lu; tap_Lu(isinf(tap_Lu)) = NaN;
%     
%     protein_Lu_Arava = protein_Lu ./ mRNA_Arava; protein_Lu_Arava(isinf(protein_Lu_Arava)) = NaN;
%     protein_Lu_count = protein_Lu ./ mRNA_count; protein_Lu_count(isinf(protein_Lu_count)) = NaN;
%     protein_Lu_seq = protein_Lu ./ mRNA_seq; protein_Lu_seq(isinf(protein_Lu_seq)) = NaN;
%     protein_Lu_Ingolia = protein_Lu ./ mRNA_Ingolia; protein_Lu_Ingolia(isinf(protein_Lu_Ingolia)) = NaN;
%     protein_Lu_Lu = protein_Lu ./ mRNA_Lu; protein_Lu_Lu(isinf(protein_Lu_Lu)) = NaN;
%     
%     Arava_d_Arava = mRNA_Arava .* d_Arava;
%     Arava_r_Arava = mRNA_Arava .* r_Arava;
%     Arava_d_Ingolia = mRNA_Arava .* d_Ingolia;
%     Arava_d_Ingolia_corrected = mRNA_Arava .* d_Ingolia_corrected;
%     
%     Ingolia_d_Arava = mRNA_Ingolia .* d_Arava;
%     Ingolia_r_Arava = mRNA_Ingolia .* r_Arava;
%     Ingolia_d_Ingolia = mRNA_Ingolia .* d_Ingolia;
%     Ingolia_d_Ingolia_corrected = mRNA_Ingolia .* d_Ingolia_corrected;
%     
%     count_d_Arava = mRNA_count .* d_Arava;
%     count_r_Arava = mRNA_count .* r_Arava;
%     count_d_Ingolia = mRNA_count .* d_Ingolia;
%     count_d_Ingolia_corrected = mRNA_count .* d_Ingolia_corrected;
%     
%     seq_d_Arava = mRNA_seq .* d_Arava;
%     seq_r_Arava = mRNA_seq .* r_Arava;
%     seq_d_Ingolia = mRNA_seq .* d_Ingolia;
%     seq_d_Ingolia_corrected = mRNA_seq .* d_Ingolia_corrected;
%     
%     Lu_d_Arava = mRNA_Lu .* d_Arava;
%     Lu_r_Arava = mRNA_Lu .* r_Arava;
%     Lu_d_Ingolia = mRNA_Lu .* d_Ingolia;
%     Lu_d_Ingolia_corrected = mRNA_Lu .* d_Ingolia_corrected;
%     
%     p_Ingolia = [set.expected_protein_Ingolia]';
%     p_Ingolia_corrected = [set.expected_protein_Ingolia_corrected]';
%        
%     data = [mRNA_lim_mean, mRNA_lim_min, mRNA_lim_max, mRNA_Arava, mRNA_Ingolia, mRNA_count, mRNA_seq, mRNA_Lu, d_Arava, r_Arava, d_Ingolia, d_Ingolia_corrected, dinv_Arava, rinv_Arava, dinv_Ingolia, dinv_Ingolia_corrected, gfp, tap, protein_Lu, gfp_Arava, gfp_count, gfp_seq, gfp_Ingolia, gfp_Lu, tap_Arava, tap_count, tap_seq, tap_Ingolia, tap_Lu, protein_Lu_Arava, protein_Lu_count, protein_Lu_seq, protein_Lu_Ingolia, protein_Lu_Lu, Arava_d_Arava, Arava_r_Arava, Arava_d_Ingolia, Arava_d_Ingolia_corrected, Ingolia_d_Arava, Ingolia_r_Arava, Ingolia_d_Ingolia, Ingolia_d_Ingolia_corrected, count_d_Arava, count_r_Arava, count_d_Ingolia, count_d_Ingolia_corrected, seq_d_Arava, seq_r_Arava, seq_d_Ingolia, seq_d_Ingolia_corrected, Lu_d_Arava, Lu_r_Arava, Lu_d_Ingolia, Lu_d_Ingolia_corrected, p_Ingolia, p_Ingolia_corrected];
%     correlations = corr(data, data, 'rows', 'pairwise');
% end

% function [] = work(names, data)
%     print_names = { '$\text{GC}$', '$\text{GC15}$', '$\text{GC3}$', '$\text{len}$', '$N_c$', '$E_w$', '$\text{SCUO}$', '$P_1$', '$P_2$', '$\text{RCBS}_\text{PC}$', '$\text{CAI}$', '$\text{tAI}$', '$\text{RCA}$', '$\text{P}$', '$\text{CPB}$', '$\text{TPI}$', '$\text{GRAVY}$', '$\text{AROMA}$', '$\text{Aliphatic}$', '$\text{Instability}$', '$\text{Side}$', '$\text{tAI}_{14}$', '$\text{tAI}_{17}$', '$\text{tAI}_{19}$', '$\text{Side}_{4}$', '$\text{Side}_{11}$', '$\text{Side}_{15}$', '$\text{Side}_{40}$', '$\text{CAI}_{2^\text{nd}}$', '$\text{tAI}_{2^\text{nd}}$', '$\Delta{}G_\text{CDS}$', '$\Delta{}G_{\text{CDS}_{17}}$', '$\Delta{}G_{\text{CDS}_{34}}$', '$\Delta{}G_{\text{CDS}_{53}}$', 'pI', 'MolWeight', 'Charge', '$\text{len}_\text{3UTR}$', '$\text{len}_\text{5UTR}$', '$\Delta{}G_\text{3UTR}$', '$\Delta{}G_\text{5UTR}$', '$\Delta{}G_{\text{5UTR}_{50}}$', '$\Delta{}G_{\text{5UTR}_{50}\text{,}\text{CDS}_{17}}$', '$\Delta{}G_{\text{5UTR}_{50}\text{,}\text{CDS}_{34}}$', '$\Delta{}G_{\text{5UTR}_{50}\text{,}\text{CDS}_{53}}$', '$\Delta{}G_{\text{5UTR,CDS}_{17}}$', '$\Delta{}G_{\text{5UTR,CDS}_{34}}$', '$\Delta{}G_{\text{5UTR,CDS}_{53}}$', '$\Delta{}G_{\text{full}}$', 'Nuc', 'Amino', 'RSCU' };
% 
%     fprintf('\\begin{tabular}{l ');
%     for i = 1:length(names)
%         fprintf('r');
%     end
%     fprintf('}\n');
%     
%     fprintf('\\toprule\n');
%     
%     fprintf('Pred');
%     for i = 1:length(names)
%         fprintf(' & %s', names{i});
%     end
%     fprintf(' \\tabularnewline\n');
%     
%     fprintf('\\midrule\n');
%     
%     if length(names) > 1
%         ref = data{1};
%         n = length(ref);
%         for i = 1:n
%             if isempty(strfind(print_names{i}, 'Delta'))
%                 continue;
%             end
%             
%             if ~isempty(ref(i).r)
%                 %fprintf('\\multirow{2}{*}{%s}', ref(i).feature);
%                 fprintf('\\multirow{2}{*}{%s}', print_names{i});
%                 for j = 1:length(names)
%                     cur = data{j};
%                     if  cur(i).p_value < 1e-3
%                         fprintf(' & $r=%.3f,p=%.3f$', cur(i).r, cur(i).p_value);
%                     else
%                         fprintf(' & $r=%.3f,\\;$\\textcolor{red}{$p=%.3f$}', cur(i).r, cur(i).p_value);
%                     end
%                 end
%                 fprintf(' \\tabularnewline\n');
%             else
%                 % fprintf('%s', ref(i).feature);
%                 fprintf('%s', print_names{i});
%             end
%             
%             for j = 1:length(names)
%                 cur = data{j};
%                 if cur(i).mu >= 1e-1
%                     fprintf(' & \\textcolor{NavyBlue}{$R^2=%.3f\\pm%.3f$}', cur(i).mu, cur(i).sigma);
%                 else
%                     fprintf(' & $R^2=%.3f\\pm%.3f$', cur(i).mu, cur(i).sigma);
%                 end
%             end
%             fprintf(' \\tabularnewline\n')
%         end
%     end
%     
%     fprintf('\\bottomrule\n');
%     fprintf('\\end{tabular}\n');
%     fprintf('\\end{table*}\n');
% end

% function [names ds data] = work(feat, utr3, utr5)
%     [names ds] = prepare_datasets(feat, utr3, utr5);
%     data = cell(1, length(names));
%     for i = 1:length(names)
%         data{i} = compute_correlations(ds{i});
%     end
% end


% {'mRNA-lim-mean', 'mRNA-lim-max', 'mRNA-lim-min', 'mRNA-Arava'}
% {'mRNA-count', 'mRNA-seq', 'Density-Arava', 'Relative-rate-Arava'}
% {'GFP', 'TAP', 'mRNA-Arava*Density-Arava', 'mRNA-Arava*Relative-rate-Arava'}
% {'mRNA-count*Density-Arava', 'mRNA-count*Relative-rate-Arava', 'mRNA-seq*Density-Arava', 'mRNA-seq*Relative-rate-Arava'}

%function [adj] = work(i, j)
%    k = [i' j'];
%    k_unique = unique(k);
%    n = length(k_unique);
%    cnt = hist(k, k_unique);
%    sorted = sortrows([cnt; k_unique; 1:n]', 1);
%    
%    %sorted
%    
%    bar(sorted(:, 1));
%    axis([1 n 0 max(cnt) + 10]);
%    
%    ind = sorted(:, 2);
%    rev_ind = zeros(1, max(ind));
%    for p = 1:n
%        rev_ind(ind(p)) = p;
%    end
%    
%    adj = zeros(n);
%    for p = 1:length(i)
%        adj(rev_ind(i(p)), rev_ind(j(p))) = 1;
%    end
%    HeatMap(adj);
%end

%function [ds2] = work(ds, num_bins)
%    mRNA = double(ds.mRNA);
%    [mRNA, ind] = sort(mRNA);
%    
%    ds = ds(ind, :);
%    n = size(ds, 1);
%    m = size(ds, 2);
%    per_bin = round(n / num_bins);
%    ds_double = double(ds);
%    ds2_double = zeros(num_bins, m);
%    for i = 1:num_bins
%        start = (i - 1) * per_bin + 1;
%        stop = i * per_bin;
%        if i == num_bins
%            stop = n;
%        end
%        ds2_double(i, :) = mean(ds_double(start:stop, :));
%    end
%    
%    ds2 = dataset();
%    for i = 1:m
%        ds2 = horzcat(ds2, dataset(ds2_double(:, i), 'VarNames', ds.Properties.VarNames{i}));
%    end
%end

%function [mu sigma errors] = work(ds, sim, thr)
%    [ind_i ind_j ds_pair] = make_paired_dataset(ds, sim, thr);
%    n = size(ds_pair, 1);
%    fprintf('Pairs for learning: %d\n', n);
%    [mu sigma errors] = regression_cv(ds_pair, 'r2', folds, 'normal', 'identity', 'linear', [], true);
%    %[mu sigma errors] = regression_cv(ds_pair, 'r2');
%end

%function [cur, cur_r2, ds] = work(ds, n_feat)
%    n = size(ds, 2) - 1;
%    
%    if nargin < 2
%        n_feat = n;
%    end
%    
%    for i = 1:n + 1
%        vals = double(ds(:, i));
%        ma = max(vals);
%        mi = min(vals);
%        if ma ~= mi
%            vals = (vals - mi) / (ma - mi);
%        else
%            vals(:) = 0;
%        end
%        ds(:, i) = dataset(vals, 'VarNames', ds.Properties.VarNames{i});
%    end
%    
%    allowed = true(1, n);
%    cur = [];
%    cur_r2 = [];
%    for j = 1:n_feat
%        r2 = -ones(1, n);
%        parfor i = 1:n
%            warning off stats:cvpartition:KFoldMissingGrp
%            warning off stats:cvpartition:TestZero
%            if allowed(i)
%                r2(i) = get_error(ds(:, [cur i n + 1]));
%                fprintf('  Feature %s : %f\n', ds.Properties.VarNames{i}, r2(i));
%%                if curB <= 0 || curR < r2
%%                    curR = r2;
%%                    curB = i;
%                %end
%            end
%        end
%        [r2m, curB] = max(r2);
%        if r2m == -1
%            break
%        end
%        allowed(curB) = false;
%        cur = [cur curB];
%        cur_r2 = [cur_r2 r2m];
%        fprintf('%d : Selected %s, R2 = %f\n', j, ds.Properties.VarNames{curB}, r2m);
%    end
%end

%function [mu sigma error] = get_error(ds)
%    [mu sigma error] = nw_regression_cv(ds, 'r2');
%end

%function [mu sigma errors] = work(ds, thr, link)
%    [feat correlation aggr ds] = sort_features(ds, 2);
%    features = find(abs(correlation) >= thr)';
%    n = size(ds, 2);
%    [mu, sigma, errors] = regression_cv(ds(:, [features n]), 'r2', folds, 'normal', link, 'linear', [], false);
%end