function [mu sigma] = compute_bins_snr(bins)
    n = size(bins, 2);
    for i = 1:n
        bin = bins{i};
        bins{i} = double(bin(:, end));
    end
    
    vals = [];
    for i = 1:n
        if size(bins{i}, 1) > 1
            vals = [vals mean(bins{i}) / std(bins{i})];
        end
    end
    
    if isempty(vals)
        mu = [];
        sigma = [];
        return;
    end
    
    mu = mean(vals);
    sigma = std(vals);
end