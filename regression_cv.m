function [mu, sigma, errors] = regression_cv(ds, dist, K, distr, link, upper, weights, stepwise, visualize)
    if nargin < 2
        dist = 'mse';
    end
    if nargin < 3
        K = 10;
    end
    if nargin < 4
        distr = 'normal';
    end
    if nargin < 5
        link = 'identity';
    end
    if nargin < 6
        upper = 'quadratic';
    end
    if nargin < 7
        weights = [];
    end
    if nargin < 8
        stepwise = true;
    end
    if nargin < 9
        visualize = false;
    end
    
    warning('off', 'stats:cvpartition:KFoldMissingGrp');
    warning('off', 'stats:LinearModel:RankDefDesignMat');
    
    %fprintf('DS %d x %d, folds: %d\n', size(ds, 1), size(ds, 2), K);
    
    y = double(ds(:, end));
    if K == size(ds, 1)
        partition = cvpartition(y, 'Leaveout');
    else
        partition = cvpartition(y, 'Kfold', K);
    end
    errors = zeros(1, K);
    model = cell(1, K);
    dsTrain = cell(1, K);
    dsTest = cell(1, K);
    
    parfor i = 1:K
        dsTrain{i} = ds(partition.training(i), :);
        dsTest{i} = ds(partition.test(i), :);
    end
    
    parfor i = 1:K
        model{i} = linear_regression(dsTrain{i}, distr, link, upper, weights, stepwise);
        %model = NWModel.fit(dsTrain);
        errors(i) = evaluate_model(model{i}, dsTest{i}, dist);
        if visualize
            figure;
            scatter(dsTrain{i}.CAI, dsTrain{i}.mRNA, 40, 'filled', 'b');
            hold on;
            scatter(dsTest{i}.CAI, dsTest{i}.mRNA, 40, 'filled', 'r');
            pred = model{i}.predict(ds);
            [~, ind] = sort(ds.CAI);
            plot(ds.CAI(ind), pred(ind), 'g--', 'LineWidth', 1);
            axis([0 1 0 max(ds.mRNA)]);
            hold off;
        end
        %fprintf('%f - %f\n', evaluate_model(model, dsTrain, 'R2'), model.Rsquared.Ordinary);
    end
    mu = mean(errors);
    sigma = std(errors);
end