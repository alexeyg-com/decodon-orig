% Compute GC3 bias for a given item
%   SEQ  - input sequence
%   GC3  - computed bias
function [gc3] = compute_gc3(seq)
    strong = 0;
    total = 0;
    n = length(seq);
    for i = 1:3:n
        total = total + 1;
        nuc = seq(i + 2);
        if nuc == 'G' || nuc == 'C'
            strong = strong + 1;
        end
    end
    gc3 = strong / total;
end