% Returns a list of all nucleotides (including the unknown nucleotide)
%   NUC - returned cell array
function [nuc] = get_nucs
    nuc = {'A', 'C', 'T', 'G', '-'};
end