% Evaluate model on a dataset
%   MODEL - model
%   DS    - dataset
%   DIST  - distance type
%
%   ERROR - computed error
function [error] = evaluate_model(model, ds, dist)
    if nargin < 3
        dist = 'mse';
    end
    if isa(model, 'struct')
        y = double(ds(:, end));
        X = double(ds(:, 1:end - 1));
        predicted = svmpredict(y, X, model);
    else
        predicted = model.predict(ds);
    end
    %scatter(double(ds(:, size(ds, 2))), predicted);
    error = vector_distance(double(ds(:, end)), predicted, dist);
end