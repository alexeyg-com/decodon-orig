function [cur, cur_r2, ds] = nw_select_features(ds, n_feat, width)
    n = size(ds, 2) - 1;
    
    if nargin < 2
        n_feat = n;
    end
    
    if nargin < 3
        width = [];
    end
    
    for i = 1:n + 1
        vals = double(ds(:, i));
        ma = max(vals);
        mi = min(vals);
        if ma ~= mi
            vals = (vals - mi) / (ma - mi);
        else
            vals(:) = 0;
        end
        ds(:, i) = dataset(vals, 'VarNames', ds.Properties.VarNames{i});
    end
    
    allowed = true(1, n);
    cur = [];
    cur_r2 = [];
    for j = 1:n_feat
        r2 = -ones(1, n);
        parfor i = 1:n
            warning off stats:cvpartition:KFoldMissingGrp
            warning off stats:cvpartition:TestZero
            if allowed(i)
                if isempty(width)
                    r2(i) = get_error(ds(:, [cur i n + 1]));
                else
                    r2(i) = get_error(ds(:, [cur i n + 1]), width([cur i]));
                end
                fprintf('  Feature %s : %f\n', ds.Properties.VarNames{i}, r2(i));
            end
        end
        [r2m, curB] = max(r2);
        if r2m == -1
            break
        end
        allowed(curB) = false;
        cur = [cur curB];
        cur_r2 = [cur_r2 r2m];
        fprintf('%d : Selected %s, R2 = %f\n', j, ds.Properties.VarNames{curB}, r2m);
    end
end

function [mu sigma error] = get_error(ds, w)
    [mu sigma error] = nw_regression_cv(ds, 'r2', 10, w);
end

%function [mu sigma errors] = work(ds, thr, link)
%    [feat correlation aggr ds] = sort_features(ds, 2);
%    features = find(abs(correlation) >= thr)';
%    n = size(ds, 2);
%    [mu, sigma, errors] = regression_cv(ds(:, [features n]), 'r2', 10, 'normal', link, 'linear', [], false);
%end