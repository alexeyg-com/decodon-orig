function [vars counts] = bootstrap_models2stats(model_set)
    n_models = length(model_set);
    vars = {};
    counts = [];
    for i = 1:n_models
        model = model_set{i};
        model_vars = model.PredictorNames;
        n_vars = length(model_vars);
        for j = 1:n_vars
            ind = find(strcmp(model_vars{j}, vars));
            if isempty(ind)
                vars = [vars, model_vars{j}];
                counts = [counts 1];
            else
                counts(ind) = counts(ind) + 1;
            end
        end
    end
end