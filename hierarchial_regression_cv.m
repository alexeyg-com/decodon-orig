function [mu sigma] = hierarchial_regression_cv(ds, n_clusters, log_binning, dist, n_folds, distr, link, upper, weights, stepwise)    
    warning('off', 'stats:cvpartition:KFoldMissingGrp');
    warning('off', 'stats:LinearModel:RankDefDesignMat');
    
    y = double(ds(:, end));
    if n_folds == size(ds, 1)
        partition = cvpartition(y, 'Leaveout');
    else
        partition = cvpartition(y, 'Kfold', n_folds);
    end
    errors = zeros(1, n_folds);
    model = cell(1, n_folds);
    dsTrain = cell(1, n_folds);
    dsTrain_clust = cell(1, n_folds);
    dsTest = cell(1, n_folds);
    dsTest_clust = cell(1, n_folds);
    
    parfor i = 1:n_folds
        dsTrain{i} = ds(partition.training(i), :);
        train_size = size(dsTrain{i}, 1);
        bins = group_by_value(dsTrain{i}, n_clusters, 'hierarchial', 'complete');
        dsTrain_clust{i} = summarize_bins(bins);
        
        dsTest{i} = ds(partition.test(i), :);
        test_size = size(dsTest{i}, 1);
        n_test_clusters = round(n_clusters * test_size / train_size);
        n_test_clusters = min(n_test_clusters, test_size);
        if ~log_binning
            test_bins = group_by_value(dsTest{i}, n_test_clusters, 'hierarchial', 'complete');
        else
            test_bins = group_equal_width_log(dsTest{i}, n_test_clusters);
        end
        %test_bins = group_equal_width(dsTest{i}, n_test_clusters);
        %dsTest_clust{i} = summarize_bins(test_bins);
        dsTest_clust{i} = select_from_bins(test_bins, 1);
    end
    
    parfor i = 1:n_folds
        model{i} = linear_regression(dsTrain_clust{i}, distr, link, upper, weights, stepwise);
        errors(i) = evaluate_model(model{i}, dsTest_clust{i}, dist);
    end
    %errors
    mu = mean(errors);
    sigma = std(errors);
end
