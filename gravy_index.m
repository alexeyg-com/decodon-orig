function [gravy] = gravy_index(seq)
    hydrophobicity = [1.8 -4.5 -3.5 -3.5 2.5 -3.5 -3.5 -0.4 -3.2 4.5 3.8 -3.9 1.9 2.8 -1.6 -0.8 -0.7 -0.9 -1.3 4.2 0 0];
    seq = codon2amino_int(seq);
    
    gravy = 0;
    for i = 1:length(seq) - 1
        gravy = gravy + hydrophobicity(seq(i));
    end
    
    gravy = gravy / (length(seq) - 1);
end