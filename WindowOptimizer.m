classdef WindowOptimizer
    properties
        MaxIterations
        WindowSize
        BestFitness
        Predictor
    end
    
    properties (Hidden)
        combo_seq
        seq_length
        iteration
        
        amino2codons
    end
    
    methods
        function [obj] = WindowOptimizer(pred, window_size, max_iterations)
            if nargin < 3
                max_iterations = -1;
            end
            
            obj.Predictor = pred;
            obj.WindowSize = window_size;
            obj.MaxIterations = max_iterations;
            
            obj.amino2codons = amino2codons_int;
        end
        
        function [combo_seq, optimized_fitness, obj] = optimize(obj, combo_seq)
            obj.combo_seq = combo_seq;
            obj.seq_length = length(combo_seq.seq) / 3;
            if obj.WindowSize > obj.seq_length
                obj.seq_length = obj.WindowSize;
            end
            
            % Prepare cache
            [~, combo_seq] = obj.Predictor.Computer.compute_features(combo_seq);
            obj.BestFitness = obj.Predictor.predict_value(combo_seq);
            
            amino2codons = obj.amino2codons;
            
            obj.iteration = 0;
            improved = true;
            while improved
                improved = false;
                obj.iteration = obj.iteration + 1;
                if obj.iteration > obj.MaxIterations && obj.MaxIterations >= 0
                    break;
                end
                
                ws = obj.WindowSize;
                pred = obj.Predictor;
                for offset = 1:obj.seq_length
                %for offset = obj.seq_length - 1:obj.seq_length
                    sub_sequence = obj.get_subsequence(offset);
                    sub_aminos = codon2amino_int(sub_sequence);
                    num_combos = obj.get_combination_count(sub_aminos);
                    if offset + obj.WindowSize - 1 <= obj.seq_length
                        offset_full = true;
                        overhang = 0;
                    else
                        offset_full = false;
                        overhang = offset + obj.WindowSize - 1 - obj.seq_length;
                    end
                    
                    fprintf('   [i] Iteration %d, offset %d: %d combinations\n', obj.iteration, offset, num_combos);
                    
                    fitness = zeros(1, num_combos);
                    parfor j = 1:num_combos
                        [index_seq, success] = WindowOptimizer.decode_sequence(j, sub_aminos);
                        if ~success
                            continue;
                        end
                        for k = 1:ws
                            index_seq(k) = amino2codons{sub_aminos(k)}(index_seq(k));
                        end
                        evaluate_seq = combo_seq;
                        if offset_full
                            evaluate_seq.seq(offset * 3 - 2 : (offset + ws - 1) * 3) = int2codon(index_seq);
                        else                            
                            evaluate_seq.seq(offset * 3 - 2 : end) = int2codon(index_seq(1:end - overhang));
                            evaluate_seq.seq(1 : overhang * 3) = int2codon(index_seq(end - overhang + 1 : end));
                        end
                        
                        fitness(j) = pred.predict_value(evaluate_seq);
                    end
                    
                    [max_fitness, fitness_combo] = max(fitness);
                    if max_fitness > obj.BestFitness
                        improved = true;
                        obj.BestFitness = max_fitness;
                        fprintf('[+] Best fitness: %.15f (iteration %d, offset %d, combo %d)\n', max_fitness, obj.iteration, offset, fitness_combo);
                        index_seq = WindowOptimizer.decode_sequence(fitness_combo, sub_aminos);
                        for k = 1:ws
                            index_seq(k) = amino2codons{sub_aminos(k)}(index_seq(k));
                        end
                        if offset_full
                            combo_seq.seq(offset * 3 - 2 : (offset + ws - 1) * 3) = int2codon(index_seq);
                        else
                            combo_seq.seq(offset * 3 - 2 : end) = int2codon(index_seq(1:end - overhang));
                            combo_seq.seq(1 : overhang * 3) = int2codon(index_seq(end - overhang + 1 : end));
                        end
                    end
                end
            end
            optimized_fitness = obj.BestFitness;
            if ~improved
                fprintf('[+] Converged at %.15f\n', optimized_fitness);
            end
        end
    end
    
    methods (Access = private)
        function [seq] = get_subsequence(obj, offset)
            if offset + obj.WindowSize - 1 <= obj.seq_length
                seq = obj.combo_seq.seq(offset * 3 - 2 : (offset + obj.WindowSize - 1) * 3);
            else
                overhang = offset + obj.WindowSize - 1 - obj.seq_length;
                seq = obj.combo_seq.seq(offset * 3 - 2 : end);
                seq = [seq obj.combo_seq.seq(1 : overhang * 3)];
            end
        end
        
        function [count] = get_combination_count(obj, seq)
            count = 1;
            for i = 1:length(seq)
                switch length(obj.amino2codons{seq(i)})
                    case 3
                        count = count * 4;
                    case 4
                        count = count * 4;
                    case 6
                        count = count * 8;
                end
            end
        end
    end
    
    methods (Static)
        function [combo_seq, success] = decode_sequence(combo, aminos)
            success = true;
            combo_seq = zeros(1, length(aminos));
            for i = 1:length(aminos)
                switch aminos(i)
                    case 1
                        bits = 2;
                        bound = 4;
                    case 2
                        bits = 3;
                        bound = 6;
                    case 3
                        bits = 1;
                        bound = 2;
                    case 4
                        bits = 1;
                        bound = 2;
                    case 5
                        bits = 1;
                        bound = 2;
                    case 6
                        bits = 1;
                        bound = 2;
                    case 7
                        bits = 1;
                        bound = 2;
                    case 8
                        bits = 2;
                        bound = 4;
                    case 9
                        bits = 1;
                        bound = 2;
                    case 10
                        bits = 3;
                        bound = 3;
                    case 11
                        bits = 3;
                        bound = 6;
                    case 12
                        bits = 1;
                        bound = 2;
                    case 13
                        bits = 0;
                        bound = 1;
                    case 14
                        bits = 1;
                        bound = 2;
                    case 15
                        bits = 2;
                        bound = 4;
                    case 16
                        bits = 3;
                        bound = 6;
                    case 17
                        bits = 2;
                        bound = 4;
                    case 18
                        bits = 0;
                        bound = 1;
                    case 19
                        bits = 1;
                        bound = 2;
                    case 20
                        bits = 2;
                        bound = 4;
                    case 21
                        bits = 3;
                        bound = 3;
                    case 22
                        bits = 0;
                        bound = 1;
                    otherwise
                        fprintf('No case for %d\n', aminos(i));
                end
                
                modulo = 2 ^ bits;
                num = mod(combo, modulo) + 1;
                combo = int32(combo / modulo);
                combo_seq(i) = num;
                
                if num > bound
                    success = false;
                    break;
                end
            end
        end
    end
end
