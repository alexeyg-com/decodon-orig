% Convert codons to amino acid integer representation
%   CODON     - codon representation
%
%   AMINO_INT - amino acid sequence
function [amino_int] = codon2amino_int(codon)
    % we have a string sequence
    if ischar(codon)
        codonInt = codon2int(codon);
    elseif isnumeric(codon)
        codonInt = codon;
    end
    
    n = length(codonInt);
    if n == 1
        conversion = [8     8     8     8    20    20    20    20     1     1     1     1     7     4     4 7    18     5     5    21    11    14    14    11    16    16    16    16    21    19 19    21     2     2     2     2    11    11    11    11    15    15    15    15     6 9     9     6     2    16    16     2    13    10    10    10    17    17    17    17 12     3     3    12    22];
        if codonInt > length(get_codons)
            amino_int = length(get_aminos);
        else
            amino_int = conversion(codonInt);
        end
    else
        amino_int = zeros(1, n);
        for i = 1:n
            amino_int(i) = codon2amino_int(codonInt(i));
        end
    end
end