% Compute relative synonymous codon usage (RSCU) for a given sequence
%   SEQ   - input sequence
%   USAGE - computed codon usage
function [usage] = compute_rscu(seq)
    arrayLen = length(get_codons);
    codons = codon2int(seq);
    
    usage = zeros(1, arrayLen);
    for i = 1:length(codons)
        usage(codons(i)) = usage(codons(i)) + 1;
    end
    
    aminoAcids = amino2codons;
    for i = 1:length(aminoAcids)
        codonsForAcid = aminoAcids{i};
        nCodons = length(codonsForAcid);
        ints = zeros(1, nCodons);
        for j = 1:nCodons
            ints(j) = codon2int(codonsForAcid{j});
        end
        usage(ints) = usage(ints) ./ max(mean(usage(ints)), 1);
        %usage(ints) = usage(ints) ./ mean(usage(ints));
    end
end