% Convert amino acid (string) into its integer representation
%   AMINOACID    -  input string with amino acids
%
%   AMINOACIDINT - the integer representation
function [aminoAcidInt] = amino2int(aminoAcid)    
    aminoAcids = get_aminos;
    len = length(aminoAcid);
    aminoAcidInt = zeros(1, len);
    for i = 1:len
        acid = aminoAcid(i);
        found = find(strcmpi(acid, aminoAcids));
        if ~isempty(found) && length(found) == 1
            aminoAcidInt(i) = found;
        else
            aminoAcidInt(i) = length(aminoAcids);
        end
    end
end