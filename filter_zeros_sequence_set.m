function [inset] = filter_zeros_sequence_set(inset, filters)
    for i = 1:length(filters)
        filter = filters{i};
        sub = [];
        for j = 1:length(inset)
            if inset(j).(filter) ~= 0
                sub = [sub j];
            end
        end
        inset = inset(sub);
    end
end