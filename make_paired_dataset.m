% Create paired dataset given GO similarity matrix
%   DS      - sequence dataset
%   SIM     - paired GO similarity matrix
%   MIN_GO  - minimum nubmer of annotations for a pair of sequences
%   THR_SIM - similarity threshold to be used
%   THR_GO  - minimum number of annotations threshold to be used
%
%   IND_I   - first index in a pair
%   IND_J   - second inde in a pair
%   DS_DIFF - computed paired dataset
function [ind_i, ind_j, ds_diff] = make_paired_dataset(ds, sim, min_go, thr_sim, thr_go)
    if nargin < 4
        thr_sim = 0.5;
    end
    
    if nargin < 5
        thr_go = 5;
    end
    
    sim_bool = sim > thr_sim;
    min_bool = min_go > thr_go;
    
    [ind_i ind_j] = find(sim_bool .* min_bool);
    sel = find(ind_i < ind_j);
    ind_i = ind_i(sel);
    ind_j = ind_j(sel);
    ds_i = ds(ind_i, :);
    ds_j = ds(ind_j, :);
    ds_diff = dataset();
    n = size(ds, 2);
    for i = 1:n
        feat = double(ds_i(:, i)) - double(ds_j(:, i));
        ds_diff = horzcat(ds_diff, dataset(feat, 'VarNames', ds.Properties.VarNames{i}));
    end
end