% Compute GO similarity matrix using GOoperations object and a set of
% annotated seuqences
%   SEQ - set of sequences
%   GO  - GOoperations object
%
%   MAT - computed simularity matrix
function [mat, min_go] = compute_go_similarity_matrix(seq, go, lambda)
    if nargin < 3
        lambda = 0.9;
    end

    n = length(seq);
    %mat = zeros(n);
    vec = zeros(n, go.graph_size);
    min_go = zeros(1, n);
    normalizer = zeros(1, n);
    fprintf('[i] Precomputing GO vectors...\n');
    for i = 1:n
        vec(i, :) = go.obtain_vector(go.str2id(seq(i).go), lambda);
        min_go(i) = length(seq(i).go);
    end
    fprintf('[i] Precomputing normalizers...\n');
    for i = 1:n
        normalizer(i) = norm(vec(i, :), 2);
    end
    fprintf('[i] Computing distances...\n');
    mat = (vec * vec') ./ (normalizer' * normalizer);
    
    fprintf('[i] Computing min matrix...\n');
    min_go = repmat(min_go, n, 1);
    min_go = min(min_go, min_go');
    
    %for i = 1:n
    %    tic;
    %    fprintf('[i] Computing similarities with sequence %d.\n', i);
    %    for j = i:n
    %        mat(i, j) = go.cosine(vec(i, :), vec(j, :));
    %        mat(j, i) = mat(i, j);
    %    end
    %    toc;
    %end
end