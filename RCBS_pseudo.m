classdef RCBS_pseudo
    properties
        CodonCounts
        NucleotideCounts
        S
    end
    methods
        function [obj] = RCBS_pseudo(cds, Sweight)
            if nargin < 2
                Sweight = 300;
            end
            if length(cds) > 1 %~isa(cds, 'struct')
                obj.CodonCounts = zeros(1, length(get_codons));
                obj.NucleotideCounts = zeros(3, length(get_nucs));
                n = length(cds);
                for i = 1:n
                    seq = cds(i).seq;
                    codon = codon2int(seq);
                    nuc = nuc2int(seq);
                    for j = 1:length(codon)
                        obj.CodonCounts(codon(j)) = obj.CodonCounts(codon(j)) + 1;
                    end
                    for j = 1:3:length(nuc)
                        for k = 0:2
                            obj.NucleotideCounts(k + 1, nuc(j + k)) = obj.NucleotideCounts(k + 1, nuc(j + k)) + 1;
                        end
                    end
                end
                obj.CodonCounts = obj.CodonCounts / sum(obj.CodonCounts);
                for k = 0:2
                    obj.NucleotideCounts(k + 1, :) = obj.NucleotideCounts(k + 1, :) / sum(obj.NucleotideCounts(k + 1, :));
                end
            else
                obj.CodonCounts = cds.CodonCounts;
                obj.NucleotideCounts = cds.NucleotideCounts;
            end
            obj.S = Sweight;
        end
        
        function [rcbs] = calculate_rcbs(obj, seq)
            codon_counts = zeros(1, length(get_codons));
            nucleotide_counts = zeros(3, length(get_nucs));
            codon = codon2int(seq);
            nuc = nuc2int(seq);
            for j = 1:length(codon)
                codon_counts(codon(j)) = codon_counts(codon(j)) + 1;
            end
            for j = 1:3:length(nuc)
                for k = 0:2
                    nucleotide_counts(k + 1, nuc(j + k)) = nucleotide_counts(k + 1, nuc(j + k)) + 1;
                end
            end
            codon_counts = codon_counts / sum(codon_counts);
            for k = 0:2
                nucleotide_counts(k + 1, :) = nucleotide_counts(k + 1, :) / sum(nucleotide_counts(k + 1, :));
            end
            L = length(codon);
            rcbs = 0;
            for i = 1:L
                phi_codon = (L * codon_counts(codon(i)) + obj.S * obj.CodonCounts(codon(i))) / (L + obj.S);
                a = nuc((i - 1) * 3 + 1);
                b = nuc((i - 1) * 3 + 2);
                c = nuc(i * 3);
                phi_a = (nucleotide_counts(1, a) * L + obj.NucleotideCounts(1, a) * obj.S) / (L + obj.S);
                phi_b = (nucleotide_counts(2, b) * L + obj.NucleotideCounts(2, b) * obj.S) / (L + obj.S);
                phi_c = (nucleotide_counts(3, c) * L + obj.NucleotideCounts(3, c) * obj.S) / (L + obj.S);
                rcbs = rcbs + log(phi_codon / (phi_a * phi_b * phi_c));
            end
            rcbs = exp(rcbs / L) - 1;
        end
    end
end