classdef PIndex
    properties
        CodonCounts
        Ffamily
    end
    
    properties (Hidden)
        amino_table
    end
    
    methods
        function [obj] = PIndex(cds)
            codons = get_codons;
            obj.CodonCounts = zeros(1, length(codons));
            obj.Ffamily = zeros(1, length(codons));
            obj.amino_table = amino2codons_int;
            family = zeros(1, length(obj.amino_table));
            
            if ~isnumeric(cds)
                n = length(cds);
                for i = 1:n
                    seq = cds(i).seq;
                    codon = codon2int(seq);
                    for j = 1:length(codon)
                        obj.CodonCounts(codon(j)) = obj.CodonCounts(codon(j)) + 1;
                    end
                end
                obj.CodonCounts = obj.CodonCounts / sum(obj.CodonCounts);

                for i = 1:length(obj.amino_table)
                    family(i) = sum(obj.CodonCounts(obj.amino_table{i}));
                end
                for i = 1:length(codons)
                    obj.Ffamily(i) = obj.CodonCounts(i) / family(codon2amino_int(i));
                end
            else
                obj.Ffamily = cds;
                obj.CodonCounts = [];
            end
        end
        
        function [P] = calculate_p(obj, seq)
            codons = get_codons;
            r = zeros(1, length(codons));
            family = zeros(1, length(obj.amino_table));
            Rfamily = zeros(1, length(codons));
            
            codon = codon2int(seq);
            nuc = nuc2int(seq);
            nucleotides = zeros(1, length(get_nucs));
            for i = 1:length(nuc)
                nucleotides(nuc(i)) = nucleotides(nuc(i)) + 1;
            end
            nucleotides = nucleotides / sum(nucleotides);
            
            for i = 1:length(codons)
                cod = nuc2int(codons{i});
                r(i) = nucleotides(cod(1)) * nucleotides(cod(2)) * nucleotides(cod(3));
            end
            
            for i = 1:length(obj.amino_table)
                family(i) = sum(r(obj.amino_table{i}));
            end
            for i = 1:length(codons)
                Rfamily(i) = r(i) / family(codon2amino_int(i));
            end
            
            P = 0;
            for i = 1:length(codon)
                P = P + log(obj.Ffamily(codon(i)) / Rfamily(codon(i)));
            end
            P = exp(P / length(codon));
        end
    end
end