classdef Dnuc 
    properties
        NativeCodonUsage
    end
    
    properties (Hidden)
        codon2amino
        trans
        codon_len
        amino_len
       
        Trp
        Met
    end
    
    methods
        function [obj] = Dnuc(reference_set)
            obj.codon_len = length(get_codons);
            obj.NativeCodonUsage = zeros(1, obj.codon_len);
            obj.codon2amino = zeros(1, obj.codon_len);
            obj.trans = amino2codons_int;
            obj.amino_len = length(obj.trans);
            obj.Trp = amino2int('W');
            obj.Met = amino2int('M');
            for i = 1:obj.codon_len
                obj.codon2amino(i) = codon2amino_int(i);
            end
            if ~isnumeric(reference_set)
                for i = 1:length(reference_set)
                    seq = reference_set(i).seq;
                    if isfield(reference_set(i), 'seq_int')
                        codon = reference_set(i).seq_int;
                    else
                        codon = codon2int(seq);
                    end
                    for j = 1:length(codon)
                        obj.NativeCodonUsage(codon(j)) = obj.NativeCodonUsage(codon(j)) + 1;
                    end
                end
                obj.NativeCodonUsage = obj.NativeCodonUsage / sum(obj.NativeCodonUsage);
                for i = 1:obj.amino_len
                    codons = obj.trans{i};
                    obj.NativeCodonUsage(codons) = obj.NativeCodonUsage(codons) / sum(obj.NativeCodonUsage(codons));
                end
            else
                obj.NativeCodonUsage = reference_set;
            end
        end
        
        function [dnuc] = calculate_dnuc(obj, seq)
            if all(isnumeric(seq))
                codon = seq;
            else
                codon = codon2int(seq);
            end
            weights = zeros(1, obj.amino_len);
            freq = zeros(1, obj.codon_len);
            for i = 1:length(codon)
                freq(codon(i)) = freq(codon(i)) + 1;
            end
            for i = 1:obj.amino_len
                codons = obj.trans{i};
                freq(codons) = freq(codons) / sum(freq(codons));
                weights(i) = sqrt(sum((obj.NativeCodonUsage(codons) - freq(codons)) .^ 2));
            end
            weights = log(weights);
            amino = obj.codon2amino(codon);
            indices = logical((amino ~= obj.Trp) .* (amino ~= obj.Met));
            dnuc = exp(sum(weights(amino(indices))) / sum(indices));
        end
    end
end
