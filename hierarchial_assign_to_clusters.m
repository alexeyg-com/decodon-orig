function [assignment] = hierarchial_assign_to_clusters(bins, single)
    single = double(single(:, 1:end - 1));
    
    n = size(single, 1);
    m = length(bins);
    
    for i = 1:m
        bin = bins{i};
        bins{i} = double(bin(:, 1:end - 1));
    end
    
    assignment = zeros(1, n);
    for i = 1:n
        dist = zeros(1, m);
        for j = 1:m
            bin = bins{j};
            l = size(bin, 1);
            clust_dist = zeros(1, l);
            for k = 1:l
                clust_dist(k) = sqrt(sum((single(i, :) - bin(k, :)) .^ 2));
            end
            dist(j) = max(clust_dist);
        end
        [~, assignment(i)] = min(dist);
    end
end