function [r2 sigma features] = linear_feature_selection(ds)
    n_features = size(ds, 2) - 1;
    
    fprintf('Initial number of features: %d\n', n_features);
    
    r2 = zeros(1, n_features);
    sigma = zeros(1, n_features);
    features = cell(1, n_features);
    
    features{1} = 1:n_features;
    X = zscore(double(ds(:, features{1})));
    y = zscore(double(ds(:, end)));
    [r2(1) sigma(1)] = regression_cv(ds, 'r2', 10, 'normal', 'identity', 'linear', [], false);
    
    for i = n_features - 1:-1:1
        current_features = features{n_features - i};
        current_r2 = zeros(1, i + 1);
        current_sigma = zeros(1, i + 1);
        parfor j = 1:i + 1
            fprintf('(%d) Going to attempt for %d while my size is %d\n', i, j, length(current_features));
            new_features = current_features([1:j - 1 j + 1:i + 1]);
            new_ds = ds(:, [new_features, end]);
            [current_r2(j) current_sigma(j)] = regression_cv(new_ds, 'r2', 10, 'normal', 'identity', 'linear', [], false);
            %svmtrain(y, X, exec_string);
        end
        [r2(n_features - i + 1) ind] = max(current_r2);
        sigma(n_features - i + 1) = current_sigma(ind);
        features{n_features - i + 1} = current_features([1:ind - 1 ind + 1:i + 1]);
    end
end
