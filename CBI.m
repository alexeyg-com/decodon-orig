classdef CBI
    properties
        IsOptimal
        NumOptimal
        Degeneracy
    end
    
    properties (Hidden)
        Met
        Trp
        Asp_GAT
        Asp_GAC
        N_aminos
    end
    
    methods
        function [obj] = CBI()
            obj.IsOptimal = false(1, length(get_codons));
            obj.Met = codon2int('ATG');
            obj.Trp = codon2int('TGG');
            obj.Asp_GAT = codon2int('GAT');
            obj.Asp_GAC = codon2int('GAC');
            obj.N_aminos = length(get_aminos);
            
            obj.IsOptimal(codon2int('TTCTTGATTATCGTTGTCTCTTCCCCAACTACCGCTTACCACCAAAACAAGGAATGTAGAGGT')) = true;
            
            trans = amino2codons_int;
            obj.NumOptimal = zeros(1, obj.N_aminos);
            obj.Degeneracy = zeros(1, obj.N_aminos);
            for i = 1:length(trans)
                obj.NumOptimal(i) = sum(obj.IsOptimal(trans{i}));
                obj.Degeneracy(i) = length(trans{i});
            end
        end
        
        function [cbi] = calculate_cbi(obj, seq)
            if all(isnumeric(seq))
                codon = seq;
            else
                codon = codon2int(seq);
            end
            indices = logical((codon ~= obj.Met) .* (codon ~= obj.Trp) .* (codon ~= obj.Asp_GAT) .* (codon ~= obj.Asp_GAC));
            codon = codon(indices);
            opt = sum(obj.IsOptimal(codon));
            tot = sum(indices);
            
            amino = codon2amino_int(codon);
            amino_count = zeros(1, obj.N_aminos);
            for i = 1:length(codon)
                amino_count(amino(i)) = amino_count(amino(i)) + 1;
            end
            
            r = 0;
            for i = 1:obj.N_aminos
                r = r + amino_count(i) * obj.NumOptimal(i) / obj.Degeneracy(i);
            end
            
            cbi = (opt - r) / (tot - r);
        end
    end
end