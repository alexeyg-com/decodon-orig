classdef Predictor
    properties
        Computer
    end
    
    properties (Hidden)
        model
    end
    
    methods
        function [obj] = Predictor(type)
            if strcmpi(type, 'protein-lu')
                fileName = 'descriptors/protein-lu.mat';
                
            elseif strcmpi(type, 'lu-rscu-dG')
                fileName = 'descriptors/lu-rscu-dG.mat';
                
            elseif strcmpi(type, 'epi-rscu-dG-cut')
                fileName = 'descriptors/epi-rscu-dG-cut.mat';
                
            elseif strcmpi(type, 'epi-rscu-dG-cut-cut')
                fileName = 'descriptors/epi-rscu-dG-cut-cut.mat';
                
            elseif strcmpi(type, 'gfp')
                fileName = 'descriptors/gfp.mat';
                
            elseif strcmpi(type, 'mrna-count')
                %
            elseif strcmpi(type, 'expected-protein-ingolia')
                fileName = 'descriptors/expected-protein-ingolia.mat';
                
            elseif strcmpi(type, 'expected-protein-ingolia-corrected')
                %
            elseif strcmpi(type, 'density-ingolia')
                %
            elseif strcmpi(type, 'density-ingolia-corrected')
                %
            else
                error('Unknown predictor requested.');
            end
            
            loaded = load(fileName, 'model');
            obj.model = loaded.model;
            obj.Computer = FeatureComputer(obj.model.features);
        end
        
        function [feat, combo_seq] = compute_features(obj, combo_seq)
            [feat, combo_seq] = obj.Computer.compute_features(combo_seq, obj.model.features);
            feat = obj.transform_features(feat);
        end
        
        function [feat] = transform_features(obj, feat)
            feat = (feat - obj.model.feature_mean) ./ obj.model.feature_std;
        end
        
        function [value, feat, combo_seq] = predict_value(obj, combo_seq, transform)
            if nargin < 3
                transform = true;
            end
            
            if ~isnumeric(combo_seq)
                [feat, combo_seq] = obj.compute_features(combo_seq);
            else
                feat = combo_seq;
                if transform
                    feat = obj.transform_features(feat);
                end
            end
            value = svmpredict(-1, feat, obj.model.model);
            value = value * obj.model.predictor_std + obj.model.predictor_mean;
        end
    end
end
