function [charge] = protein_charge(seq)
    seq = nt2aa(seq);
    [~, charge] = isoelectric(seq);
end