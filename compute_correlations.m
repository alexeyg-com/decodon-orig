function [res] = compute_correlations(ds)
    feature_groups = {'GC'; 'GC15'; 'GC3'; 'len'; 'Nc'; 'Ew'; 'SCUO'; 'P1'; 'P2'; 'RCBSpc'; 'CAI'; 'tAI'; 'RCA'; 'P'; 'CPB'; 'TPI'; 'GRAVY'; 'AROMA'; 'Aliphatic'; 'Instability'; 'SideCharge'; 'tAI14'; 'tAI17'; 'tAI19'; 'SideCharge4'; 'SideCharge11'; 'SideCharge15'; 'SideCharge40'; 'CAI2nd'; 'tAI2nd'; 'dG_CDS'; 'dG_CDS17'; 'dG_CDS34'; 'dG_CDS53'; 'pI'; 'MolWeight'; 'Charge'; 'Fop'; 'CBI'; 'len_3UTR'; 'len_5UTR'; 'dG_3UTR'; 'dG_5UTR'; 'dG_5UTR50'; 'dG_5UTR50_CDS17'; 'dG_5UTR50_CDS34'; 'dG_5UTR50_CDS53'; 'dG_5UTR_CDS17'; 'dG_5UTR_CDS34'; 'dG_5UTR_CDS53'; 'dG_full'; 'codon.+'; 'nuc.+'; 'amino.+'; 'rscu.+'; 'pair_rscu.+'};
    features = cell(1, length(feature_groups));
    
    for i = 1:length(feature_groups)
        for j = 1:size(ds, 2) - 1
            if regexpi(ds.Properties.VarNames{j}, ['^' feature_groups{i} '$'])
                features{i} = [features{i} j];
            end
        end
    end
    
    res = [];
    y = double(ds(:, end));
    for i = 1:length(feature_groups)
        if isempty(features{i})
            continue;
        end
        
        % Should we compute correlation? (i.e. we do this only if we have a
        % singe feature)
        if length(features{i}) == 1
            x = double(ds(:, features{i}));
            [r p] = corr(x, y);
        else
            r = [];
            p = [];
        end
        
        ds_reg = ds(:, [features{i} end]);
        [mu, sigma] = regression_cv(ds_reg, 'R2', 10, 'normal', 'identity', 'linear', [], false);
        
        if isempty(r)
            fprintf('%10s : R2 = %6.3f +/- %6.3f\n', feature_groups{i}, mu, sigma);
        else
            fprintf('%10s : r = %6.3f (p-value %.3f), R2 = %6.3f +/- %6.3f\n', feature_groups{i}, r, p, mu, sigma);
        end
        
        item = struct('r', r, 'p_value', p, 'mu', mu, 'sigma', sigma, 'feature', feature_groups{i});
        res = [res, item];
    end
end