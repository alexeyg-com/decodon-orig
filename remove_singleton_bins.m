function [bins removed] = remove_singleton_bins(bins)
    sizes = zeros(1, length(bins));
    for i = 1:length(bins)
        sizes(i) = size(bins{i}, 1);
    end
    bins = bins(sizes > 1);
    removed = sum(sizes == 1);
end