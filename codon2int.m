% Convert codon (string) into its integer representation
%   CODONINT - the integer representation
function [codonInt] = codon2int(codon)    
    codons = get_codons;
    len = length(codon);
    if len < 3
        codonInt = [];
    elseif len == 3
        found = find(strcmpi(codon, codons));
        if length(found) == 1
            codonInt = [found];
        else
            codonInt = [length(codons)];
        end
    else
        codonInt = zeros(1, floor(len / 3));
        k = 1;
        for i = 1:3:len
            codonInt(k) = codon2int(codon(i:i + 2));
            k = k + 1;
        end
    end
end