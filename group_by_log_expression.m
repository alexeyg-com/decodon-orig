% Group entries in a dataset by LOG2 expression levels
%   DS       - dataset
%   NUM_BINS - number of bins to group expression into
%
%   BINS     - created bins
function [bins] = group_by_log_expression(ds, num_bins)
    ds(:, end) = log2(ds(:, end));
    bins = group_by_expression(ds, num_bins);
end