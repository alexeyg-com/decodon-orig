classdef RCA
    properties
        CodonCounts
        NucleotideCounts
        W
    end
    
    methods
        function [obj] = RCA(cds, pseudo)
            if nargin < 2
                pseudo = 0.0001;
            end
            codons = get_codons;
            if ~isnumeric(cds)
                obj.CodonCounts = zeros(1, length(codons));
                obj.NucleotideCounts = zeros(3, length(get_nucs));
                obj.W = zeros(1, length(codons));
                n = length(cds);
                for i = 1:n
                    seq = cds(i).seq;
                    codon = codon2int(seq);
                    nuc = nuc2int(seq);
                    for j = 1:length(codon)
                        obj.CodonCounts(codon(j)) = obj.CodonCounts(codon(j)) + 1;
                    end
                    for j = 1:3:length(nuc)
                        for k = 0:2
                            obj.NucleotideCounts(k + 1, nuc(j + k)) = obj.NucleotideCounts(k + 1, nuc(j + k)) + 1;
                        end
                    end
                end
                obj.CodonCounts = obj.CodonCounts / sum(obj.CodonCounts);
                obj.CodonCounts(obj.CodonCounts == 0) = pseudo;
                for k = 0:2
                    %obj.NucleotideCounts(k + 1, obj.NucleotideCounts(k + 1, :) == 0) = pseudo;
                    obj.NucleotideCounts(k + 1, :) = obj.NucleotideCounts(k + 1, :) / sum(obj.NucleotideCounts(k + 1, :));
                end
                for i = 1:length(codons)
                    a = nuc2int(codons{i}(1));
                    b = nuc2int(codons{i}(2));
                    c = nuc2int(codons{i}(3));
                    obj.W(i) = obj.CodonCounts(i) / (obj.NucleotideCounts(1, a) * obj.NucleotideCounts(1, b) * obj.NucleotideCounts(1, c));
                end
            else
                obj.CodonCounts = [];
                obj.NucleotideCounts = [];
                obj.W = cds;
            end
        end
        
        function [rca] = calculate_rca(obj, seq)
            codon = codon2int(seq);
            L = length(codon);
            rca = 0;
            for i = 1:L
                rca = rca + log(obj.W(codon(i)));
            end
            rca = exp(rca / L) - 1;
        end
    end
end