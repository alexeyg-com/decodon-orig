% Summarizes expression into a more compact form
%   EXPR - expression to summarize
%
%   S    - summarized expression
function [s] = summarize_expr(expr)
    real_expr = 2 .^ expr.e;
    s.e_mean = mean(real_expr);
    s.e_max = quantile(real_expr, 0.95);
    s.e_min = quantile(real_expr, 0.05);
    s.e_std = std(real_expr);
    s.e = real_expr;
    s.p = expr.p;
    s.name = expr.name;
    s.desc = expr.desc;
end