function [r2, feat] = read_features(filename)
    fin = fopen(filename, 'r');
    
    r2 = [];
    feat = [];
    
    is_r2 = true;
    all_feat = {};
    while ~feof(fin)
        line = fgets(fin);
        if is_r2
            r2 = [r2 str2num(line)];
        else
            all_feat = [all_feat {str2num(line)}];
        end
        is_r2 = ~is_r2;
    end
    fclose(fin);
    for i = 1:length(all_feat)
        cur = all_feat{i};
        if i < length(all_feat)
            next = all_feat{i + 1};
        else
            next = [];
        end
        feat = [feat setdiff(cur, next)];
    end
    feat = feat + 1;
end