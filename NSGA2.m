classdef NSGA2
    properties (Constant = true, Hidden)
        NumberOfObjectives = 2;
    end
    
    properties
        Predictor
        PopulationSize
        MaxGenerations
        MaxNoImprovementGenerations
        CrossoverProbability
        MutationProbability
        BestSequence
        CrossoverPoints
        AllowedClones
        UndesiredMotifs
    end
    
    properties (Hidden)
        amino2codon
        codons
        amino2codon_probability
        population
        objective
        combo_seq
        
        dominates
        dominated
        rank
        front
        front_cnt
        crowding

        generation
        best_fitness
        generation_objectives
    end
    
    methods
        function [obj] = NSGA2(pred, population, generations, no_improvement)
            if nargin < 2
                population = 500;
            end
            if nargin < 3
                generations = -1;
            end
            if nargin < 4
                no_improvement = 5;
            end
            
            obj.Predictor = pred;
            obj.PopulationSize = population;
            obj.MaxGenerations = generations;
            obj.MaxNoImprovementGenerations = no_improvement;
            obj.CrossoverProbability = 0.9;
            obj.MutationProbability = 0;
            obj.CrossoverPoints = 100;
            obj.AllowedClones = 3;
            obj.UndesiredMotifs = {};

            obj.amino2codon = amino2codons_int;
            obj.codons = get_codons;
            obj.amino2codon_probability = NSGA2.compute_codon_probability;
        end
        
        function [obj] = optimize(obj, combo_seq, restriction_sites)
            combo_seq.seq_amino = codon2amino_int(combo_seq.seq);
            obj.combo_seq = combo_seq;
            obj.BestSequence = combo_seq;
            obj.best_fitness = -Inf;
            obj = obj.generate_population;
            obj = obj.sort_and_crowding;

            
            mutable = 0;
            for i = 1:length(combo_seq.seq_amino)
                if length(obj.amino2codon{combo_seq.seq_amino(i)}) > 1
                    mutable = mutable + 1;
                end
            end
            obj.MutationProbability = 1 / mutable;
            
            if nargin >= 3
                obj.UndesiredMotifs = restriction_sites;
                fprintf('[i] Set %d undesired motifs.\n', length(restriction_sites));
            end
            
            obj.generation = 1;
            obj.generation_objectives = zeros(1, obj.MaxGenerations);
            while (obj.generation < obj.MaxGenerations)
                obj.generation = obj.generation + 1;
                if mod(obj.generation, 100) == 0
                    fprintf('[i] Generation %d...\n', obj.generation);
                end
                children = obj.selection;
                children = obj.mutate(children);
                obj.population = [obj.population children];
                
                obj = obj.sort_and_crowding;
                obj = obj.replace;
                obj = obj.sort_and_crowding;

                obj = obj.update_best;
            end
        end
    end
    
    methods (Access = private)
        function [obj] = sort_and_crowding(obj)
            obj = obj.compute_population_objectives;
            obj = obj.sort;
            obj = obj.compute_crowding_distance;
        end
        
        function [obj] = update_best(obj)
            top = obj.front{1}(1);
            top = obj.population(top);
            obj.generation_objectives(obj.generation) = top.objective(1);
            if top.objective(1) > obj.best_fitness
                obj.best_fitness = top.objective(1);
                obj.BestSequence = top;
                fprintf('[+] Best fitness %.15f\n', obj.best_fitness);
            end
        end
        
        function [obj] = generate_population(obj)
            % array preallocation
            pop = obj.combo_seq;
            pop(obj.PopulationSize) = obj.combo_seq;
            
            parfor i = 2:obj.PopulationSize
                pop(i) = obj.generate_individual;
            end
            obj.population = pop;
        end
        
        function [ind] = generate_individual(obj)
            seq = obj.combo_seq.seq;
            seq_amino = obj.combo_seq.seq_amino;
            for i = 1:length(seq_amino)
                amino = seq_amino(i);
                cur_codons = obj.amino2codon{amino};
                degeneracy = length(cur_codons);
                p = rand();
                pos = 1;
                while pos < degeneracy && p > obj.amino2codon_probability{amino}(pos)
                    pos = pos + 1;
                end
                seq((i - 1) * 3 + 1:3 * i) = obj.codons{cur_codons(pos)};
            end
            ind = obj.combo_seq;
            ind.seq = seq;
        end
        
        function [obj] = compute_population_objectives(obj)
            population_size = length(obj.population);
            values = zeros(population_size, obj.NumberOfObjectives);
            parfor i = 1:population_size
                values(i, :) = obj.compute_objective(obj.population(i));
            end
            obj.objective = values;
            
            % Cache objective value
            for i = 1:population_size
                obj.population(i).objective = values(i, :);
            end
        end
        
        function [obj] = sort(obj)
            population_size = length(obj.population);
            obj.dominates = cell(1, population_size);
            obj.dominated = zeros(1, population_size);
            for i = 1:population_size
                for j = i + 1:population_size
                    cmp = NSGA2.determine_domination(obj.objective(i, :), obj.objective(j, :));
                    if cmp > 0
                        obj.dominates{i} = [obj.dominates{i} j];
                        obj.dominated(j) = obj.dominated(j) + 1;
                    elseif cmp < 0
                        obj.dominates{j} = [obj.dominates{j} i];
                        obj.dominated(i) = obj.dominated(i) + 1;
                    end
                end
            end
            
            obj.front_cnt = 0;
            obj.rank = zeros(1, population_size);
            found = find(obj.dominated == 0);
            while ~isempty(found)
                obj.front_cnt = obj.front_cnt + 1;
                n_found = length(found);
                for i = 1:n_found
                    ind = obj.dominates{found(i)};
                    obj.dominated(ind) = obj.dominated(ind) - 1;
                end
                obj.rank(found) = obj.front_cnt;
                obj.dominated(found) = -1;
                found = find(obj.dominated == 0);
            end
            obj.front = cell(obj.front_cnt, 1);
            for i = 1:obj.front_cnt
                obj.front{i} = find(obj.rank == i);
            end
        end

        function [obj] = compute_crowding_distance(obj)
            population_size = length(obj.population);
            obj.crowding = zeros(1, population_size);
            for r = 1:obj.front_cnt
                ids = obj.front{r};
                for i = 1:NSGA2.NumberOfObjectives
                    [vals, ind] = sort(obj.objective(ids, i));
                    obj.crowding(ids(ind(1))) = Inf;
                    obj.crowding(ids(ind(end))) = Inf;
                    for j = 2:length(ids) - 1
                        obj.crowding(ids(ind(j))) = obj.crowding(ids(ind(j))) + (vals(j - 1) + vals(j + 1)) / (vals(end) - vals(1));
                    end
                end
                [~, ind] = sort(obj.crowding(ids), 'descend');
                obj.front{r} = ids(ind);
            end
        end

        function [obj] = replace(obj)
            new_population = zeros(1, obj.PopulationSize);
            new_population(1) = obj.front{1}(1);
            cur_ind = 2;
            r = 1;
            rank_pos = 2;
            clones = 1;
            %while r < obj.front_cnt && cur_ind <= obj.PopulationSize
            %    in_rank = length(obj.front{r});
            %    if cur_ind + in_rank - 1 <= obj.PopulationSize
            %        new_population(cur_ind:cur_ind + in_rank - 1) = obj.front{r};
            %        cur_ind = cur_ind + in_rank;
            %    else
            %        new_population(cur_ind:end) = obj.front{r}(1:obj.PopulationSize - cur_ind + 1);
            %        cur_ind = obj.PopulationSize + 1;
            %    end
            %    r = r + 1;
            %end
            while r <= obj.front_cnt && cur_ind <= obj.PopulationSize
                in_rank = length(obj.front{r});
                if rank_pos > in_rank
                    rank_pos = 1;
                    r = r + 1;
                    continue;
                end
                
                last = new_population(cur_ind - 1);
                new = obj.front{r}(rank_pos);
                if all(obj.objective(last, :) == obj.objective(new, :)) ...
                    && all(obj.population(last).seq == obj.population(new).seq)
                    if clones < obj.AllowedClones || obj.AllowedClones <= 0
                        new_population(cur_ind) = obj.front{r}(rank_pos);
                        cur_ind = cur_ind + 1;
                        clones = clones + 1;
                    end
                else
                    new_population(cur_ind) = obj.front{r}(rank_pos);
                    cur_ind = cur_ind + 1;
                    clones = 1;
                end
                rank_pos = rank_pos + 1;
            end
            % Add new individuals
            if obj.PopulationSize - cur_ind + 1 >= 1
                fprintf('[i] Creating %d new individuals...\n', obj.PopulationSize - cur_ind + 1);
            end
            effective_population_size = length(obj.population);
            for i = cur_ind:obj.PopulationSize
                effective_population_size = effective_population_size + 1;
                obj.population(effective_population_size) = obj.generate_individual;
                new_population(i) = effective_population_size;
            end
            
            obj.population = obj.population(new_population);
        end

        function [selection] = binary_tournament(obj, num)
            if nargin < 2
                num = obj.PopulationSize;
            end
            selection = zeros(1, num);
            population_size = length(obj.population);
            for i = 1:num
                tour = randperm(population_size, 2);
                a = tour(1);
                b = tour(2);
                won = a;
                if obj.rank(b) < obj.rank(a)
                    won = b;
                elseif obj.rank(a) == obj.rank(b) && obj.crowding(a) > obj.crowding(b)
                    won = b;
                end
                selection(i) = won;
            end
        end

        function [ind1, ind2] = crossover(obj, p1, p2)
            if isnumeric(p1)
                p1 = obj.population(p1);
            end
            if isnumeric(p2)
                p2 = obj.population(p2);
            end

            ind1 = p1;
            ind2 = p2;
            if rand(1) <= obj.CrossoverProbability
                gene_length = length(obj.combo_seq.seq) / 3;
                sites = randperm(gene_length, obj.CrossoverPoints);
                sites = sort(sites);
                %ind1.seq(site1 * 3 - 2 : (site2 - 1) * 3) = p2.seq(site1 * 3 - 2 : (site2 - 1) * 3);
                %ind2.seq(site1 * 3 - 2 : (site2 - 1) * 3) = p1.seq(site1 * 3 - 2 : (site2 - 1) * 3);
                for i = 1:2:obj.CrossoverPoints - 1
                    ind1.seq(sites(i) * 3 - 2 : (sites(i + 1) - 1) * 3) = p2.seq(sites(i) * 3 - 2 : (sites(i + 1) - 1) * 3);
                    ind2.seq(sites(i) * 3 - 2 : (sites(i + 1) - 1) * 3) = p1.seq(sites(i) * 3 - 2 : (sites(i + 1) - 1) * 3);
                end
                ind1.objective = [];
                ind2.objective = [];
            end
        end

        function [children] = selection(obj)
            num_parents = obj.PopulationSize;
            if mod(num_parents, 2) == 1
                num_parents = num_parents + 1;
            end
            parents = obj.binary_tournament(num_parents);
            children(1) = obj.population(1);
            children(num_parents) = obj.population(1);
            for i = 1:2:num_parents
                [children(i), children(i + 1)] = obj.crossover(parents(i), parents(i + 1));
            end
        end

        function [mutants] = mutate(obj, ind)
            mutants = ind;
            parfor i = 1:length(ind)
                mutants(i) = obj.mutate_individual(ind(i));
            end
        end

        function [ind] = mutate_individual(obj, ind)
            gene_length = length(obj.combo_seq.seq_amino);
            for i = 1:gene_length
                amino = obj.combo_seq.seq_amino(i);
                options = obj.amino2codon{amino};
                degeneracy = length(options);
                if degeneracy > 1 && rand(1) <= obj.MutationProbability
                    shift = randi(degeneracy - 1, 1);
                    codon = codon2int(ind.seq(3 * i - 2:3 * i));
                    pos = find(options == codon);
                    codon = options(mod(pos + shift, degeneracy) + 1);
                    ind.seq(3 * i - 2:3 * i) = obj.codons{codon};
                end
            end
        end

        function [objective] = compute_objective(obj, combo_seq)
            if isfield(combo_seq, 'objective') && ~isempty(combo_seq.objective)
                objective = combo_seq.objective;
            else
                objective = zeros(1, obj.NumberOfObjectives);
                objective(1) = obj.Predictor.predict_value(combo_seq);
                objective(2) = -NSGA2.count_motifs(combo_seq.seq, obj.UndesiredMotifs);
            end
        end
    end

    methods (Static)
        function [cmp] = determine_domination(a, b)
            if all(a >= b) && any(a > b)
                cmp = 1;
            elseif all(a <= b) && any(a < b)
                cmp = -1;
            else
                cmp = 0;
            end
        end
        
        function [p] = compute_codon_probability()
            options = get_S288c_options;
            weights = options.W;
            amino2codon = amino2codons_int;
            n_amino = length(amino2codon);
            p = cell(n_amino, 1);
            for i = 1:n_amino
                codons = amino2codon{i};
                degeneracy = length(codons);
                p{i} = weights(codons);
                p{i} = cumsum(p{i} / sum(p{i}));
            end
        end
        
        function [count, positions] = count_motifs(seq, motifs)
            n_motifs = length(motifs);
            count = 0;
            positions = [];
            for i = 1:n_motifs
                match_seq = seq;
                shift = 0;
                while ~isempty(match_seq)
                    pos = regexp(match_seq, motifs{i}, 'once');
                    if isempty(pos)
                        break;
                    end
                    if nargout > 1
                        record.motif = motifs{i};
                        record.position = shift + pos;
                        positions = [positions record];
                    end
                    match_seq = match_seq(pos + 1:end);
                    count = count + 1;
                    shift = pos;
                end
            end
        end
    end
end