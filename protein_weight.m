function [w] = protein_weight(seq)
    seq = nt2aa(seq);
    seq = seq(seq ~= '*');
    [w] = molweight(seq);
end