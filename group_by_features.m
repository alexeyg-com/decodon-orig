function [bins] = group_by_features(ds, num_bins, mode, type_linkage)
    if nargin < 3
        mode = 'kmeans';
    end
    if nargin < 4
        type_linkage = 'centroid';
    end
    
    n = size(ds, 1);
    m = size(ds, 2);
    % Should we normalize?
    ds_clust = double(ds(:, [1 : m - 1]));
    
    if strcmpi(mode, 'kmeans')
        ids = kmeans(ds_clust, num_bins, 'EmptyAction', 'drop', 'MaxIter', 300, 'Replicates', 5);
    elseif strcmpi(mode, 'hierarchial')
        ds_dist = pdist(ds_clust);
        link = linkage(ds_dist, type_linkage);
        ids = cluster(link, 'maxclust', num_bins);
    else
        error('Unknown groupping mode.');
    end
    
    
    bins = cell(1, num_bins);
    
    for i = 1:num_bins
        id = find(ids == i);
        bins{i} = ds(id, :);
    end
end
