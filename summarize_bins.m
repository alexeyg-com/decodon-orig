% Given a separation of the dataset into bins, create a new dataset with
% bin averages.
%   BINS - separation into bins
%
%   DS   - dataset of averages
function [ds] = summarize_bins(bins)
    num_bins = length(bins);
    ds = dataset();
    if num_bins == 0
        return
    end
    var_names = bins{1}.Properties.VarNames;
    m = size(bins{1}, 2);
    
    %ds_double = zeros(num_bins, m);
    ds_double = [];
    for i = 1:num_bins
        if ~isempty(bins{i})
            if isempty(ds_double)
                ds_double = mean(double(bins{i}), 1);
            else
                ds_double = [ds_double; mean(double(bins{i}), 1)];
            end
        end
    end
    for i = 1:m
        ds = horzcat(ds, dataset(ds_double(:, i), 'VarNames', var_names{i}));
    end
end