function [set] = attach_arava(inset, fileName_names, fileName_data)
    if nargin < 2
        fileName_names = 'import/Arava_names.xls';
    end
    if nargin < 3
        fileName_data = 'import/Arava_data.csv';
    end
    [dummy, names] = xlsread(fileName_names);
    levels = csvread(fileName_data);
    
    mRNA = levels(:, 2);
    density = levels(:, 5);
    relatie_rate = levels(:, 7);
    
    set = inset;
    map_names = strtok({inset.desc});
    total = size(levels, 1);
    missing = 0;
    for i = 1:total;
        name = names{i};
        ind = find(strcmpi(name, map_names), 1);
        if ~isempty(ind)
            set(ind).mRNA_Arava = mRNA(i);
            set(ind).density_Arava = density(i) / 100;
            set(ind).relative_rate_Arava = relatie_rate(i) / 100;
        else
            fprintf('Unable to find CDS %s\n', name);
            missing = missing + 1;
        end
    end
    
    fprintf('Total: %d, Missing: %d\n', total, missing);
end
