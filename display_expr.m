% Plots mean expression levels and their standard deviations.
%   MU    - mean values
%   SIGMA - standard deviations
%   IND   - selected indices (plotted in blue)
%   NIND  - non-selected indices (plotted in red)
function [] = display_expr(mu, sigma, ind, nind)
    x = 1:length(mu);
    [dummy, order] = sort(mu);
    color = ones(1, length(mu));
    color(nind) = 2;
    msize = ones(1, length(mu)) * 10;
    %msize(nind) = 10;
    
    mu = mu(order);
    msize = msize(order);
    color = color(order);
    pos = find(color == 1);
    npos = find(color == 2);
    
    %plot(x, mu);
    %hold on;
    subplot(2, 1, 1);
    scatter(x(npos), mu(npos), msize(npos), color(npos), 'f', 'o', 'r');
    subplot(2, 1, 2);
    scatter(x(pos), mu(pos), msize(pos), color(pos), 'f', 'o', 'b');
end