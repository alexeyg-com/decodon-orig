% Maps expression form microarray sets A and B to sequences read from a
% file.
%   EXPRA         - expression for microarray set A
%   EXPRB         - expression for microarray set B
%   FASTAFILENAME - name of a file containing CDS sequences
%
%   M             - expression per gene with sequences.
function [m] = map_expr_to_seq(exprA, exprB, fastaFileName)
    fasta = fastaread(fastaFileName);
    exprMap = strtok(exprA.desc);
    nMap = length(exprMap);
    names = {};
    m = [];
    k = 0;
    n = length(fasta);
    found = false(1, nMap);
    missedProbes = 0;
    missedCDS = 0;
    for i = 1:n
        seqName = strtok(fasta(i).Header);
        pos = strfind(seqName, '_');
        type = 'CDS';
        if ~isempty(pos)
            type = seqName(pos + 1:end);
            seqName = seqName(1:pos - 1);
        end
        ind = find(strcmpi(seqName, exprMap));
        found(ind) = true;
        if strcmpi(type, 'CDS') 
            if (length(ind) > 0)
                ind = ind(1);
                if isnan(exprA.e_mean(ind)) || isnan(exprB.e_mean(ind))
                    continue;
                end
                k = k + 1;
                m(k).desc = fasta(i).Header;
                m(k).seq = fasta(i).Sequence;
                m(k).utr3 = {};
                m(k).utr5 = {};
                m(k).mRNA_lim_mean = [exprA.e_mean(ind) exprB.e_mean(ind)];
                m(k).mRNA_lim_max = [exprA.e_max(ind) exprB.e_max(ind)];
                m(k).mRNA_lim_min = [exprA.e_min(ind) exprB.e_min(ind)];
                m(k).mRNA_lim_std = [exprA.e_std(ind) exprB.e_std(ind)];
                %m(k).e = {exprA.e(:, ind); exprB.e(:, ind)};
                m(k).probe_desc = exprA.desc{ind};
                names = [names; seqName];
            else
                fprintf('Unable to find probe for %s\n', seqName);
                missedProbes = missedProbes + 1;
                %fprintf('%s\n', seqName);
            end
        else
            r = find(strcmpi(seqName, names));
            if ~isempty(r)
                if strcmpi(type, '3UTR')
                    m(r).utr3 = [m(r).utr3; fasta(i).Sequence];
                else
                    m(r).utr5 = [m(r).utr5; fasta(i).Sequence];
                end
            else
                fprintf('Unable to find CDS for %s %s\n', seqName, type);
                missedCDS = missedCDS + 1;
            end
        end
    end
    
    missedGenes = 0;
    for i = 1:nMap
        if ~found(i)
            fprintf('Unable to find gene for %s\n', exprMap{i});
            missedGenes = missedGenes + 1;
        end
    end
    
    fprintf('Missing probes: %d, missing genes: %d, missed CDS %d\n', missedProbes, missedGenes, missedCDS);
end