% Compute amino acid usage for a given item
%   SEQ   - input sequence
%   USAGE - computed codon usage
function [usage] = compute_amino_usage(seq)
    arrayLen = length(get_aminos);
    aminos = codon2amino_int(codon2int(seq));
    
    usage = zeros(1, arrayLen);
    for i = 1:length(aminos)
        usage(aminos(i)) = usage(aminos(i)) + 1;
    end
    usage = usage / sum(usage);
end