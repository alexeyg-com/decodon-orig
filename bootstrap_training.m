function [models] = bootstrap_training(seq, bootstrap, reps, utr3, utr5, goals)
    if nargin < 2
        bootstrap = 0.9;
    end

    if nargin < 3
        reps = 1000;
    end
    
    if nargin < 4
        utr3 = true;
    end
    
    if nargin < 5
        utr5 = true;
    end
    
    if nargin < 6
        [~, ds] = prepare_datasets(seq, utr3, utr5);
    else
        if isa(goals, 'char')
            goals = { goals };
        end
        [~, ds] = prepare_datasets(seq, utr3, utr5, goals);
    end
    
    n_ds = length(goals);
    models = cell(1, n_ds);
    for i = 1:n_ds
        n_items = size(ds{i}, 1);
        n_select = min(round(n_items * bootstrap), n_items);
        ds_models = cell(1, reps);
        parfor j = 1:reps
            ds_rep = ds{i}(randperm(n_items, n_select), :);
            ds_models{j} = linear_regression(ds_rep, 'normal', 'identity', 'linear', [], true);
        end
        models{i} = ds_models;
    end
end