% Extarct GO terms from GFF-like gene description
%   DESC - gene description
%   GO   - extracted GO terms
function [go] = extract_go(desc)
    expr = '(^|;)Ontology_term=([0-9a-zA-Z\\-_\\.:\\^\\*\\$\\+\\?\\!%%,]+)';
    [matchstart,matchend,tokenindices,matchstring,tokenstring,tokenname,splitstring] = regexp(desc,expr);
    unsplit = tokenstring{1}{2};
    go = regexp(unsplit,',','split');
end