function [r2 sigma features] = svm_feature_selection(ds, item)
    n_features = size(ds, 2) - 1;
    exec_string = sprintf('-s %d -t %d -d %d -g %.4f -r %.4f -c %.4f -n %.4f -p %.4f -v %d -q', item.s, item.t, item.d, item.g, item.r, item.c, item.nu_epsilon, item.nu_epsilon, item.CV);
    
    fprintf('Initial number of features: %d\n', n_features);
    
    r2 = zeros(1, n_features);
    sigma = zeros(1, n_features);
    features = cell(1, n_features);
    
    features{1} = 1:n_features;
    X = zscore(double(ds(:, features{1})));
    y = zscore(double(ds(:, end)));
    [r2(1) sigma(1)] = svmtrain(y, X, exec_string);
    
    for i = n_features - 1:-1:1
        current_features = features{n_features - i};
        current_r2 = zeros(1, i + 1);
        current_sigma = zeros(1, i + 1);
        parfor j = 1:i + 1
            fprintf('(%d) Going to attempt for %d while my size is %d\n', i, j, length(current_features));
            new_features = current_features([1:j - 1 j + 1:i + 1]);
            X = zscore(double(ds(:, new_features)));
            y = zscore(double(ds(:, end)));
            [current_r2(j) current_sigma(j)] = svmtrain(y, X, exec_string);
        end
        [r2(n_features - i + 1) ind] = max(current_r2);
        sigma(n_features - i + 1) = current_sigma(ind);
        features{n_features - i + 1} = current_features([1:ind - 1 ind + 1:i + 1]);
    end
end
