% Visualize a set microarray expression values and their standard
% deviations
%   SEQ - set of sequences to visualize
%   HL  - names of sequences to be highligted
function [] = visualize_set_std(seq, hl)
    n = length(seq);
    mu = zeros(1, n);
    sigma = zeros(1, n);
    for i = 1:n
        mu(i) = seq(i).e_mean(1);
        sigma(i) = seq(i).e_std(1);
    end
    
    emu = mu;
    %mu = log2(mu);
    %upper = log2(emu + sigma);
    upper = emu + sigma;
    lower = emu - sigma;
    %lower(find(lower < 0)) = 0;
    sigma(find(lower < 0))
    
    [dummy, ind] = sort(mu);
    n = length(mu);
    revInd = zeros(1, n);
    x = 1:n;
    for i = 1:n
        revInd(ind(i)) = i;
    end
    
    m = length(hl);
    gene_names = strtok({seq.desc});
    mv = max(mu);
    hl_ind = [];
    for i = 1:m
        searchInd = find(strcmpi(hl{i}, gene_names));
        if length(searchInd) ~= 0
            hl_ind = [hl_ind searchInd'];
        else
            fprintf('Not found: %s\n', hl{i});
            %fprintf('%s\n', hl{i});
        end
    end
    
    yvalues = repmat([0 mv], [length(hl_ind) 1]);
    xvalues = [revInd(hl_ind); revInd(hl_ind)]';
    
    plot(x, mu(ind), 'b', x, upper(ind), 'g--', x, lower(ind), 'g--', xvalues', yvalues', 'r--');
    axis([1 n 0 mv]);
    xlabel('Gene #');
    ylabel('Average expression');
end