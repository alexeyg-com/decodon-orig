classdef CAIselecter
    properties
        Sequences
        SelectFraction
        SelectedIndices
        SelectedCount
    end
    
    properties (Hidden)
        cai
        reference_size
        desired_size
    end
    
    methods
        function [obj] = CAIselecter(reference_set, select_fraction)
            if nargin < 2
                select_fraction = 0.01;
            end
            obj.Sequences = reference_set;
            obj.SelectFraction = select_fraction;
            obj.reference_size = length(reference_set);
            obj.cai = zeros(1, obj.reference_size); 
            obj.desired_size = round(obj.SelectFraction * obj.reference_size);
            obj.SelectedCount = obj.reference_size;
            obj.SelectedIndices = 1:obj.SelectedCount;
            
            for i = 1:obj.reference_size
                obj.Sequences(i).seq_int = codon2int(obj.Sequences(i).seq);
            end
            
            iteration = 0;
            while ~obj.is_converged()
                iteration = iteration + 1;
                obj = obj.compute_cai();
                obj = obj.update_size();
                obj = obj.select_cai();
                fprintf('[i] Iteration %i: %i\n', iteration, obj.SelectedCount); 
            end
            
            stable = false;
            while ~stable && obj.SelectedCount > 1
                previous_indices = obj.SelectedIndices;
                while true
                    iteration = iteration + 1;
                    obj = obj.compute_cai();
                    obj = obj.select_cai();
                    fprintf('[i] Iteration %i: %i (robustness)\n', iteration, obj.SelectedCount); 
                    
                    rows = size(previous_indices, 1);
                    previous_indices(rows + 1, :) = obj.SelectedIndices;
                    % check for unique convergence
                    if all(previous_indices(rows, :) == obj.SelectedIndices)
                        stable = true;
                        fprintf('   [+] Converged.\n'); 
                        break;
                    end
                    
                    % search for a loop
                    loop = false;
                    for i = 1:rows - 1
                        if all(previous_indices(i, :) == obj.SelectedIndices)
                            loop = true;
                            fprintf('   [-] Loop detected.\n'); 
                            break;
                        end
                    end
                    if loop
                        break;
                    end
                end
                if ~stable && obj.SelectedCount > 1
                    obj.SelectedCount = obj.SelectedCount - 1;
                    obj.SelectedIndices = obj.SelectedIndices(1:obj.SelectedCount);
                end
            end
        end
    end
    
    methods (Access = private)
        function [ans] = is_converged(obj)
            ans = obj.SelectedCount == obj.desired_size;
        end
        
        function [obj] = compute_cai(obj)
            calc = CAI(obj.Sequences(obj.SelectedIndices));
            for i = 1:obj.reference_size
                obj.cai(i) = calc.calculate_cai(obj.Sequences(i).seq_int);
            end
            %figure;
            %hist(obj.cai, 50);
        end
        
        function [obj] = select_cai(obj)
            [dummy, ind] = sort(obj.cai, 'descend');
            %obj.previous_indices = obj.SelectedIndices;
            obj.SelectedIndices = sort(ind(1:obj.SelectedCount));
        end
        
        function [obj] = update_size(obj)
            obj.SelectedCount = max(round(obj.SelectedCount / 2), obj.desired_size);
        end
    end
end