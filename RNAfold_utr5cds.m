function [energy] = RNAfold_utr5cds(utr5, cds, part_utr5, part_cds)
    if nargin >= 3 && part_utr5 >= 0
        part_utr5 = min(part_utr5, length(utr5));
        utr5 = utr5(end - part_utr5 + 1:end);
    end
    
    if nargin >= 4 && part_cds >= 0
        part_cds = min(part_cds, round(length(cds) / 3));
        cds = cds(1:part_cds * 3);
    end
    
    energy = RNAfold([utr5 cds]);
end