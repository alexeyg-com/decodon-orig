function [goals ds] = prepare_datasets(seq, utr3, utr5, goals)
    mRNA_goals = {'mRNA-lim-mean', 'mRNA-lim-max', 'mRNA-lim-min', 'mRNA-Arava', 'mRNA-Ingolia', 'mRNA-count', 'mRNA-seq', 'mRNA-Lu'};
    density_goals = {'Density-Arava', 'Relative-rate-Arava', 'Density-Ingolia', 'Density-Ingolia-corrected'};
    velocity_goals = {'Inv-density-Arava', 'Inv-relative-rate-Arava', 'Inv-density-Ingolia', 'Inv-density-Ingolia-corrected'};
    protein_goals = {'GFP', 'TAP', 'Protein-Lu'};
    protein_per_mRNA_goals = {'GFP/mRNA-Arava', 'GFP/mRNA-count', 'GFP/mRNA-seq', 'GFP/mRNA-Ingolia', 'GFP/mRNA-Lu', 'TAP/mRNA-Arava', 'TAP/mRNA-count', 'TAP/mRNA-seq', 'TAP/mRNA-Ingolia', 'TAP/mRNA-Lu', 'Protein-Lu/mRNA-Arava', 'Protein-Lu/mRNA-count', 'Protein-Lu/mRNA-seq', 'Protein-Lu/mRNA-Ingolia', 'Protein-Lu/mRNA-Lu'};
    expected_protein_goals = {'mRNA-Arava*Density-Arava', 'mRNA-Arava*Relative-rate-Arava', 'mRNA-Arava*Density-Ingolia', 'mRNA-Arava*Density-Ingolia-corrected', 'mRNA-Ingolia*Density-Arava', 'mRNA-Ingolia*Relative-rate-Arava', 'mRNA-Ingolia*Density-Ingolia', 'mRNA-Ingolia*Density-Ingolia-corrected', 'mRNA-count*Density-Arava', 'mRNA-count*Relative-rate-Arava', 'mRNA-count*Density-Ingolia', 'mRNA-count*Density-Ingolia-corrected', 'mRNA-seq*Density-Arava', 'mRNA-seq*Relative-rate-Arava', 'mRNA-seq*Density-Ingolia', 'mRNA-seq*Density-Ingolia-corrected', 'mRNA-Lu*Density-Arava', 'mRNA-Lu*Relative-rate-Arava', 'mRNA-Lu*Density-Ingolia', 'mRNA-Lu*Density-Ingolia-corrected', 'expected-protein-Ingolia', 'expected-protein-Ingolia-corrected'};
    
    if nargin < 2
        utr3 = false;
    end
    
    if nargin < 3
        utr5 = false;
    end
    
    if nargin < 4
        goals = [mRNA_goals, density_goals, velocity_goals, protein_goals, protein_per_mRNA_goals expected_protein_goals];
    end
    
    ds = cell(1, length(goals));
    
    for i = 1:length(goals)
        goal = goals{i};
        fprintf('Generating dataset for: %s\n', goal);
        ds{i} = make_dataset(seq, goal, utr3, utr5);
    end
end
