classdef CPB
    properties
        CodonCounts
        AminoCounts
        CodonPairCounts
        AminoPairCounts
        CPS
    end
    
    properties (Hidden)
        amino_table
    end
    
    methods
        function [obj] = CPB(cds)
            codons = get_codons;
            aminos = get_aminos;
            if ~isnumeric(cds)
                obj.CodonCounts = zeros(1, length(codons));
                obj.AminoCounts = zeros(1, length(aminos));
                obj.CodonPairCounts = zeros(length(codons));
                obj.AminoPairCounts = zeros(length(aminos));
                obj.CPS = zeros(length(codons));

                n = length(cds);
                for i = 1:n
                    seq = cds(i).seq;
                    codon = codon2int(seq);
                    amino = codon2amino_int(seq);
                    for j = 1:length(codon)
                        obj.CodonCounts(codon(j)) = obj.CodonCounts(codon(j)) + 1;
                        obj.AminoCounts(amino(j)) = obj.AminoCounts(amino(j)) + 1;
                    end
                    for j = 1:length(codon) - 1
                        obj.CodonPairCounts(codon(j), codon(j + 1)) = obj.CodonPairCounts(codon(j), codon(j + 1)) + 1;
                        obj.AminoPairCounts(amino(j), amino(j + 1)) = obj.AminoPairCounts(amino(j), amino(j + 1)) + 1;
                    end
                end
                for i = 1:length(codons)
                    amino_i = codon2amino_int(i);
                    for j = 1:length(codons)
                        amino_j = codon2amino_int(j);
                        obj.CPS(i, j) = obj.CodonPairCounts(i, j) / (obj.CodonCounts(i) * obj.CodonCounts(j) / (obj.AminoCounts(amino_i) * obj.AminoCounts(amino_j)) * obj.AminoPairCounts(amino_i, amino_j));
                    end
                end
                obj.CPS = log(obj.CPS);
            else
                obj.CPS = cds;
                obj.CodonCounts = [];
                obj.AminoCounts = [];
                obj.CodonPairCounts = [];
                obj.AminoPairCounts = [];
            end
        end
        
        function [cpb] = calculate_cpb(obj, seq)
            codon = codon2int(seq);
            
            cpb = 0;
            for i = 1:length(codon) - 2
                cpb = cpb + obj.CPS(codon(i), codon(i + 1));
            end
            cpb = cpb / (length(codon) - 2);
        end
    end
end